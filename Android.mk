#
# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES :=ingenic-launcher-theme:libs/themes.jar
include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES :=ingenic-zxing:libs/zxing.jar
include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES :=iwds-jar:libs/iwds-jar.jar
include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES :=android-support-jar:libs/android-support-v4.jar
include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional


amazing_clock_dir := ../AmazingClock
amazing_notification_dir := ../AmazingNotification

src_dirs := src $(amazing_clock_dir)/src $(amazing_notification_dir)/src
res_dirs := res $(amazing_clock_dir)/res $(amazing_notification_dir)/res

LOCAL_RESOURCE_DIR := $(addprefix $(LOCAL_PATH)/, $(res_dirs))
               
LOCAL_AAPT_FLAGS := \
   --auto-add-overlay \
   --extra-packages com.ingenic.library.clock \
   --extra-packages com.ingenic.library.notification

LOCAL_SRC_FILES := $(call all-java-files-under, $(src_dirs))  $(call all-renderscript-files-under, src)


LOCAL_STATIC_JAVA_LIBRARIES := ingenic-launcher-theme \
                               ingenic-zxing  \
                               iwds-jar  \
                               android-support-jar

LOCAL_PACKAGE_NAME := AmazingLauncher

# Builds against the public SDK
#LOCAL_SDK_VERSION := current


LOCAL_CERTIFICATE := shared


include $(BUILD_PACKAGE)

# This finds and builds the test apk as well, so a single make does both.
include $(call all-makefiles-under,$(LOCAL_PATH))



