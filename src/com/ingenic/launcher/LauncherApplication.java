/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.launcher;

import com.ingenic.launcher.service.SunRiseSetService;

import android.app.Application;
import android.content.Intent;

/**
 * application
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public class LauncherApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        startService(new Intent(this, SunRiseSetService.class));
      //  CrashHandler crashHandler = CrashHandler.getInstance();
    //    crashHandler.init(this);
    }

}
