/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.blur;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;

/**
 * Blur
 * 
 * @author Shi Guanghua
 * 
 */
public class Blur {

    private float scaledBitmapDefValue = 0.3f;// 缩放比例（高斯模糊时先将原图缩小再模糊的这样可以减少运算量缩得太小会出现马赛克）
    private float blurRadiusDefValue = 10.0f;// 模糊矩阵半径（用于获取每个像素计算的色值范围）

    private BaseRender mRender;

    private LruCache<String, Bitmap> mMemoryCache;

    private static Blur blur;

    private Context mContext;

    private Blur(Context context) {
        mContext = context;
        mRender = new JNIRender();// 等GPU算法弄好了在这里可以做个判断是用那种方式进行模糊处理
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 16;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public static synchronized Blur getInstance(Context context) {
        if (blur == null)
            blur = new Blur(context);
        return blur;
    }

    public Bitmap blur(Bitmap source) {

        float scale = scaledBitmapDefValue;
        float radius = blurRadiusDefValue;
        int width;
        int height;
        Bitmap bitmap;
        if (scale == 1.0f) {
            width = source.getWidth();
            height = source.getHeight();
            bitmap = source;
        } else {
            width = Math.round(source.getWidth() * scale);
            height = Math.round(source.getHeight() * scale);
            bitmap = Bitmap.createScaledBitmap(source, width, height, true);// 这个地方会导致GC
        }
        if (height <= 0)
            height = 1;
        if (width <= 0)
            width = 1;
        Bitmap outputBitmap = getBitmapFromMemCache("" + width + height);
        if (outputBitmap == null) {
            outputBitmap = bitmap.copy(bitmap.getConfig(), true);
            addBitmapToMemoryCache("" + width + height, outputBitmap);
        }
        mRender.blur(radius, bitmap, outputBitmap);
        bitmap.recycle();
        return outputBitmap;
    }

    public Bitmap blur(Bitmap source, int x, int y, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(source, x, y, width, height);
        return blur(bitmap);
    }

    public Bitmap blur(int id) {
        Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(),
                id);
        return blur(bitmap);
    }

    public Bitmap blur(int id, int x, int y, int width, int height) {
        Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(),
                id);
        bitmap = Bitmap.createBitmap(bitmap, x, y, width, height);
        return blur(bitmap);
    }

    public Bitmap blur(Drawable drawable) {
        BitmapDrawable bd = (BitmapDrawable) drawable;
        Bitmap bitmap = bd.getBitmap();
        return blur(bitmap);
    }

    public Bitmap blur(Drawable drawable, int x, int y, int width, int height) {
        BitmapDrawable bd = (BitmapDrawable) drawable;
        Bitmap bitmap = bd.getBitmap();
        bitmap = Bitmap.createBitmap(bitmap, x, y, width, height);
        return blur(bitmap);
    }

    public void setScaledBitmap(float scaledBitmap) {
        scaledBitmapDefValue = scaledBitmap;
    }

    public void setBlurRadius(float radius) {
        blurRadiusDefValue = radius;
    }

    private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }
}
