/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.blur;

import android.graphics.Bitmap;

/**
 * JNIRender
 * 
 * @author Shi Guanghua
 * 
 */
public class JNIRender implements BaseRender {

    static {
        System.loadLibrary("blurjni");
    }

    private static native void Blur(Bitmap in, Bitmap out, int r);

    @Override
    public void blur(float radius, Bitmap in, Bitmap out) {
        Blur(in, out, (int) radius);
    }

}
