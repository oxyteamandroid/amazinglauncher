/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher;

import java.util.ArrayList;
import java.util.List;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;

import com.ingenic.iwds.app.AmazingIndeterminateProgressDialog;
import com.ingenic.launcher.utils.Constants;
import com.ingenic.launcher.utils.Utils;
import com.ingenic.launcher.view.AmazingClockView;
import com.ingenic.launcher.view.AmazingClockView.OnPageChangeListener;
import com.ingenic.launcher.view.AmazingContentView;
import com.ingenic.launcher.view.DecorView;
import com.ingenic.launcher.view.DecorView.OnContentClickListener;
import com.ingenic.launcher.view.DecorView.OnContentLongClickListener;
import com.ingenic.launcher.widget.BottomOriginWidget;
import com.ingenic.launcher.widget.notification.NotificationCenterWidget;
import com.ingenic.launcher.widget.wear.WearDragViewGroup;
import com.ingenic.launcher.widget.wear.WearDragViewGroup.OnClickClockViewIconListener;
import com.ingenic.library.analog.AnalogClockView;
import com.ingenic.library.clock.Clock;
import com.ingenic.library.clock.ClockProperty;
import com.ingenic.library.clock.ClockViewManager;
import com.ingenic.library.digital.DigitalClockView;

/**
 * 桌面的主界面
 *
 * @author Shi Guanghua
 *
 */
public class Launcher extends Activity implements OnContentLongClickListener,
        OnContentClickListener, DecorView.OnBarActionChangeListener {

    /**
     * 桌面装载 表盘，状态栏，other应用界面，语音界面 的view
     */
    private DecorView mDecorView;
    /**
     * 装载 表盘，预览表盘，other 应用界面， 语音界面
     */
    private AmazingContentView mContentView;

    /**
     * 表盘view
     */
    private AmazingClockView mAmazingClockView;

    /**
     * other 应用界面
     */

    /**
     * 表盘资源列表
     */
    private List<Clock> mClockList = null;

    /**
     * 数字表盘
     */
    private DigitalClockView mDigitalClockView;

    /**
     * 模拟表盘
     */
    private AnalogClockView mAnalogClockView;

    /**
     * 表盘的管理类
     */
    private ClockViewManager mClockViewManager;

    /**
     * 判断圆屏是方的还是圆的
     */
    private boolean mScreenisRound = false;

    /**
     * 标记是否已经进入桌面
     */
    private boolean mAttached = false;

    /**
     * 表盘当前的索引
     */
    private int mCurrentClockIndex = 0;

    /**
     * 屏幕的高减这个高度才能显示底部的拖动条
     */
    private final int SHOW_STATUS_ARROW = 30;

    /**
     * 仿 android wear 应用
     */
    private WearDragViewGroup dragViewGroup;

    /**
     * 加载框
     */
    private AmazingIndeterminateProgressDialog mAmazingIndeterminateProgressDialog;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mAttached = true;
        setContentView(R.layout.ui_home);
        initData();
        initView();
        readClocks();
    }

    private void initView() {
        mAmazingClockView = new AmazingClockView(this);
        mDecorView = (DecorView) findViewById(R.id.decorView);
        mContentView = new AmazingContentView(this);
        mAmazingIndeterminateProgressDialog = new AmazingIndeterminateProgressDialog(
                this);
        mAmazingIndeterminateProgressDialog.setCancelable(false);

        if (!mAmazingIndeterminateProgressDialog.isShowing()) {
            mAmazingIndeterminateProgressDialog.show();
        }

        // android wear
        dragViewGroup = new WearDragViewGroup(this);
        dragViewGroup.setVisibility(View.INVISIBLE);
        mDecorView.setDisplayOtherView(false);

        mContentView.addView(mAmazingClockView);
        mContentView.addView(dragViewGroup);
        mContentView.setDecorView(mDecorView);

        mDecorView.setContentView(mContentView);

        mDecorView.setBottomBar(getBottomBar(), R.id.bottom_arrow);
        mDecorView.setStatusBar(getTopStatusBar(), R.id.status_arrow);
        mDecorView.setOnContentLongClickListener(this);
        mDecorView.setOnContentClickListener(this);

        mAmazingClockView.setTouchType(AmazingClockView.HORIZONTAL);
        mAmazingClockView.setCircularable(true);

        mAmazingClockView.setOnPageChangeListener(new OnPageChangeListener() {

            @SuppressLint("NewApi")
            @Override
            public void onPageChange(int old, int current) {
                mCurrentClockIndex = current;
                Intent intent = new Intent(
                        Clock.COM_INGENIC_RUNNING_CLOCK_ID_ACTION);
                intent.putExtra(Clock.CLOCK_ID_FLAG,
                        mClockList.get(mCurrentClockIndex).getmClockId());
                Launcher.this.sendBroadcast(intent);
            }
        });

        dragViewGroup
                .setOnClickClockViewIconListener(new OnClickClockViewIconListener() {
                    @Override
                    public void onClick() {
                        onBackPressed();
                    }
                });
        mDecorView.setOnBarActionChangeListener(this);
    }

    @SuppressWarnings("deprecation")
    private void initData() {
        mClockList = new ArrayList<Clock>();
        mClockViewManager = ClockViewManager.getInstance(this);
        mScreenisRound = Utils.IsCircularScreen();
        Constants.SCREEN_WIDTH = getWindowManager().getDefaultDisplay()
                .getWidth();
        Constants.SCREEN_HEIGHT = getWindowManager().getDefaultDisplay()
                .getHeight();

    }

    /**
     * 读取表盘的信息
     */
    private synchronized void readClocks() {
        new ClockTask().execute("loadClock");
    }

    public class ClockTask extends AsyncTask<String, Void, List<Clock>> {
        @Override
        protected void onPostExecute(List<Clock> result) {
            super.onPostExecute(result);
            if (result != null)
                if (result != null && result.size() > 0) {
                    if (mClockList != null) {
                        mClockList.clear();
                        mClockList.addAll(result);
                        if (mAmazingClockView.getChildCount() > 0) {
                            mAmazingClockView.removeAllViews();
                        }

                        for (int index = 0; index < mClockList.size(); index++) {
                            if (Clock.TYPE_DIGITAL.equals(mClockList.get(index)
                                    .getmType())) {
                                mDigitalClockView = new DigitalClockView(
                                        Launcher.this);
                                mDigitalClockView.setDigitalClock(mClockList
                                        .get(index));
                                mAmazingClockView.addView(mDigitalClockView,
                                        index);

                            } else if (Clock.TYPE_ANALOG.equals(mClockList.get(
                                    index).getmType())) {

                                mAnalogClockView = new AnalogClockView(
                                        Launcher.this);
                                mAnalogClockView.setAnalogClock(mClockList
                                        .get(index));
                                mAmazingClockView.addView(mAnalogClockView,
                                        index);

                            }
                        }

                        SharedPreferences clockPreferences = getSharedPreferences(
                                "clock", Context.MODE_PRIVATE);
                        int indexClock = clockPreferences.getInt("clockIndex",
                                0);
                        if (indexClock > 0) {
                            mAmazingClockView.setCurrentPage(indexClock);
                        }

                        initThreadForClock();
                        if (mAmazingIndeterminateProgressDialog.isShowing())
                            mAmazingIndeterminateProgressDialog.dismiss();
                    }
                } else if (result != null && result.size() == 0) {
                    // initBgStatus();
                }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<Clock> doInBackground(String... params) {
            if ("loadClock".equals(params[0])) {
                return mClockViewManager.readClocks(mScreenisRound);
            } else {
                return null;
            }
        }
    }

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case Constants.MESSAGE_UPDATE_SECOND:
                Launcher.this.sendBroadcast(new Intent(
                        ClockProperty.SECOND_ACTION_UPDATE));
                break;
            default:
                break;
            }
            super.handleMessage(msg);
        }
    };

    private void initThreadForClock() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (mAttached) {
                    mHandler.removeMessages(Constants.MESSAGE_UPDATE_SECOND);
                    mHandler.sendEmptyMessage(Constants.MESSAGE_UPDATE_SECOND);
                    try {
                        sleep(Constants.FRESH_INTERVAL_TIME);
                    } catch (InterruptedException e) {
                        // e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub

        SharedPreferences clockPreferences = this.getSharedPreferences("clock",
                Context.MODE_PRIVATE);
        Editor clockEditor = clockPreferences.edit();
        clockEditor.putInt("clockIndex", mCurrentClockIndex);
        clockEditor.commit();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dragViewGroup.refresh();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        mAttached = false;
        super.onDestroy();
        if (mAmazingIndeterminateProgressDialog.isShowing())
            mAmazingIndeterminateProgressDialog.dismiss();
    }

    private Handler mAnimationLauncher = new Handler(new Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            View viewOther = (View) msg.obj;

            viewOther.setVisibility(View.VISIBLE);
            Utils.setScaleView(Constants.ZOOM_OTHER_VALUES,
                    Constants.ZOOM_OTHER_NORMAL,
                    Constants.ANIMATION_DURATION_OTHER_TIME, viewOther, false);
            return false;
        }
    });

    /**
     * 跳转到表盘的界面
     */
    private void getClockView() {
        this.sendBroadcast(new Intent(Clock.COM_INGENIC_START_CLOCK_ANIMATION));
        setClocksAnim(true);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (!mAmazingClockView.getLongClickEnable()) {
            getClockView();
        } else {
            getOtherView();
        }

        if (mDecorView.isShowStatusBar()) {
            mDecorView.hideStatusBar();
            mDecorView.hideBottomBar();
        }

        // super.onBackPressed();
    }

    private void getOtherView() {
        View viewClock = mContentView.getChildAt(0);
        View viewOther = mContentView.getChildAt(1);

        if (Utils.getIsFinishedAnimation() && viewOther != null
                && viewOther.getVisibility() == View.VISIBLE) {

            if (viewOther != null && viewClock != null) {
                Utils.setScaleView(Constants.ZOOM_OTHER_NORMAL,
                        Constants.ZOOM_OTHER_VALUES,
                        Constants.ANIMATION_DURATION_OTHER_TIME, viewOther,
                        false);
                mDecorView.setDisplayOtherView(false);
                viewClock.setVisibility(View.VISIBLE);
                Utils.setScaleView(Constants.ZOOM_CLOCK_VALUES,
                        Constants.ZOOM_NORMAL,
                        Constants.ANIMATION_DURATION_CLOCK_TIME, viewClock,
                        true);
            }

        } else {
            if (viewOther != null && viewClock != null) {
                Utils.setScaleView(Constants.ZOOM_NORMAL,
                        Constants.ZOOM_CLOCK_VALUES,
                        Constants.ANIMATION_DURATION_CLOCK_TIME, viewClock,
                        true);

                Message msg = new Message();
                msg.obj = viewOther;
                msg.what = Constants.DELAY_DISPLAY_FLAG;
                mAnimationLauncher.sendMessageDelayed(msg,
                        Constants.OTHER_DISPLAY_DELAYED);
                mDecorView.setDisplayOtherView(true);
            }
        }
        dragViewGroup.onBack();
    }

    private BottomOriginWidget mWidget;

    private View getBottomBar() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view;
        view = inflater.inflate(R.layout.bottom_bar, null);
        mWidget = (BottomOriginWidget) view
                .findViewById(R.id.widget_pager_bottom);
        LayoutParams lp = mWidget.getLayoutParams();
        lp.width = Constants.SCREEN_WIDTH;
        lp.height = Constants.SCREEN_HEIGHT;
        mWidget.setLayoutParams(lp);
        return view;
    }

    private View getTopStatusBar() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view;

        view = inflater.inflate(R.layout.status_bar, null);

        NotificationCenterWidget notficationWidget = (NotificationCenterWidget) view
                .findViewById(R.id.notification_center);
        notficationWidget
                .setListOnScrollListener(new AbsListView.OnScrollListener() {

                    @Override
                    public void onScrollStateChanged(AbsListView view,
                            int scrollState) {
                        switch (scrollState) {
                        case SCROLL_STATE_IDLE:
                            Log.d("Launcher",
                                    "====================滚动停止==================");
                            mDecorView.setCanScroll(true);
                            break;
                        case SCROLL_STATE_FLING:
                            Log.d("Launcher",
                                    "====================正在滚动==================");
                            mDecorView.setCanScroll(false);
                            break;
                        case SCROLL_STATE_TOUCH_SCROLL:
                            mDecorView.setCanScroll(false);
                            Log.d("Launcher",
                                    "====================触摸滚动==================");
                            break;
                        default:
                            break;
                        }
                    }

                    @Override
                    public void onScroll(AbsListView view,
                            int firstVisibleItem, int visibleItemCount,
                            int totalItemCount) {
                        Log.d("Launcher", "=======firstVisibleItem = "
                                + firstVisibleItem + " visibleItemCount = "
                                + visibleItemCount + " totalItemCount = "
                                + totalItemCount);
                        if (visibleItemCount == 0)
                            return;

                        if (visibleItemCount + firstVisibleItem == totalItemCount) {
                            // 当列表滚动到底部才可以滑动status bar
                            mDecorView.setCanScroll(true);
                            Log.d("Launcher",
                                    "====================到达最底部==================");
                        } else {
                            mDecorView.setCanScroll(false);
                            Log.d("Launcher",
                                    "====================不是最底部==================");
                        }
                    }
                });

        LayoutParams lp = notficationWidget.getLayoutParams();
        lp.width = Constants.SCREEN_WIDTH;
        lp.height = Constants.SCREEN_HEIGHT - SHOW_STATUS_ARROW;
        notficationWidget.setLayoutParams(lp);

        return view;
    }

    @SuppressLint("NewApi")
    private void setClocksAnim(final boolean in) {
        if (mAmazingClockView != null) {
            ValueAnimator animator = null;

            if (!in) {
                animator = ValueAnimator.ofFloat(Constants.ZOOM_NORMAL,
                        Constants.ZOOM_vALUES);
                mAmazingClockView.setScale(Constants.ZOOM_vALUES);
            } else {
                animator = ValueAnimator.ofFloat(Constants.ZOOM_vALUES,
                        Constants.ZOOM_NORMAL);
                mAmazingClockView.setScale(Constants.ZOOM_NORMAL);
            }

            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float value = ((Float) animation.getAnimatedValue())
                            .floatValue();
                    if (mAmazingClockView.getCurrent() != null) {
                        if (mAmazingClockView.getCurrent() instanceof DigitalClockView) {
                            View view = ((DigitalClockView) mAmazingClockView
                                    .getCurrent()).getRootView().getChildAt(0);
                            view.setScaleX(value);
                            view.setScaleY(value);

                        } else if (mAmazingClockView.getCurrent() instanceof AnalogClockView) {
                            View view = ((AnalogClockView) mAmazingClockView
                                    .getCurrent()).getRootView().getChildAt(0);
                            view.setScaleX(value);
                            view.setScaleY(value);

                        }
                    }
                }
            });

            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator arg0) {
                    if (in) {
                        mAmazingClockView.setVisibility(View.VISIBLE);
                        if (mAmazingClockView.getCurrent() instanceof DigitalClockView) {
                            View view = ((DigitalClockView) mAmazingClockView
                                    .getCurrent()).getRootView().getChildAt(1);
                            if (view != null)
                                view.setVisibility(View.INVISIBLE);
                        } else if (mAmazingClockView.getCurrent() instanceof AnalogClockView) {
                            View view = ((AnalogClockView) mAmazingClockView
                                    .getCurrent()).getRootView().getChildAt(1);
                            if (view != null)
                                view.setVisibility(View.INVISIBLE);
                        }
                    }
                }

                @Override
                public void onAnimationRepeat(Animator arg0) {
                }

                @Override
                public void onAnimationEnd(Animator arg0) {

                    if (!in) {

                        mAmazingClockView.setLongClickEnable(false);

                        if (mAmazingClockView.getCurrent() instanceof DigitalClockView) {
                            View view = ((DigitalClockView) mAmazingClockView
                                    .getCurrent()).getRootView().getChildAt(1);

                            if (view != null)
                                view.setVisibility(View.VISIBLE);
                        } else if (mAmazingClockView.getCurrent() instanceof AnalogClockView) {
                            View view = ((AnalogClockView) mAmazingClockView
                                    .getCurrent()).getRootView().getChildAt(1);

                            if (view != null)
                                view.setVisibility(View.VISIBLE);
                        }

                    } else {

                        mAmazingClockView.setLongClickEnable(true);

                        if (mAmazingClockView.getCurrent() instanceof DigitalClockView) {
                            View view = ((DigitalClockView) mAmazingClockView
                                    .getCurrent()).getRootView().getChildAt(1);

                            if (view != null)
                                view.setVisibility(View.INVISIBLE);
                        } else if (mAmazingClockView.getCurrent() instanceof AnalogClockView) {
                            View view = ((AnalogClockView) mAmazingClockView
                                    .getCurrent()).getRootView().getChildAt(1);

                            if (view != null)
                                view.setVisibility(View.INVISIBLE);
                        }

                    }

                }

                @Override
                public void onAnimationCancel(Animator arg0) {
                }
            });

            animator.setDuration(Constants.ANIMATION_DURATION_CLOCK_TIME)
                    .start();
        }
    }

    @Override
    public boolean onContentLongClick(View contentView) {

        if (mAmazingClockView.getLongClickEnable()) {
            mAmazingClockView.setLongClickEnable(false);
            Intent intent = new Intent(Clock.COM_INGENIC_STOP_CLOCK_ANIMATION);
            this.sendBroadcast(intent);
            setClocksAnim(false);
            mDecorView.setNotMoveStatus(true);
        }
        return true;
    }

    @Override
    public void onContentClick() {

        if (mDecorView.isShowStatusBar()) {
            return;
        }
        if (!mAmazingClockView.getLongClickEnable()) {
            this.sendBroadcast(new Intent(
                    Clock.COM_INGENIC_START_CLOCK_ANIMATION));

            mAmazingClockView.setLongClickEnable(true);
            mDecorView.setDisplayOtherView(false);
            setClocksAnim(true);
            mDecorView.setNotMoveStatus(false);
        } else {
            getOtherView();
        }
    }

    @Override
    public void onBarActionChange(int action) {
        if (null == mWidget)
            return;

        switch (action) {
        case DecorView.BAR_BOTTOM: {
            // 重置widget指示器
            mWidget.resetIndicator();

            // 启动计步View动画
            mWidget.startStepAnim();
            break;
        }
        case DecorView.NO_BAR: {
            // 还原进度位置
            mWidget.resetStep();
            break;
        }
        default:
            break;
        }
    }

}
