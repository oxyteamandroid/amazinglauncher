/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.TimeAdditionalInfo;
import com.ingenic.iwds.datatransactor.elf.TimeAdditionalTransactionModel;
import com.ingenic.iwds.datatransactor.elf.TimeAdditionalTransactionModel.TimeAdditionalInfoTransactionModelCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.launcher.utils.Constants;
import com.ingenic.library.clock.Clock;
import com.ingenic.library.clock.ClockProperty;

/**
 * 月相，日落日出服务
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class SunRiseSetService extends Service implements
        TimeAdditionalInfoTransactionModelCallback {

    private Context mContext;
    private SharedPreferences mPreferences;
    private static TimeAdditionalTransactionModel mTimeAdditionalTransactionModel = null;
    private boolean timeModelAvaiable = false;
    // 日出时间 默认为早上六点
    private int sunrise = 60 * 6;
    // 日落时间 默认为晚上六点
    private int sunset = 60 * 18;
    private SimpleDateFormat formatFrom = new SimpleDateFormat("hh:mm aa",
            Locale.ENGLISH);
    private SimpleDateFormat formatTo = new SimpleDateFormat("kk:mm");
    private TimeAdditionalReceiver mTimeAdditionalReceiver;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());

        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());
        mContext = this;
        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;
        startForeground(9999, notification);

        mPreferences = mContext.getSharedPreferences("timeAdditional",
                Context.MODE_PRIVATE);
        if (mTimeAdditionalTransactionModel == null) {
            mTimeAdditionalTransactionModel = new TimeAdditionalTransactionModel(
                    mContext, this, Constants.TIMEADDITIONAL_UUID);
        }
        mTimeAdditionalTransactionModel.start();

        mTimeAdditionalReceiver = new TimeAdditionalReceiver();
        // 注册请求日出日落时间的广播
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.COM_TIME_ADDITIONAL_ACTION);
        this.registerReceiver(mTimeAdditionalReceiver, filter);

    }

    public class TimeAdditionalReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Constants.COM_TIME_ADDITIONAL_ACTION.equals(action)) {
                requestTimeAdditionalInfo();
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if (mTimeAdditionalTransactionModel != null) {
            mTimeAdditionalTransactionModel.stop();
        }
        mContext.unregisterReceiver(mTimeAdditionalReceiver);
        // 重启日落日出服务
        sendBroadcast(new Intent(Constants.RESTART_TIME_ADDITIONAL_ACTION));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 根据时间获取分钟数
     * 
     * @param dateStr
     *            时间字符串
     * @return 分钟数
     */
    private int getTime(String dateStr) {
        Date date = null;
        if (dateStr == null) {
            dateStr = "00:00 am";
        }
        try {
            date = formatFrom.parse(dateStr);
        } catch (ParseException e) {
            // e.printStackTrace();
        }
        dateStr = formatTo.format(date);
        String[] d = dateStr.split(":");
        return Integer.parseInt(d[0]) * 60 + Integer.parseInt(d[1]);
    }

    @Override
    public void onSendResult(DataTransactResult result) {
    }

    @Override
    public void onRequestFailed() {
        IwdsLog.e("Launcher", "TimeAdditional - request failed");
    }

    @Override
    public void onRequest() {

    }

    @Override
    public void onObjectArrived(TimeAdditionalInfo object) {
        // 接收到当天日出日落时间
        if (object != null) {
            // 设置日出日落时间
            sunrise = getTime(object.sunrise);
            sunset = getTime(object.sunset);
            // 保存数据
            Intent intent = new Intent();
            intent.setAction(Clock.SUN_RISE_SET_ACTION);
            intent.putExtra(ClockProperty.SUN_RISE_FLAG, sunrise);
            intent.putExtra(ClockProperty.SUN_SET_FLAG, sunset);

            mContext.sendBroadcast(intent);
        }
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        timeModelAvaiable = isAvailable;

        requestTimeAdditionalInfo();
    }

    private void requestTimeAdditionalInfo() {
        if (timeModelAvaiable) {
            if (mTimeAdditionalTransactionModel != null) {
                // 请求日出日落时间
                mTimeAdditionalTransactionModel.request();
            }
        }
    }

}
