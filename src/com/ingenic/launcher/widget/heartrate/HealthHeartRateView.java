/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.heartrate;

import java.util.Calendar;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.launcher.R;
import com.ingenic.launcher.utils.Constants;
import com.ingenic.launcher.widget.heartrate.BaseModel.HeartRate2Model;
import com.ingenic.launcher.widget.heartrate.Model.Listener;
import com.ingenic.launcher.widget.heartrate.TimeModel.TimeListener;

/**
 * HealthHeartRateView
 * 
 * @author Jiangyanbo
 * 
 */
public class HealthHeartRateView extends LinearLayout {

    private static final String TAG = HealthHeartRateView.class.getSimpleName();

    private ImageView mHeartRateIcon;
    private TextView steps;
    private TextView timestamp;
    private TextView measuring;
    private TextView unit;
    private TextView heartRateUnit;

    private AnimationDrawable mAnimation;
    protected TimeModel mTimeModel;

    private HeartRate2Model mHeartRateModel;

    private long start;
    private int measureHeartRate;
    protected boolean isMeasure;

    private ImageView mAnim;

    public SharedPreferences sp;
    public Editor editor;

    private String spName = "heart_rate";
    private String heartRateTag = "heart_rate_tag";

    private Activity mActivity;

    private BroadcastReceiver mReceiver;

    public HealthHeartRateView(Context context) {
        this(context, null);
    }

    public HealthHeartRateView(Context context, AttributeSet attrs) {
        super(context, attrs);

        sp = context.getSharedPreferences(spName, Context.MODE_PRIVATE);
        editor = sp.edit();

        mActivity = (Activity) context;
        measureHeartRate = sp.getInt(heartRateTag, 0);

        LayoutInflater.from(context).inflate(R.layout.widget_heart_rate_layout, this);

        mAnim = (ImageView) findViewById(R.id.anim);

        mHeartRateIcon = (ImageView) findViewById(R.id.heart_rate_icon);
        timestamp = (TextView) findViewById(R.id.timestamp);
        heartRateUnit = (TextView) findViewById(R.id.heart_rate_unit);
        measuring = (TextView) findViewById(R.id.measuring);
        steps = (TextView) findViewById(R.id.steps);
        unit = (TextView) findViewById(R.id.unit);

        setEvent();
    }

    public void showHeartRate() {
        measuring.setVisibility(View.INVISIBLE);
        steps.setVisibility(View.VISIBLE);
        unit.setVisibility(View.VISIBLE);
    }

    public void showMeasuring() {
        measuring.setVisibility(View.VISIBLE);
        steps.setVisibility(View.INVISIBLE);
        unit.setVisibility(View.INVISIBLE);
    }

    private void setEvent() {
        mHeartRateIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                setIconOnClick();
            }
        });
    }

    protected void setIconOnClick() {
        if ((isMeasure = !isMeasure)) {
            IwdsLog.d(TAG, "click measuring view");
            startMeasure();
        } else {
            IwdsLog.d(TAG, "click hide view");
            stopMeasure();
        }
    }

    public void show() {

    }

    public void registerShowBroadcast() {

        IntentFilter filter = new IntentFilter(Constants.COM_INGENIC_LAUNCHER_DISPLAY_WIDGET_ACTION);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                String action = getResources().getString(R.string.heartrate_widget);
                String className = intent.getStringExtra(Constants.COM_INGENIC_WIDGET_DISPLAY_FLAG);
                className = null != className ? className : intent.getAction();
                IwdsLog.d(TAG, action + " <--- broadcast hide view ---> action: " + className);
                if (!action.equals(className) || Intent.ACTION_SCREEN_OFF.equals(intent.getAction())) {
                    IwdsLog.d(TAG, "broadcast hide view");
                    hide();
                }
            }
        };

        mActivity.registerReceiver(mReceiver, filter);
    }

    public void unregister() {
        if (null == mReceiver)
            return;

        mActivity.unregisterReceiver(mReceiver);
        mReceiver = null;
    }

    public void hide() {
        if (isMeasure) {
            IwdsLog.d(TAG, "real hide view");
            isMeasure = false;
            stopMeasure();
        }
    }

    private void startMeasure() {
        mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mHeartRateModel = new HeartRate2Model(getContext(), new Listener() {

            @Override
            public void onChange(int count, SensorEvent event) {
                IwdsLog.d(TAG, "onChange count: " + count);
                if (-1 == count) {
                    Toast.makeText(mActivity, R.string.measurement_failure, Toast.LENGTH_SHORT).show();
                    stopMeasure();
                    return;
                }
                showHeartRate();
                measureHeartRate = count;
                steps.setText("" + count);
            }
        });
        showMeasuring();

        if (null == mAnimation)
            mAnimation = (AnimationDrawable) getResources().getDrawable(R.anim.widget_heart_rate);

        if (null != mAnimation && mAnimation.isRunning()) {
            mAnimation.stop();
        }
        mAnim.setImageDrawable(mAnimation);
        if (!mAnimation.isRunning())
            mAnimation.start();

        mAnim.setVisibility(View.VISIBLE);
        mHeartRateIcon.setVisibility(View.INVISIBLE);
        mAnim.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                setIconOnClick();
            }
        });

        mHeartRateModel.register();
        IwdsLog.d(TAG, "start measure heart rate!");
    }

    private void stopMeasure() {
        mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mAnim.setVisibility(View.GONE);
        mHeartRateIcon.setVisibility(View.VISIBLE);
        if (null != mAnimation && mAnimation.isRunning()) {
            mAnimation.stop();
        }

        mHeartRateModel.unregister();
        IwdsLog.d(TAG, "stop measure heart rate!");

        showHeartRate();

        start = System.currentTimeMillis();

        if (0 == measureHeartRate)
            return;
        timestamp.setText(getTimeDescription(getContext(), start) + " " + measureHeartRate);
        heartRateUnit.setText(R.string.step_bpm);
        mTimeModel = null != mTimeModel ? mTimeModel : new TimeModel(getContext().getApplicationContext(), new TimeListener() {

            @Override
            public void onTimeChange(long time) {
                IwdsLog.d(TAG, "pass a minute");
                timestamp.setText(getTimeDescription(getContext(), start) + " " + measureHeartRate);
            }
        });
        mTimeModel.registerMin();
    }

    public String getTimeDescription(Context context, long start) {
        long now = System.currentTimeMillis();
        Calendar calStart = Calendar.getInstance();
        calStart.setTimeInMillis(start);

        Calendar calNow = Calendar.getInstance();
        calNow.setTimeInMillis(now);

        int years = calNow.get(Calendar.YEAR) - calStart.get(Calendar.YEAR);
        int resId = R.string.sport_just_now;
        if (years > 0)
            return context.getString(R.string.sport_years_ago, years, years > 1 ? "s" : "");

        int month = calNow.get(Calendar.MONTH) - calStart.get(Calendar.MONTH);
        if (month > 0)
            return context.getString(R.string.sport_months_ago, month, month > 1 ? "s" : "");

        int day = calNow.get(Calendar.DAY_OF_WEEK) - calStart.get(Calendar.DAY_OF_WEEK);
        if (day > 0)
            return context.getString(R.string.sport_days__ago, day, day > 1 ? "s" : "");

        int hours = calNow.get(Calendar.HOUR_OF_DAY) - calStart.get(Calendar.HOUR_OF_DAY);
        if (hours > 0)
            return context.getString(R.string.sport_hours_ago, hours, hours > 1 ? "s" : "");

        int minute = (int) ((now - start) / (1000 * 60));
        if (minute > 0)
            return context.getString(R.string.sport_minutes_ago, minute, minute > 1 ? "s" : "");

        return context.getString(resId);
    }

    @Override
    protected void onAttachedToWindow() {
        IwdsLog.d(TAG, "onAttachedToWindow");
        super.onAttachedToWindow();
        registerShowBroadcast();
    }

    @Override
    protected void onDetachedFromWindow() {
        IwdsLog.d(TAG, "onDetachedFromWindow");
        super.onDetachedFromWindow();
        hide();
        unregister();
    }

}