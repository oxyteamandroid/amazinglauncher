/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.heartrate;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;

import com.ingenic.iwds.utils.IwdsLog;

/**
 * TimeModel
 * 
 * @author Jiangyanbo
 * 
 */
public class TimeModel {

    private static final String TAG = TimeModel.class.getSimpleName();

    private static final Set<TimeListener> minListeners = new HashSet<TimeListener>();
    private static final Set<TimeListener> secListeners = new HashSet<TimeListener>();

    private Context mContext;
    private TimeListener mListener;
    private Handler mHandler;

    public TimeModel(Context context, TimeListener listener) {
        IwdsLog.d(TAG, "TimeModel");
        this.mContext = context.getApplicationContext();
        this.mListener = listener;
        mHandler = new Handler();
    }

    public TimeModel registerMin() {
        IwdsLog.d(TAG, "registerMin");
        if (null != mListener) {
            minListeners.add(mListener);
            startMin();
        }
        return this;
    }

    public void unregisterMin() {
        IwdsLog.d(TAG, "unregisterMin");
        if (null != mListener)
            minListeners.remove(mListener);
        stopMin();
    }

    public void registerSec() {
        IwdsLog.d(TAG, "registerSec");
        if (null != mListener) {
            secListeners.add(mListener);
            startSec();
        }
    }

    public void unregisterSec() {
        IwdsLog.d(TAG, "unregisterSec");
        if (null != mListener)
            secListeners.remove(mListener);
        stopSec();
    }

    public void startMin() {
        IwdsLog.d(TAG, "startMin");
        if (null != receiver)
            return;
        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                long time = System.currentTimeMillis();
                for (TimeListener item : minListeners) {
                    item.onTimeChange(time);
                }
            }
        };
        mContext.registerReceiver(receiver, new IntentFilter(
                Intent.ACTION_TIME_TICK));
    }

    public void startSec() {
        IwdsLog.d(TAG, "startSec");
        if (null != service)
            return;

        Runnable runnable = new Runnable() {
            public void run() {
                final long time = System.currentTimeMillis();
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        for (TimeListener item : secListeners)
                            item.onTimeChange(time);
                    }
                });

            }
        };

        service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(runnable, 0, 1, TimeUnit.SECONDS);
    }

    private static BroadcastReceiver receiver;
    private static ScheduledExecutorService service;

    public void stopSec() {
        IwdsLog.d(TAG, "stopSec");
        if (!secListeners.isEmpty())
            return;
        service.shutdown();
        service = null;
    }

    public void stopMin() {
        IwdsLog.d(TAG, "stopMin");
        if (!minListeners.isEmpty())
            return;

        mContext.unregisterReceiver(receiver);
        receiver = null;
    }

    public static interface TimeListener {
        public void onTimeChange(long time);
    }
}