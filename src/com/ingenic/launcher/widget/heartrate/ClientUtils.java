/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.heartrate;

import java.util.HashSet;
import java.util.Set;

import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceClient.ConnectionCallbacks;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartsense.SensorServiceManager;
import com.ingenic.iwds.utils.IwdsLog;

import android.content.Context;

/**
 * client 工具类
 * 
 * @author Jiangyanbo
 * 
 */
public class ClientUtils implements ConnectionCallbacks {

    private static final String TAG = ClientUtils.class.getSimpleName();

    private static ClientUtils sInstance;

    private Context mContext;
    private ServiceClient mClient;
    private Set<Callback> mCallSet;
    private SensorServiceManager mService;

    private ClientUtils(Context context) {
        mContext = context.getApplicationContext();
        mCallSet = new HashSet<ClientUtils.Callback>();
    }

    public static ClientUtils getInstance(Context context) {
        if (null == sInstance)
            synchronized (ClientUtils.class) {
                if (null == sInstance)
                    sInstance = new ClientUtils(context.getApplicationContext());
            }
        return sInstance;
    }

    public void start(Callback call) {

        if (null == mClient) {
            mClient = new ServiceClient(mContext, ServiceManagerContext.SERVICE_SENSOR, this);
            mClient.connect();
            IwdsLog.d(TAG, "mClient start connect");
        }
        call = null == call ? Callback.empty : call;
        if (null == mService)
            mCallSet.add(call);
        else
            call.onCall(mService);
    }

    public static interface Callback {
        public void onCall(SensorServiceManager service);

        public static final Callback empty = new Callback() {

            @Override
            public void onCall(SensorServiceManager service) {
            }
        };
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        mService = (SensorServiceManager) mClient.getServiceManagerContext();
        for (Callback item : mCallSet)
            item.onCall(mService);
        mCallSet.clear();
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        IwdsLog.d(TAG, "Sensor service onDisconnected :" + unexpected);
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {
        IwdsLog.d(TAG, "Sensor service connect fail reason: " + reason.getReasonCode());
    }

}