/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.heartrate;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.SparseArray;

import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.smartsense.SensorEventListener;
import com.ingenic.iwds.smartsense.SensorServiceManager;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.launcher.widget.heartrate.ClientUtils.Callback;

/**
 * Model
 * 
 * @author Jiangyanbo
 * 
 */
public class Model {

    public static final String TAG = Model.class.getSimpleName();

    private static SparseArray<Model> map = new SparseArray<Model>();

    private SensorServiceManager service;
    private Sensor sensor;
    private boolean isRegister;
    private int sensorType;
    private static Context context;

    private Listener listeners;

    private Model(int type) {
        sensorType = type;
    }

    public static Model getInstance(Context con, int type) {

        Model instance = map.get(type);
        if (null == instance) {
            synchronized (Model.class) {
                if (null == instance) {
                    context = con.getApplicationContext();
                    instance = new Model(type);
                    map.put(type, instance);
                }
            }
        }
        return instance;
    }

    // ================================================================================

    private SensorEventListener listener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (null != listeners)
                listeners.onChange((int) event.values[0], event);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    // ================================================================================

    private void registerSensor(Context context) {
        if (isRegister)
            return;
        ClientUtils.getInstance(context).start(new Callback() {
            @Override
            public void onCall(SensorServiceManager service) {
                if (isRegister)
                    return;

                Model.this.service = service;
                if (null == sensor) {
                    sensor = service.getDefaultSensor(sensorType);
                }
                if (null == sensor) {
                    IwdsLog.d(TAG, "register sensor fails ---> " + isRegister
                            + " " + getSensorName(sensorType));
                    return;
                }
                isRegister = service.registerListener(listener, sensor, 0);
                IwdsLog.d(TAG, "register sensor success ---> " + isRegister
                        + " " + getSensorName(sensorType));
            }
        });
    }

    public String getSensorName(int type) {

        return dic.get(type);
    }

    @SuppressLint("UseSparseArrays")
    private static final HashMap<Integer, String> dic = new HashMap<Integer, String>();
    static {
        dic.put(Sensor.TYPE_HEART_RATE, "TYPE_HEART_RATE");
        dic.put(Sensor.TYPE_UV, "TYPE_UV");
        dic.put(Sensor.TYPE_GESTURE, "TYPE_GESTURE");
        dic.put(Sensor.TYPE_MOTION, "TYPE_MOTION");
        dic.put(Sensor.TYPE_VOICE_TRIGGER, "TYPE_VOICE_TRIGGER");
        dic.put(Sensor.TYPE_STEP_COUNTER, "TYPE_STEP_COUNTER");
        dic.put(Sensor.TYPE_RELATIVE_HUMIDITY, "TYPE_RELATIVE_HUMIDITY");
        dic.put(Sensor.TYPE_AMBIENT_TEMPERATURE, "TYPE_AMBIENT_TEMPERATURE");
        dic.put(Sensor.TYPE_PRESSURE, "TYPE_PRESSURE");
        dic.put(Sensor.TYPE_PROXIMITY, "TYPE_PROXIMITY");
        dic.put(Sensor.TYPE_ALL, "TYPE_ALL");
    }

    private void unregisterSensor() {
        if (isRegister)
            service.unregisterListener(listener, sensor);
        IwdsLog.d(TAG, "unregister sensor ---> " + isRegister);
        isRegister = false;
    }

    // ==========================================================================

    public void register(Listener listener) {
        listeners = listener;
        registerSensor(context);
    }

    public void unregister(Listener listener) {
        IwdsLog.d(TAG, "sensor unregister(Listener listener) ---> " + isRegister);
        listeners = null;
        unregisterSensor();
    }

    // ==========================================================================

    public static interface Listener {
        public void onChange(int count, SensorEvent event);

        public static final Listener empty = new Listener() {

            @Override
            public void onChange(int count, SensorEvent value) {
            }
        };
    }
}