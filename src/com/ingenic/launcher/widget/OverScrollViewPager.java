/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.TranslateAnimation;

/**
 * Widget
 * 
 * @author tZhang & ShiGuangHua(Kenny)
 * 
 */
public class OverScrollViewPager extends ViewPager {

    private int currentPosition = 0;
    private Rect mRect = new Rect();// 用来记录初始位置
    private boolean handleDefault = true;
    private float preX = 0f;
    private static final float RATIO = 0.5f;// 摩擦系数
    private static final float SCROLL_WIDTH = 10f;

    public OverScrollViewPager(Context context) {
        super(context);
    }

    public OverScrollViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            preX = ev.getX();// 记录起点
            currentPosition = getCurrentItem();
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
        case MotionEvent.ACTION_UP:
            onTouchActionUp();
            break;
        case MotionEvent.ACTION_MOVE:
            if (getAdapter().getCount() == 1) {
                float nowX = ev.getX();
                float offset = nowX - preX;
                preX = nowX;

                if (offset > SCROLL_WIDTH) {// 手指滑动的距离大于设定值
                    whetherConditionIsRight(offset);
                } else if (offset < -SCROLL_WIDTH) {
                    whetherConditionIsRight(offset);
                } else if (!handleDefault) {// 这种情况是已经出现缓冲区域了，手指慢慢恢复的情况
                    if (getLeft() + (int) (offset * RATIO) != mRect.left) {
                        layout(getLeft() + (int) (offset * RATIO), getTop(),
                                getRight() + (int) (offset * RATIO),
                                getBottom());
                    }
                }
            } else if ((currentPosition == 0 || currentPosition == getAdapter()
                    .getCount() - 1)) {
                float nowX = ev.getX();
                float offset = nowX - preX;
                preX = nowX;

                if (currentPosition == 0) {
                    if (offset > SCROLL_WIDTH) {// 手指滑动的距离大于设定值
                        whetherConditionIsRight(offset);
                    } else if (!handleDefault) {// 这种情况是已经出现缓冲区域了，手指慢慢恢复的情况
                        if (getLeft() + (int) (offset * RATIO) >= mRect.left) {
                            layout(getLeft() + (int) (offset * RATIO),
                                    getTop(), getRight()
                                            + (int) (offset * RATIO),
                                    getBottom());
                        }
                    }
                } else {
                    if (offset < -SCROLL_WIDTH) {
                        whetherConditionIsRight(offset);
                    } else if (!handleDefault) {
                        if (getRight() + (int) (offset * RATIO) <= mRect.right) {
                            layout(getLeft() + (int) (offset * RATIO),
                                    getTop(), getRight()
                                            + (int) (offset * RATIO),
                                    getBottom());
                        }
                    }
                }
            } else {
                handleDefault = true;
            }

            if (!handleDefault) {
                return true;
            }
            break;

        default:
            break;
        }
        return super.onTouchEvent(ev);
    }

    private void whetherConditionIsRight(float offset) {
        if (mRect.isEmpty()) {
            mRect.set(getLeft(), getTop(), getRight(), getBottom());
        }
        handleDefault = false;
        layout(getLeft() + (int) (offset * RATIO), getTop(), getRight()
                + (int) (offset * RATIO), getBottom());
    }

    private void onTouchActionUp() {
        if (!mRect.isEmpty()) {
            recoveryPosition();
        }
    }

    private void recoveryPosition() {
        TranslateAnimation ta = new TranslateAnimation(getLeft(), mRect.left,
                0, 0);
        ta.setDuration(300);
        startAnimation(ta);
        layout(mRect.left, mRect.top, mRect.right, mRect.bottom);
        mRect.setEmpty();
        handleDefault = true;
    }

}
