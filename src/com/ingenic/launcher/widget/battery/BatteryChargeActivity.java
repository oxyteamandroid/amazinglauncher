/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.battery;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.ingenic.launcher.R;
import com.ingenic.launcher.utils.Constants;

/**
 * 插入电源弹出的Activity
 *
 * @author likairui & Shi Guanghua
 *
 */
public class BatteryChargeActivity extends Activity {
    private RingProgressView progress;
    private final float ANIMATION_INIT = 0f;
    private final float ALPHA_MAX = 1f;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.black);
        setContentView(R.layout.widget_battery_dialog_charge);

        progress = (RingProgressView) findViewById(R.id.battery_progress);
        TextView tv_battery_pct = (TextView) findViewById(R.id.tv_battery_state);
        TextView tv_battery_text = (TextView) findViewById(R.id.tv_battery_text);

        Intent intent = getIntent();
        int level = intent.getIntExtra(Constants.BATTERY_LEVEL_BUNDLE,
                Constants.BATTERY_DEFAULT_CHARGE);
        int full = intent.getIntExtra(Constants.BATTERY_FULL_BUNDLE,
                Constants.BATTERY_FULL_POWER_STATE);

        tv_battery_pct.setText(((level * Constants.BATTERY_FULL_POWER_STATE)
                / full + "%"));

        if (Constants.BATTERY_FULL_POWER_STATE == level)
            tv_battery_text.setText(getResources().getString(
                    R.string.battery_text_full));
        else
            tv_battery_text.setText(getResources().getString(
                    R.string.battery_text_charging));

        startAlphaAnimation(ANIMATION_INIT, ALPHA_MAX);
        startBatteryAnimation(ANIMATION_INIT, level);

        /**
         * 延时自动关闭
         */
        new Handler().postDelayed(new Runnable() {
            public void run() {
                finish();
                overridePendingTransition(0, R.anim.alpha_out);
            }
        }, Constants.DURATION_ACTIVITY_DISPLAY);
    }

    private void startBatteryAnimation(float startPosition, float stopPosition) {

        ValueAnimator mValue = ValueAnimator.ofFloat(startPosition,
                stopPosition);
        mValue.setDuration(Constants.DURATION_ACTIVITY_DISPLAY);
        mValue.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                float valueInt = ((Float) animation.getAnimatedValue())
                        .intValue();

                progress.setProgress((int) valueInt);
            }
        });
        mValue.start();
    }

    private void startAlphaAnimation(float startPosition, float stopPosition) {
        ValueAnimator mValue = ValueAnimator.ofFloat(startPosition,
                stopPosition);
        mValue.setDuration(Constants.DURATION_ACTIVITY_DISPLAY);
        mValue.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                float valueInt = ((Float) animation.getAnimatedValue())
                        .floatValue();
                getWindow().getDecorView().setAlpha(valueInt);
            }
        });
        mValue.start();
    }

}
