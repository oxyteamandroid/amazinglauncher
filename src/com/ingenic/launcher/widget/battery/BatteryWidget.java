/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.battery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingenic.launcher.R;
import com.ingenic.launcher.utils.Constants;

/**
 *
 * 电量显示 Widget
 *
 * Copyright (c) 2015年10月27日 by ingenic
 *
 * @author <a href="kairui.li@ingenic.com">kairuili</a>
 *
 */
public class BatteryWidget extends RelativeLayout {
    private Context mContext;

    private RingProgressView battery_progress;
    private BatteryChangedReceiver mBatteryChangedReceiver;

    private RelativeLayout rl_battery;
    private RelativeLayout rl_battery_low;

    private TextView tv_battery_pct;
    private TextView tv_battery_text;
    private TextView tv_battery_low;

    /**
     * 是否插入电源
     */
    private boolean isPlugged = false;

    public BatteryWidget(Context context) {
        super(context);
        initView(context);
    }

    public BatteryWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public BatteryWidget(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    private void initView(Context context) {
        mContext = context;
        mBatteryChangedReceiver = new BatteryChangedReceiver();

        View view = LayoutInflater.from(context).inflate(
                R.layout.widget_battery_layout, this, true);

        rl_battery = (RelativeLayout) view.findViewById(R.id.rl_battery);
        rl_battery_low = (RelativeLayout) view
                .findViewById(R.id.rl_battery_low);

        battery_progress = (RingProgressView) view
                .findViewById(R.id.battery_progress);
        tv_battery_pct = (TextView) view.findViewById(R.id.tv_battery_pct);
        tv_battery_text = (TextView) view.findViewById(R.id.tv_battery_text);
        tv_battery_low = (TextView) view.findViewById(R.id.tv_battery_low);

        battery_progress.setProgress(Constants.BATTERY_DEFAULT_CHARGE);

        context.registerReceiver(mBatteryChangedReceiver, getFilter());
    }

    private IntentFilter getFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);
        filter.addAction(Intent.ACTION_BATTERY_LOW);
        filter.addAction(Intent.ACTION_BATTERY_OKAY);
        return filter;
    }

    /**
     * 显示未充电
     */
    private void showBatteryNoCharge() {
        battery_progress.setRingColor(Color.rgb(139, 242, 28));
        rl_battery.setVisibility(VISIBLE);
        rl_battery_low.setVisibility(GONE);
        tv_battery_text.setText(getResources().getString(
                R.string.battery_text_discharging));
    }

    /**
     * 显示正在充电
     */
    private void showBatteryCharge() {
        showBatteryNoCharge();
        tv_battery_text.setText(getResources().getString(
                R.string.battery_text_charging));
    }

    /**
     * 显示电量低
     */
    private void showBatteryLow(int BatteryLevel, int BatteryFull) {
        battery_progress.setRingColor(Color.rgb(230, 66, 68));
        rl_battery_low.setVisibility(VISIBLE);
        rl_battery.setVisibility(GONE);

        tv_battery_low
                .setText(//
                String.valueOf(((BatteryLevel * Constants.BATTERY_FULL_POWER_STATE) / BatteryFull)
                        + "%"));
    }

    /**
     * 启动进入充电状态的 Activity
     * @param context
     *
     * @param mBatteryLevel
     *            当前的电量
     * @param BatteryFull
     *            电量的最大值
     */
    private void openBatteryChargeActivity(Context context, int BatteryLevel, int BatteryFull) {
        Intent intent = new Intent(mContext, BatteryChargeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.BATTERY_LEVEL_BUNDLE, BatteryLevel);
        bundle.putInt(Constants.BATTERY_FULL_BUNDLE, BatteryFull);
        intent.putExtras(bundle);
        context.startActivity(intent);
        //((Activity) context).overridePendingTransition(R.anim.alpha_in, 0);
    }

    class BatteryChangedReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            // 电池当前的电量, 它介于0和 EXTRA_SCALE之间
            int mBatteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,
                    15);
            // 电池电量的最大值
            int mBatteryFull = intent.getIntExtra(BatteryManager.EXTRA_SCALE,
                    Constants.BATTERY_FULL_POWER_STATE);

            final String action = intent.getAction();
            if (Intent.ACTION_BATTERY_CHANGED.equalsIgnoreCase(action)) {
                battery_progress.setProgress(mBatteryLevel);
                tv_battery_pct
                        .setText(//
                        String.valueOf(((mBatteryLevel * Constants.BATTERY_FULL_POWER_STATE) / mBatteryFull)
                                + "%"));

                // 电池的健康状态
                int health = intent
                        .getIntExtra(BatteryManager.EXTRA_HEALTH, -1);
                switch (health) {
                case BatteryManager.BATTERY_HEALTH_GOOD:
                    // 正在放电
                    if (15 >= mBatteryLevel)
                        showBatteryLow(mBatteryLevel, mBatteryFull);

                    else
                        showBatteryNoCharge();
                    break;
                }

                int status = intent
                        .getIntExtra(BatteryManager.EXTRA_STATUS, -1);
                switch (status) {
                case BatteryManager.BATTERY_STATUS_CHARGING:
                    // 正在充电
                    if (!isPlugged) {
                        openBatteryChargeActivity(context, mBatteryLevel, mBatteryFull);
                        isPlugged = true;
                    }

                    showBatteryCharge();

                    if (Constants.BATTERY_FULL_POWER_STATE == mBatteryLevel)
                        tv_battery_text.setText(getResources().getString(
                                R.string.battery_text_full));

                    break;

                case BatteryManager.BATTERY_STATUS_DISCHARGING:
                    isPlugged = false;
                    break;
                case BatteryManager.BATTERY_STATUS_FULL:
                    // 已充满
                    showBatteryCharge();
                    tv_battery_text.setText(getResources().getString(
                            R.string.battery_text_full));
                    break;
                }
            } else if (Intent.ACTION_BATTERY_LOW.equalsIgnoreCase(action)) {
                // 电池电量低
                showBatteryLow(mBatteryLevel, mBatteryFull);
            }
        }
    }

    public void setDestroy() {
        mContext.unregisterReceiver(mBatteryChangedReceiver);
    }
}
