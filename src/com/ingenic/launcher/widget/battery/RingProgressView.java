/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.battery;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * 自定义控件：环形进度控件
 * 
 * @author likairui & MingDan
 * 
 */
public class RingProgressView extends View {
    // 画外圈的画笔
    private Paint mOuterRingPaint;
    // 画圆环的画笔
    private Paint mRingPaint;
    // 圆形颜色
    private int mCircleColor;
    // 圆环颜色
    private int mRingColor;
    // 半径
    private float mCircleRadius;
    // 圆环半径
    private float mRingRadius;
    // 圆与圆环间隔宽度
    private float mSpaceWidth;
    // 圆心x坐标
    private int mXCenter;
    // 圆心y坐标
    private int mYCenter;
    // 总进度
    private int mTotalProgress = 100;
    // 当前进度
    private int mProgress;

    public RingProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // 获取自定义的属性
        initAttrs(context, attrs);
        initVariable();
    }

    /**
     * 获取自定义属性值
     */
    private void initAttrs(Context context, AttributeSet attrs) {
        mCircleRadius = dip2px(context, 70);
        // 圆与圆环间隔宽度
        mSpaceWidth = dip2px(context, 5);
        // 圆环半径
        mRingRadius = mCircleRadius + mSpaceWidth;

        mCircleColor = Color.rgb(33, 33, 33);
        mRingColor = Color.rgb(139, 242, 28);
    }

    /**
     * 初始化
     */
    private void initVariable() {
        mOuterRingPaint = new Paint();
        mOuterRingPaint.setAntiAlias(true);
        mOuterRingPaint.setColor(mCircleColor);
        mOuterRingPaint.setStyle(Paint.Style.STROKE);
        mOuterRingPaint.setStrokeWidth(25);

        mRingPaint = new Paint();
        mRingPaint.setAntiAlias(true);
        mRingPaint.setColor(mRingColor);
        mRingPaint.setStyle(Paint.Style.STROKE);
        mRingPaint.setStrokeWidth(25);
        mRingPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {

        mXCenter = getWidth() / 2;
        mYCenter = getHeight() / 2;

        canvas.drawCircle(mXCenter, mYCenter, mRingRadius, mOuterRingPaint);

        if (mProgress >= 0) {
            RectF oval = new RectF();
            oval.left = mXCenter - mRingRadius;
            oval.top = mYCenter - mRingRadius;
            oval.right = mXCenter + mRingRadius;
            oval.bottom = mYCenter + mRingRadius;
            canvas.drawArc(oval, -90,
                    ((float) mProgress / mTotalProgress) * 360, false,
                    mRingPaint);
        }
    }

    public void setProgress(int progress) {
        mProgress = progress;
        postInvalidate();
    }

    private static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 设置圆环颜色
     * 
     * @param mRingColor
     */
    public void setRingColor(int mRingColor) {
        mRingPaint.setColor(mRingColor);
        postInvalidate();
    }

}
