/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingLauncher Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.wear;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.widget.Toast;

public class WearAppManager {
    /**
     * 开启一个应用程序
     * 
     * @param pContext
     *            上下文
     * @param packageName
     *            包名
     */
    public static void startApplication(Context pContext, String packageName) {
        // 查询这个应用程序的入口activity。 把他开启起来。
        PackageManager pm = pContext.getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage(packageName);
        if (intent != null)
            pContext.startActivity(intent);
        else
            Toast.makeText(pContext, "不能启动当前应用", Toast.LENGTH_SHORT).show();
    }

    /**
     * 卸载一个应用程序 会弹出系统的卸载窗口，修改为静默卸载
     * 
     * @param pContext
     *            上下文
     * @param packageName
     *            包名
     * @return 意图对象
     */
    @Deprecated
    public static Intent uninstallAppliation(Context pContext,
            String packageName) {
        // <action android:name="android.intent.action.VIEW" />
        // <action android:name="android.intent.action.DELETE" />
        // <category android:name="android.intent.category.DEFAULT" />
        // <data android:scheme="package" />
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setAction("android.intent.action.DELETE");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setData(Uri.parse("package:" + packageName));
        // startActivityForResult(intent, 0);
        return intent;
    }
}
