/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingLauncher Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.wear;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import android.widget.TextView;

import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.devicemanager.DeviceManagerServiceManager;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.launcher.R;
import com.ingenic.launcher.utils.Constants;
import com.ingenic.launcher.view.DecorView.OnBarActionChangeListener;

/**
 * 自定义可拖拽ViewGroup + 边缘缩放 + 长按卸载
 * 
 * @author kairuili
 * 
 */
public class WearDragViewGroup extends RelativeLayout implements
        OnGestureListener, ServiceClient.ConnectionCallbacks {
    /**
     * 触摸状态
     */
    private final static int TOUCH_STATE_REST = 0;
    /**
     * 移动状态
     */
    private final static int TOUCH_STATE_SCROLLING = 1;
    /**
     * 触摸状态，默认为停止
     */
    private int mTouchState = TOUCH_STATE_REST;
    /**
     * 滚动的距离
     */
    private int mTouchSlop;

    /**
     * 自定义圆形ImagView
     */
    private WearRoundedImageView mRoundedImageView;

    /**
     * 所有App数据
     */
    private List<WearAppInfo> appInfos;

    /**
     * 用户App数据
     */
    private List<WearAppInfo> userAppInfos;

    /**
     * App 数量
     */
    private int app_num;

    private GestureDetector detector;
    private Scroller mScroller;
    private Context mContext;
    private ViewConfiguration configuration;

    /**
     * 使用两个不同的动画来模拟
     */
    private Animation shake;
    private Animation shake2;

    private List<Integer> userApps;

    // *********************************** 坐标 距离
    // ***********************************
    /**
     * 最后点击的 X,Y 坐标
     */
    private float mLastMotionX, mLastMotionY;
    /**
     * X,Y 轴的移动距离
     */
    int move_X = 0, move_Y = 0;
    /**
     * 最大允许的移动距离
     */
    int MAXMOVE = 300;

    /**
     * 必须把 onInterceptTouchEvent 中的坐标放到全局才能在 onTouchEvent 中准确的计算偏移量
     */
    private float intercept_x, intercept_y;

    // *********************************** view 信息
    // ***********************************

    /**
     * 图标的大小
     */
    public int viewSize = 120;

    /**
     * 子view距屏幕的距离
     */
    private int[] locationsScr = new int[2];

    private ServiceClient mClient;

    /**
     * 构造方法
     * 
     * @param context
     */
    public WearDragViewGroup(Context context) {
        super(context);
        mContext = context;
        fillData();
    }

    private void fillData() {
        mClient = new ServiceClient(mContext,
                ServiceManagerContext.SERVICE_DEVICE_MANAGER, this);
        mClient.connect();

        WearCustomMsg.setContext(mContext);

        // 初始化 Scroller，可以在构造中加插值器加速减速
        mScroller = new Scroller(mContext);
        detector = new GestureDetector(mContext, this);
        userApps = new ArrayList<Integer>();
        userAppInfos = new ArrayList<WearAppInfo>();

        configuration = ViewConfiguration.get(mContext);

        // 获得最小的滚动距离
        mTouchSlop = configuration.getScaledTouchSlop();
        // 仿IOS长按卸载摇晃动画
        shake = AnimationUtils.loadAnimation(mContext,
                com.ingenic.launcher.R.anim.widget_uninstall_shake);
        shake2 = AnimationUtils.loadAnimation(mContext,
                R.anim.widget_uninstall_shake2);

        viewSize = WearDensityUtil.px2dip(mContext, viewSize);

        initView();
    }

    public void initView() {
        setBackgroundColor(Color.BLACK);

        // 获取所有有Activity的应用程序信息
        appInfos = WearAppInfoProvider.getAllActivity(mContext);

        // 添加所有 App 图标到 ViewGroup 中
        app_num = 0;
        userAppInfos.clear();

        for (int i = 0; i < (appInfos.size() + 1); i++) {
            mRoundedImageView = new WearRoundedImageView(mContext);

            if (0 == i) {
                mRoundedImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
                mRoundedImageView.setTag(R.id.app_id, i);
                mRoundedImageView.setTag(R.id.package_name, "SwitchWear");
                mRoundedImageView.setOval(true);
                mRoundedImageView.setScaleType(ScaleType.FIT_CENTER);
                mRoundedImageView.setBorderWidthDP(2);
                mRoundedImageView.setBackground(getResources().getDrawable( R.drawable.widget_view_bg));
                mRoundedImageView.setOnClickListener(new SwitchWearClickListener());
                mRoundedImageView.setOnTouchListener(new MyTouchClickListener());
                addView(mRoundedImageView);
                ++app_num;
                continue;
            }

            mRoundedImageView.setImageDrawable(appInfos.get(i - 1).getIcon());
            mRoundedImageView.setOval(true);
            mRoundedImageView.setScaleType(ScaleType.FIT_CENTER);
            mRoundedImageView.setBorderWidthDP(2);
            mRoundedImageView.setBackground(getResources().getDrawable(
                    R.drawable.widget_view_bg));
            // 设置Tag
            mRoundedImageView.setTag(R.id.package_name, appInfos.get(i - 1)
                    .getPackname());
            mRoundedImageView.setTag(R.id.app_id, i - 1);
            mRoundedImageView.setTag(R.id.app_name, appInfos.get(i - 1).getName());

            // 设置监听
            mRoundedImageView.setOnClickListener(new ImageOnClickListener());
            mRoundedImageView
                    .setOnLongClickListener(new MyOnLongClickListener());
            mRoundedImageView.setOnTouchListener(new MyTouchClickListener());

            addView(mRoundedImageView);

            // 添加用户App到集合中，用于长按时只开启用户App的动画
            if (appInfos.get(i - 1).isUserApp())
                userAppInfos.add(appInfos.get(i - 1));

            ++app_num;
        }
        // 初始化摇晃动画
        initUserAppAnim(userAppInfos);
    }

    /**
     * 切换表盘的点击事件
     */
    class SwitchWearClickListener implements OnClickListener {
        public void onClick(View v) {
            mClickListener.onClick();
        }
    }

    private OnClickClockViewIconListener mClickListener;

    public void setOnClickClockViewIconListener(OnClickClockViewIconListener l) {
        mClickListener = l;
    }

    public interface OnClickClockViewIconListener {
        void onClick();
    }

    public void computeScroll() {
        // 先判断mScroller滚动是否完成
        if (mScroller.computeScrollOffset()) {
            // 返回当前滚动y方向的偏移
            requestLayout();
            scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
            postInvalidate();
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent even) {
        intercept_x = even.getX();
        intercept_y = even.getY();
        switch (even.getAction()) {
        case MotionEvent.ACTION_DOWN:
            mLastMotionX = intercept_x;
            mLastMotionY = intercept_y;
            // 判断当前触摸状态
            mTouchState = mScroller.isFinished() ? //
            TOUCH_STATE_REST
                    : TOUCH_STATE_SCROLLING;
            break;
        case MotionEvent.ACTION_MOVE:
            // 计算偏移量是否满足滚动距离
            final int xDiff = (int) Math.abs(even.getX() - mLastMotionX);
            boolean xMoved = xDiff > mTouchSlop;

            final int yDiff = (int) Math.abs(even.getY() - mLastMotionY);
            boolean yMoved = yDiff > mTouchSlop;
            // 判断是否在移动
            if (yMoved || xMoved)
                mTouchState = TOUCH_STATE_SCROLLING;

            break;
        case MotionEvent.ACTION_UP:
            mTouchState = TOUCH_STATE_REST;
            break;
        }
        // 移动返回true，点击返回false
        return mTouchState != TOUCH_STATE_REST;
    }

    public boolean onTouchEvent(MotionEvent even) {
        final float x = even.getX();
        final float y = even.getY();

        switch (even.getAction()) {
        case MotionEvent.ACTION_DOWN:
            if (!mScroller.isFinished()) {
                mScroller.forceFinished(true);
                // 获取最后停止的坐标
                move_X = mScroller.getFinalX();
                move_Y = mScroller.getFinalY();
            }
            break;
        case MotionEvent.ACTION_MOVE:
            // 屏幕上触摸点的数量
            if (even.getPointerCount() == 1) {
                // 计算偏移量，必须用 onInterceptTouchEvent 中的坐标，否则会漂移
                int deltaX = (int) (intercept_x - x);
                intercept_x = x;
                int deltaY = (int) (intercept_y - y);
                intercept_y = y;

                // 根据偏移量移动
                scrollBy(deltaX, deltaY);

                requestLayout();
            }
            break;
        case MotionEvent.ACTION_UP:
            // ******************************************** 边界回弹处理
            // ********************************************
            // 当前 View 的坐标
            Point viewInScreen = new Point();
            // 屏幕正中间 view 的坐标，用于回弹的目的坐标
            Point centerView = new Point();

            centerView.x = (int) (Constants.SCREEN_WIDTH/ 2 - viewSize / 2);
            centerView.y = (int) (Constants.SCREEN_HEIGHT / 2 - viewSize / 2);

            // 所有 View 到屏幕中心的距离
            List<Integer> allViewScreenCenter = new ArrayList<Integer>();
            locationsScr.clone();
            for (int i = 0; i < getChildCount(); i++) {
                // 获取 view 相对于屏幕的坐标
                getChildAt(i).getLocationOnScreen(locationsScr);
                viewInScreen.x = locationsScr[0];
                viewInScreen.y = locationsScr[1];
                allViewScreenCenter.add(getDistance(viewInScreen.x,
                        viewInScreen.y, centerView.x, centerView.y));
            }

            int minNum = allViewScreenCenter.get(0);
            int index = 0;
            // 找出离屏幕中心最近的 view
            for (int i = 0; i < allViewScreenCenter.size(); i++)
                if (minNum > allViewScreenCenter.get(i)) {
                    minNum = allViewScreenCenter.get(i);
                    index = i;
                }

            // 再次获取最近 view 的坐标准备回弹到屏幕中心
            getChildAt(index).getLocationOnScreen(locationsScr);
            viewInScreen.x = locationsScr[0];
            viewInScreen.y = locationsScr[1];
            int distance = allViewScreenCenter.get(index);

            // 由于缩放到最小看不见的View 的 (X,Y) 顶点坐标已经发生改变，所以要还原到缩放前 View 的顶点坐标
            if (distance > centerView.x) {
                viewInScreen.x = viewInScreen.x - (viewSize / 2);
                viewInScreen.y = viewInScreen.y - (viewSize / 2);
            }

            // 计算偏移量
            int deltaX = (int) (viewInScreen.x - centerView.x);
            int deltaY = (int) (viewInScreen.y - centerView.y);

            // 回弹到屏幕中心位置；平滑移动；起始点，移动的距离，执行时间
            mScroller.startScroll(getScrollX(), getScrollY(), deltaX, deltaY,
                    600);
            invalidate();

            mTouchState = TOUCH_STATE_REST;
            break;
        }
        return this.detector.onTouchEvent(even);
    }

    int Fling_move = 0;

    /**
     * 手指在触摸屏上迅速移动，并松开的动作
     */
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
            float velocityY) {
        // 随手指 快速拨动的代码
        // MessageUtils.i("velocityX = " + velocityX + ", velocityY = " +
        // velocityY);
        /*
         * int slowX = -(int) velocityX * 3 / 4; int slowY = -(int) velocityY *
         * 3 / 4; mScroller.fling(move_X, move_Y, slowX, slowY, 100, 100, 200,
         * 200); move_X = mScroller.getFinalX(); move_Y = mScroller.getFinalY();
         * computeScroll();
         */
        return false;
    }

    /**
     * 计算两点间的距离
     * 
     * @param start_x
     *            起始点X坐标
     * @param start_y
     *            起始点Y坐标
     * @param last_x
     *            目标点X坐标
     * @param last_y
     *            目标点Y坐标
     * @return 两点间距离，整形
     */
    public int getDistance(int start_x, int start_y, int last_x, int last_y) {
        int x = (last_x - start_x) * (last_x - start_x);
        int y = (last_y - start_y) * (last_y - start_y);
        /** 使用勾股定理返回两点之间的距离 */
        return (int) Math.sqrt(x + y);
    }

    /**
     * 刚刚手指接触到触摸屏的那一刹那，就是触的那一下
     */
    public boolean onDown(MotionEvent e) {
        // MessageUtils.i("onDown");
        return true;
    }

    /**
     * 手指在触摸屏上滑动
     */
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
            float distanceY) {
        // MessageUtils.i("onScroll");
        return false;
    }

    /**
     * 手指离开触摸屏的那一刹那
     */
    public boolean onSingleTapUp(MotionEvent e) {
        // MessageUtils.i("onSingleTapUp");
        return false;
    }

    /**
     * 手指按在触摸屏上，它的时间范围在按下起效，在长按之前
     */
    public void onShowPress(MotionEvent e) {
        // MessageUtils.i("onShowPress");
    }

    /**
     * 手指按在持续一段时间，并且没有松开
     */
    public void onLongPress(MotionEvent e) {
        // MessageUtils.i("onLongPress");
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int size_part = viewSize / 2;
        int child_X = (r - l) / 2;
        int child_Y = (b - t) / 2;

        final int count = getChildCount();
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            child.measure(r - l, b - t);
            switch (i) {
            case 0:// 正中间
                child.layout(child_X - size_part, //
                        child_Y - size_part, //
                        child_X + size_part, //
                        child_Y + size_part);
                break;
            // *******************************第 1
            // 圈******************************
            case 1:// 左边
                child.layout(child_X - size_part - viewSize, //
                        child_Y - size_part, //
                        child_X - size_part, //
                        child_Y + size_part);
                break;
            case 2:// 左上角
                child.layout(child_X - viewSize, //
                        child_Y - size_part - viewSize, //
                        child_X, //
                        child_Y - size_part);
                break;
            case 3:// 右上角
                child.layout(child_X, //
                        child_Y - size_part - viewSize,//
                        child_X + viewSize, //
                        child_Y - size_part);
                break;
            case 4:// 右边
                child.layout(child_X + size_part, //
                        child_Y - size_part, //
                        child_X + size_part + viewSize, //
                        child_Y + size_part);
                break;
            case 5:// 右下角
                child.layout(child_X, //
                        child_Y + size_part, //
                        child_X + viewSize,//
                        child_Y + size_part + viewSize);
                break;
            case 6:// 左下角
                child.layout(child_X - viewSize, //
                        child_Y + size_part, //
                        child_X,//
                        child_Y + size_part + viewSize);
                break;

            // *******************************第 2
            // 圈******************************
            case 7:// 左边
                child.layout(child_X - viewSize * 2 - size_part, //
                        child_Y - size_part, //
                        child_X - size_part - viewSize, //
                        child_Y + size_part);
                break;
            case 8:
                child.layout(child_X - viewSize * 2, //
                        child_Y - size_part - viewSize,//
                        child_X - viewSize, //
                        child_Y - size_part);
                break;
            case 9:// 左上角
                child.layout(child_X - size_part - viewSize, //
                        child_Y - size_part - viewSize * 2, //
                        child_X - size_part, //
                        child_Y - size_part - viewSize);
                break;
            case 10:
                child.layout(child_X - size_part, //
                        child_Y - size_part - viewSize * 2, //
                        child_X + size_part, //
                        child_Y - size_part - viewSize);
                break;
            case 11:// 右上角
                child.layout(child_X + size_part, //
                        child_Y - size_part - viewSize * 2, //
                        child_X + size_part + viewSize, //
                        child_Y - size_part - viewSize);
                break;
            case 12:
                child.layout(child_X + viewSize, //
                        child_Y - size_part - viewSize,//
                        child_X + viewSize * 2, //
                        child_Y - size_part);
                break;
            case 13:// 右边
                child.layout(child_X + size_part + viewSize, //
                        child_Y - size_part,//
                        child_X + size_part + viewSize * 2, //
                        child_Y + size_part);
                break;
            case 14:
                child.layout(child_X + viewSize, //
                        child_Y + size_part, //
                        child_X + viewSize * 2, //
                        child_Y + size_part + viewSize);
                break;
            case 15:// 右下角
                child.layout(child_X + size_part, //
                        child_Y + size_part + viewSize,//
                        child_X + size_part + viewSize, //
                        child_Y + size_part + viewSize * 2);
                break;
            case 16:
                child.layout(child_X - size_part, //
                        child_Y + size_part + viewSize,//
                        child_X + size_part, //
                        child_Y + size_part + viewSize * 2);
                break;
            case 17:// 左下角
                child.layout(child_X - size_part - viewSize, //
                        child_Y + size_part + viewSize, //
                        child_X - size_part, //
                        child_Y + size_part + viewSize * 2);
                break;
            case 18:
                child.layout(child_X - viewSize * 2, //
                        child_Y + size_part, //
                        child_X - viewSize, //
                        child_Y + size_part + viewSize);
                break;

            // *******************************第 3
            // 圈******************************
            case 19:
                child.layout(child_X - viewSize * 3 - size_part, //
                        child_Y - size_part, //
                        child_X - size_part - viewSize * 2, //
                        child_Y + size_part);
                break;
            case 20:
                child.layout(child_X - viewSize * 3, //
                        child_Y - size_part - viewSize,//
                        child_X - viewSize * 2, //
                        child_Y - size_part);
                break;
            case 21:
                child.layout(child_X - viewSize * 2 - size_part, //
                        child_Y - size_part - viewSize * 2, //
                        child_X - size_part - viewSize, //
                        child_Y - size_part - viewSize);
                break;
            case 22:
                child.layout(child_X - viewSize * 2, //
                        child_Y - size_part - viewSize * 3, //
                        child_X - viewSize, //
                        child_Y - size_part - viewSize * 2);
                break;
            case 23:
                child.layout(child_X - viewSize, //
                        child_Y - size_part - viewSize * 3, //
                        child_X, //
                        child_Y - size_part - viewSize * 2);
                break;
            case 24:
                child.layout(child_X, //
                        child_Y - size_part - viewSize * 3, //
                        child_X + viewSize, //
                        child_Y - size_part - viewSize * 2);
                break;
            case 25:
                child.layout(child_X + viewSize, //
                        child_Y - size_part - viewSize * 3, //
                        child_X + viewSize * 2, //
                        child_Y - size_part - viewSize * 2);
                break;
            case 26:
                child.layout(child_X + viewSize + size_part, //
                        child_Y - size_part - viewSize * 2, //
                        child_X + size_part + viewSize * 2, //
                        child_Y - size_part - viewSize);
                break;
            case 27:
                child.layout(child_X + viewSize * 2, //
                        child_Y - size_part - viewSize, //
                        child_X + viewSize * 3, //
                        child_Y - size_part);
                break;
            case 28:
                child.layout(child_X + viewSize * 2 + size_part, //
                        child_Y - size_part, //
                        child_X + viewSize * 3 + size_part, //
                        child_Y + size_part);
                break;
            case 29:
                child.layout(child_X + viewSize * 2, //
                        child_Y + size_part, //
                        child_X + viewSize * 3, //
                        child_Y + size_part + viewSize);
                break;
            case 30:
                child.layout(child_X + viewSize + size_part, //
                        child_Y + size_part + viewSize, //
                        child_X + viewSize * 2 + size_part, //
                        child_Y + size_part + viewSize * 2);
                break;
            case 31:
                child.layout(child_X + viewSize, //
                        child_Y + size_part + viewSize * 2, //
                        child_X + viewSize * 2, //
                        child_Y + size_part + viewSize * 3);
                break;
            case 32:
                child.layout(child_X, //
                        child_Y + size_part + viewSize * 2, //
                        child_X + viewSize, //
                        child_Y + size_part + viewSize * 3);
                break;
            case 33:
                child.layout(child_X - viewSize, //
                        child_Y + size_part + viewSize * 2, //
                        child_X, //
                        child_Y + size_part + viewSize * 3);
                break;
            case 34:
                child.layout(child_X - viewSize * 2, //
                        child_Y + size_part + viewSize * 2, //
                        child_X - viewSize, //
                        child_Y + size_part + viewSize * 3);
                break;
            case 35:
                child.layout(child_X - viewSize * 2 - size_part, //
                        child_Y + size_part + viewSize, //
                        child_X - viewSize - size_part, //
                        child_Y + size_part + viewSize * 2);
                break;
            case 36:
                child.layout(child_X - viewSize * 3, //
                        child_Y + size_part, //
                        child_X - viewSize * 2, //
                        child_Y + size_part + viewSize);
                break;

            // *******************************第 4
            // 圈******************************
            case 37:
                child.layout(child_X - viewSize * 4 - size_part, //
                        child_Y - size_part, //
                        child_X - viewSize * 3 - size_part, //
                        child_Y + size_part);
                break;
            case 38:
                child.layout(child_X - viewSize * 4, //
                        child_Y - size_part - viewSize,//
                        child_X - viewSize * 3, //
                        child_Y - size_part);
                break;
            case 39:
                child.layout(child_X - viewSize * 3 - size_part, //
                        child_Y - size_part - viewSize * 2, //
                        child_X - viewSize * 2 - size_part, //
                        child_Y - size_part - viewSize);
                break;
            case 40:
                child.layout(child_X - viewSize * 3, //
                        child_Y - size_part - viewSize * 3, //
                        child_X - viewSize * 2, //
                        child_Y - size_part - viewSize * 2);
                break;
            case 41:
                child.layout(child_X - viewSize * 2 - size_part, //
                        child_Y - size_part - viewSize * 4, //
                        child_X - viewSize - size_part, //
                        child_Y - size_part - viewSize * 3);
                break;
            case 42:
                child.layout(child_X - viewSize - size_part, //
                        child_Y - size_part - viewSize * 4, //
                        child_X - size_part, //
                        child_Y - size_part - viewSize * 3);
                break;
            case 43:
                child.layout(child_X - size_part, //
                        child_Y - size_part - viewSize * 4, //
                        child_X + size_part, //
                        child_Y - size_part - viewSize * 3);
                break;
            case 44:
                child.layout(child_X + size_part, //
                        child_Y - size_part - viewSize * 4, //
                        child_X + size_part + viewSize, //
                        child_Y - size_part - viewSize * 3);
                break;
            case 45:
                child.layout(child_X + size_part + viewSize, //
                        child_Y - size_part - viewSize * 4, //
                        child_X + size_part + viewSize * 2, //
                        child_Y - size_part - viewSize * 3);
                break;
            default:
                child.setVisibility(View.GONE);
                break;
            }

        }
    }

    /**
     * 是否处于卸载状态标志
     */
    private boolean isUnInstall = false;

    /**
	 * 
	 */
    private long lastClick;

    /**
     * 点击事件
     * 
     * @author kairuili
     */
    class ImageOnClickListener implements OnClickListener {
        public void onClick(View v) {
            String app_name = v.getTag(R.id.app_name).toString();
            String package_name = v.getTag(R.id.package_name).toString();

            // 打开应用
            if (!isUnInstall) {
                // 大于一秒才能通过，防止button多次点击总结
                if (System.currentTimeMillis() - lastClick <= 1000)
                    return;

                // 卸载或打开应用都要停止动画
                stopUserAppAnim();
                WearAppManager.startApplication(mContext, package_name);
            }
            // 卸载应用
            else {
                boolean isUserApp = false;
                // 遍历点击的App是否属于用户应用
                for (int i = 0; i < userAppInfos.size(); i++)
                    if (userAppInfos.get(i).getPackname() == package_name)
                        isUserApp = true;

                // 执行卸载
                if (isUserApp) {
                    // 大于一秒才能通过，防止button多次点击
                    if (System.currentTimeMillis() - lastClick <= 1000)
                        return;

                    // 卸载或打开应用都要停止动画
                    stopUserAppAnim();
                    dialogDeleteApp(app_name, package_name);

                } else
                    // 禁止卸载系统应用！
                    WearCustomMsg.showToast(R.string.systemapp_no_uninstall);
            }
            lastClick = System.currentTimeMillis();
        }
    }

    /**
     * 长按事件
     * 
     * @author kairuili
     */
    class MyOnLongClickListener implements OnLongClickListener {
        public boolean onLongClick(View v) {
            // 只有卸载状态为false才能开启卸载动画
            if (!isUnInstall && 0 != userAppInfos.size()) {
                if (!mRoundedImageView.getIsStopZoom()) {
                    mRoundedImageView.setIsStopZoom(true);
                    // 将每个ImageView 恢复到原大小
                    for (int i = 0; i < getChildCount(); i++) {
                        View child = getChildAt(i);
                        float scaleX = child.getScaleX();

                        // 设置放大动画
                        ScaleAnimation animation = new ScaleAnimation(scaleX,
                                1.0f, scaleX, 1.0f, Animation.RELATIVE_TO_SELF,
                                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        animation.setDuration(300); // 设置动画持续时间

                        child.setAnimation(animation);
                        child.setScaleX(1f);
                        child.setScaleY(1f);
                        animation.startNow();
                        animation.reset();
                    }
                }
                startAppAnim();
                isUnInstall = true;
            } else if (0 == userAppInfos.size())
                WearCustomMsg.showToast(R.string.no_uninstall_application);
            return true;
        }
    }

    /**
     * 保存缩放比例
     */
    private float scale = 0f;

    /**
     * 处理按下状态
     * 
     * @author kairuili
     * 
     */
    class MyTouchClickListener implements OnTouchListener {
        public boolean onTouch(final View view, MotionEvent event) {
            switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // 按下效果，缩小0.9倍，透明 40%
                scale = view.getScaleX();
                view.setScaleX(scale * 0.9f);
                view.setScaleY(scale * 0.9f);
                view.setAlpha(0.4f);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                // 如果缩放已经打开则还原到按下前大小
                if (!mRoundedImageView.getIsStopZoom()) {
                    // 还原 view 状态
                    view.setScaleX(scale);
                    view.setScaleY(scale);
                } else {
                    view.setScaleX(1f);
                    view.setScaleY(1f);
                }
                view.setAlpha(1f);
                break;
            }
            return false;
        }
    }

    /**
     * 卸载应用的全屏Dialog
     * 
     * @param appName
     * @param packName
     */
    @SuppressWarnings("deprecation")
    private void dialogDeleteApp(String appName, final String packName) {
        final Dialog dialog = new Dialog(mContext, R.style.Dialog_Fullscreen);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        // 设置模糊度
        ((Activity) mContext).getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        lp.alpha = 0.8f;// （0.0-1.0）//透明度，黑暗度为lp.dimAmount = 1.0f;
        dialog.getWindow().setAttributes(lp);
        dialog.setContentView(R.layout.widget_delete_dialog);

        // 设置删除app名字
        String sAgeFormat1 = getResources().getString(R.string.delete_app_name);
        TextView textView = (TextView) dialog.findViewById(R.id.tv_appname);
        textView.setText(String.format(sAgeFormat1, appName));
        // 删除应用Button
        dialog.findViewById(R.id.btn_uninstall).setOnClickListener(
                new OnClickListener() {
                    public void onClick(View v) {
                        if (!packName.isEmpty() && mServiceManager != null)
                            mServiceManager.deletePackage(packName);

                        dialog.dismiss();
                        refresh();
                    }
                });
        // 取消 Button
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(
                new OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();
                        refresh();
                    }
                });
        dialog.show();
    }

    /**
     * 外部主动刷新dragViewGroup
     */
    public void refresh() {
        // 只在App数量发生变化才更新 dragViewGroup，+1是因为正中间多了一个 View
        if (getApp_num() != (WearAppInfoProvider.getAllActivity(mContext).size() + 1)) {
            stopUserAppAnim();
            this.removeAllViews();

            if (!getUserAppInfos().isEmpty())
                clearUserAppInfos();

            initView();
        }

        // 只在卸载状态才恢复动画
        if (getIsUnInstall())
            startAppAnim();
    }

    /**
     * 按下返回键
     */
    public void onBack() {
        WearRoundedImageView.isStopZoom = false;
        this.requestLayout();

        // 停止卸载动画
        this.stopUserAppAnim();
        // 修改卸载状态为false
        this.setIsUnInstall(false);
    }

    /**
     * 初始化用户App的摇晃动画，系统App不能卸载
     * 
     * @param pUserApp
     *            用户App 集合
     */
    public void initUserAppAnim(List<WearAppInfo> pUserApp) {
        // 清空 userApp，重新 add
        userApps.clear();
        // 遍历所有App和用户App，包名一致则开启摇晃动画
        for (int i = 0; i < pUserApp.size(); i++)
            for (int j = 0; j < getChildCount(); j++)
                if (pUserApp.get(i).getPackname() == getChildAt(j).getTag(
                        R.id.package_name))
                    userApps.add(j);
    }

    /**
     * 只开启用户App的摇晃动画，系统App不摇晃
     */
    public void startAppAnim() {
        // 添加角标
        WearRoundedImageView.isUninstall = true;
        WearRoundedImageView.app_id = userApps;
        for (int i = 0; i < userApps.size(); i++)
            getChildAt(userApps.get(i)).startAnimation(
                    (i % 2 == 0) ? shake : shake2);
    }

    /**
     * 停止全部view摇晃动画
     */
    public void stopUserAppAnim() {
        // 清除角标
        WearRoundedImageView.isUninstall = false;
        WearRoundedImageView.app_id = userApps;
        for (int i = 0; i < userApps.size(); i++) {
            getChildAt(userApps.get(i)).clearAnimation();
            getChildAt(userApps.get(i)).invalidate();
        }
    }

    public void clearUserAppInfos() {
        userAppInfos.clear();
    }

    public List<WearAppInfo> getUserAppInfos() {
        return userAppInfos;
    }

    public int getApp_num() {
        return app_num;
    }

    public void setIsUnInstall(boolean isUnInstall) {
        this.isUnInstall = isUnInstall;
    }

    public boolean getIsUnInstall() {
        return isUnInstall;
    }

    public int getViewSize() {
        return viewSize;
    }

    private DeviceManagerServiceManager mServiceManager;

    @Override
    public void onConnected(ServiceClient serviceClient) {
        mServiceManager = (DeviceManagerServiceManager) serviceClient
                .getServiceManagerContext();
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {
    }

}
