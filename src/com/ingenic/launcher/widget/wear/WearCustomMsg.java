/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingLauncher Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.wear;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class WearCustomMsg {
    private static Toast mToast;
    private static Handler mHandler = new Handler();
    private static Context mContext;
    /**
     * 显示时间， 默认为800毫秒
     */
    private static int duration = 800;

    /**
     * Log.i()
     * 
     * @param pMessage
     */
    public static void i(String pMessage) {
        Log.i("ingenic", pMessage);
    }

    /**
     * 土司
     * 
     * @param pContext
     * @param pMessage
     */
    @Deprecated
    public static void showToast(Context pContext, String pMessage) {
        Toast.makeText(pContext, pMessage, Toast.LENGTH_SHORT).show();
    }

    /**
     * 土司
     * 
     * @param pContext
     * @param id
     *            资源 id
     */
    @Deprecated
    public static void showToast(Context pContext, int id) {
        Toast.makeText(pContext, pContext.getResources().getString(id),
                Toast.LENGTH_SHORT).show();
    }

    private static Runnable r = new Runnable() {
        public void run() {
            mToast.cancel();
        }
    };

    /**
     * 土司
     * 
     * @param text
     *            显示内容
     */
    public static void showToast(String text) {
        mHandler.removeCallbacks(r);
        if (mToast != null)
            mToast.setText(text);
        else
            mToast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
        mHandler.postDelayed(r, duration);

        mToast.show();
    }

    /**
     * 土司
     * 
     * @param resId
     *            资源 id
     */
    public static void showToast(int resId) {
        showToast(mContext.getResources().getString(resId));
    }

    /**
     * 创建Toast时先做一下判断，如果前面有Toast在显示，只需调用Toast中的setText（）方法将要显示的信息替换即可
     * 
     * @param mContext
     * @param text
     * @param duration
     */
    public static void showToast(Context mContext, String text, int duration) {
        mHandler.removeCallbacks(r);
        if (mToast != null)
            mToast.setText(text);
        else
            mToast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
        mHandler.postDelayed(r, duration);

        mToast.show();
    }

    public static void showToast(Context mContext, int resId, int duration) {
        showToast(mContext, mContext.getResources().getString(resId), duration);
    }

    public static void setContext(Context pContext) {
        mContext = pContext;
    }

    public static void setDuration(int pDuration) {
        duration = pDuration;
    }

}
