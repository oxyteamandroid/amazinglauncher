/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingLauncher Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.wear;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;

/**
 * 提供手机里面安装的所有的应用程序信息
 */
public class WearAppInfoProvider {
    /**
     * 获取所有的安装的应用程序信息
     * 
     * @param pContext
     *            上下文
     * @return
     */
    public static List<WearAppInfo> getAppInfos(Context pContext) {
        PackageManager pm = pContext.getPackageManager();
        // 所有的安装在系统上的应用程序包信息。
        List<PackageInfo> packInfos = pm.getInstalledPackages(0);
        List<WearAppInfo> appInfos = new ArrayList<WearAppInfo>();
        for (PackageInfo packInfo : packInfos) {
            WearAppInfo mAppInfo = new WearAppInfo();
            // packInfo 相当于一个应用程序apk包的清单文件
            String packname = packInfo.packageName;
            Drawable icon = packInfo.applicationInfo.loadIcon(pm);
            String name = packInfo.applicationInfo.loadLabel(pm).toString();
            // 应用程序apk包的路径
            String apkpath = packInfo.applicationInfo.sourceDir;
            File file = new File(apkpath);
            long appSize = file.length();

            int flags = packInfo.applicationInfo.flags;// 应用程序信息的标记 相当于用户提交的答卷
            if ((flags & ApplicationInfo.FLAG_SYSTEM) == 0)
                mAppInfo.setUserApp(true); // 用户程序
            else
                mAppInfo.setUserApp(false); // 系统程序

            if ((flags & ApplicationInfo.FLAG_EXTERNAL_STORAGE) == 0)
                mAppInfo.setInRom(true); // 手机的内存
            else
                mAppInfo.setInRom(false); // 手机外存储设备

            mAppInfo.setPackname(packname);
            mAppInfo.setIcon(icon);
            mAppInfo.setName(name);
            mAppInfo.setAppSize(appSize);
            appInfos.add(mAppInfo);
        }
        return appInfos;
    }

    /**
     * 获取所有有Activity的应用程序信息
     * 
     * @param pContext
     * @return
     */
    public static List<WearAppInfo> getAllActivity(Context pContext) {
        List<WearAppInfo> appInfos = new ArrayList<WearAppInfo>();
        PackageManager mManagerApp = pContext.getPackageManager();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> mAllAppResolve = mManagerApp.queryIntentActivities(
                mainIntent, 0);
        for (ResolveInfo resolveInfo : mAllAppResolve) {
            WearAppInfo mAppInfo = new WearAppInfo();

            ActivityInfo activityInfo = resolveInfo.activityInfo;
            String packname = activityInfo.packageName;
            // 排除自己
            if (pContext.getPackageName().equals(packname))
                continue;
            Drawable icon = activityInfo.loadIcon(mManagerApp);
            String appName = (String) activityInfo.loadLabel(mManagerApp);
            // 应用程序apk包的路径
            String apkpath = activityInfo.applicationInfo.sourceDir;
            File file = new File(apkpath);
            long appSize = file.length();

            int flags = activityInfo.applicationInfo.flags;// 应用程序信息的标记
                                                           // 相当于用户提交的答卷
            if ((flags & ApplicationInfo.FLAG_SYSTEM) == 0)
                mAppInfo.setUserApp(true); // 用户程序
            else
                mAppInfo.setUserApp(false); // 系统程序

            if ((flags & ApplicationInfo.FLAG_EXTERNAL_STORAGE) == 0)
                mAppInfo.setInRom(true); // 手机的内存
            else
                mAppInfo.setInRom(false); // 手机外存储设备

            mAppInfo.setPackname(packname);
            mAppInfo.setIcon(icon);
            mAppInfo.setName(appName);
            mAppInfo.setAppSize(appSize);
            appInfos.add(mAppInfo);
        }
        return appInfos;
    }
}
