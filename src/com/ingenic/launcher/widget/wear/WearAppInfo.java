/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingLauncher Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.wear;

import android.graphics.drawable.Drawable;

/**
 * 应用程序信息的业务bean
 */
public class WearAppInfo {
    private Drawable icon;
    private String name;
    private String packname;
    private boolean inRom;
    private boolean userApp;
    private long appSize;

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackname() {
        return packname;
    }

    public void setPackname(String packname) {
        this.packname = packname;
    }

    public boolean isInRom() {
        return inRom;
    }

    public void setInRom(boolean inRom) {
        this.inRom = inRom;
    }

    public boolean isUserApp() {
        return userApp;
    }

    public void setUserApp(boolean userApp) {
        this.userApp = userApp;
    }

    public long getAppSize() {
        return appSize;
    }

    public void setAppSize(long appSize) {
        this.appSize = appSize;
    }

    @Override
    public String toString() {
        return "AppInfo{" + "name='" + name + '\'' + ", packname='" + packname
                + '\'' + ", inRom=" + inRom + ", userApp=" + userApp
                + ", appSize=" + appSize + '}';
    }
}