/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget;

import java.util.ArrayList;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 * Widget
 * 
 * @author tZhang & ShiGuangHua(Kenny)
 * 
 */
public class WidgetPagerAdapter extends PagerAdapter {

    private ArrayList<View> mPages = new ArrayList<View>();

    public void insert(View view) {
        if (null == view)
            return;

        if (null == mPages)
            mPages = new ArrayList<View>();

        mPages.add(view);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return null == mPages ? 0 : mPages.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        if (null == mPages || mPages.size() <= position)
            return null;

        container.addView(mPages.get(position));
        return mPages.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (null == mPages || mPages.size() <= position)
            return;

        container.removeView(mPages.get(position));
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
