/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget;

import java.util.List;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.ingenic.iwds.appwidget.WidgetHost;
import com.ingenic.iwds.appwidget.WidgetHostView;
import com.ingenic.iwds.appwidget.WidgetManager;
import com.ingenic.iwds.appwidget.WidgetProviderInfo;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.launcher.R;
import com.ingenic.launcher.utils.Constants;
import com.ingenic.launcher.utils.Utils;
import com.ingenic.launcher.widget.health.HealthStepView;

/**
 * Widget
 * 
 * @author tZhang & ShiGuangHua(Kenny)
 * 
 */
public class BottomWidget extends FrameLayout implements
        ServiceClient.ConnectionCallbacks, ViewPager.OnPageChangeListener {

    static final String TAG = BottomWidget.class.getSimpleName();
    private static final int MSG_SHOW_INDICATOR = 0;
    private static final int MSG_HIDE_INDICATOR = 1;
    private static final int MSG_NOTIFICATION_WIDGET_DISPLAY = 2;
    private static final int DEFAULT_INDICATOR_ANIMATION_DURATION = 600;
    private static final int DEFAULT_INDICATOR_HIDE_DELAY_DURATION = 1000;

    private Context mContext;
    private ServiceClient mClient;
    private WidgetManager mManager;

    private WidgetHost mHost;
    private View mContainer;
    private HealthStepView mStepView;
    private OverScrollViewPager mWidgetPager;
    private WidgetPagerAdapter mWidgetAdapter;
    private WidgetHostView mHostView;
    private LinearLayout mIndicator;
    private LinearLayout.LayoutParams mPointParams;
    private int indicatorIndex = 0;
    private int preIndicatorIndex = 0;
    private Intent mIntentWidget;

    public BottomWidget(Context context) {
        this(context, null);
    }

    public BottomWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BottomWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        mIntentWidget = new Intent(
                Constants.COM_INGENIC_LAUNCHER_DISPLAY_WIDGET_ACTION);
        initView();

        mClient = new ServiceClient(mContext,
                ServiceManagerContext.SERVICE_LOCAL_WIDGET, this);
        mClient.connect();
    }

    private void initView() {
        mContainer = LayoutInflater.from(mContext).inflate(
                R.layout.widget_layout, this, false);

        mWidgetPager = (OverScrollViewPager) mContainer
                .findViewById(R.id.widget_pager);
        mWidgetAdapter = new WidgetPagerAdapter();
        mWidgetPager.setAdapter(mWidgetAdapter);

        mIndicator = (LinearLayout) mContainer
                .findViewById(R.id.widget_indicator);

        int pointSize = Utils.dp2Px(mContext,
                Constants.WIDGET_INDICATOR_FONT_SIZE);
        mPointParams = new LinearLayout.LayoutParams(pointSize, pointSize);

        addView(mContainer, new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        mManager = (WidgetManager) serviceClient.getServiceManagerContext();
        mHost = new WidgetHost(mManager);
        mHost.startListening();

        List<WidgetProviderInfo> infos = mManager.getInstalledProviders();
        int widgetSize = Utils.getWidgetSize(mContext);

        View point = new View(mContext);
        point.setBackgroundResource(R.drawable.widget_indicator_dot_selected);
        mIndicator.addView(point, mPointParams);

        int widgetLibraryCount = 0;
        if (infos == null || infos.size() == 0) {
            for (int index = 0; index < widgetSize; index++) {
                if (!Utils.getWhetherWidget(
                        Utils.getWidgetClassName(index, mContext), mContext)) {
                    mWidgetAdapter.insert(Utils.getLocalLibraryWidget(
                            Utils.getWidgetClassName(index, mContext),
                            mContext));
                    widgetLibraryCount++;
                }
            }
            setNotSelected(widgetLibraryCount);
            if (widgetLibraryCount > 2) {
                mWidgetPager.setOffscreenPageLimit(widgetLibraryCount - 1);
            }

            return;
        }

        // 获取健康的library
        View view = Utils.getHealthLibraryView(mContext);
        if (view != null) {
            mStepView = (HealthStepView) view.findViewById(R.id.steps_view);
        }

        for (int index = 0; index < widgetSize; index++) {
            String packageName = Utils.getWidgetClassName(index, mContext);
            if (Utils.getWhetherWidget(packageName, mContext)) {
                WidgetProviderInfo info = null;
                for (int i = 0; i < infos.size(); i++) {
                    info = infos.get(i);
                    if (info.provider.getPackageName().equals(packageName)) {
                        mHostView = mHost.createView(mContext, info);
                        mWidgetAdapter.insert(mHostView);
                        break;
                    }
                }
            } else {
                mWidgetAdapter.insert(Utils.getLocalLibraryWidget(
                        Utils.getWidgetClassName(index, mContext), mContext));
            }
        }

        mWidgetPager.setOnPageChangeListener(this);
        if (infos.size() > 2) {
            mWidgetPager.setOffscreenPageLimit(infos.size() - 1);
        }

        if (infos.size() < 1)
            return;

        int count = mWidgetAdapter.getCount();
        if (count > 1) {
            setNotSelected(count);
        }
        setWidgetPostionNotifi(Utils.getCurrentWidgetPostion());
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {

    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {

    }

    @Override
    public void onPageScrollStateChanged(int status) {
        switch (status) {
        case ViewPager.SCROLL_STATE_IDLE:
            mHandler.removeMessages(MSG_HIDE_INDICATOR);
            mHandler.sendEmptyMessageDelayed(MSG_HIDE_INDICATOR,
                    DEFAULT_INDICATOR_HIDE_DELAY_DURATION);
            break;
        default:
            break;
        }
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        if (null != mIndicator && mIndicator.getAlpha() < 1.0f) {
            mHandler.removeMessages(MSG_HIDE_INDICATOR);
            mIndicator.setAlpha(1.0f);
        }
    }

    @Override
    public void onPageSelected(int position) {
        preIndicatorIndex = indicatorIndex;
        indicatorIndex = position;
        mIndicator.getChildAt(preIndicatorIndex).setBackgroundResource(
                R.drawable.widget_indicator_dot_normal);
        mIndicator.getChildAt(indicatorIndex).setBackgroundResource(
                R.drawable.widget_indicator_dot_selected);
        setWidgetPostionNotifi(position);
    }

    private void setWidgetPostionNotifi(int mPosition) {
        mHandler.removeMessages(MSG_NOTIFICATION_WIDGET_DISPLAY);
        Message msg = new Message();
        msg.arg1 = mPosition;
        msg.what = MSG_NOTIFICATION_WIDGET_DISPLAY;
        mHandler.sendMessageDelayed(msg, DEFAULT_INDICATOR_ANIMATION_DURATION);
    }

    private void setNotSelected(int count) {
        for (int index = 0; index < count - 1; index++) {
            View points = new View(mContext);
            mPointParams.setMargins(Utils.dp2Px(mContext, 2), 0, 0, 0);
            points.setBackgroundResource(R.drawable.widget_indicator_dot_normal);
            mIndicator.addView(points, mPointParams);
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_SHOW_INDICATOR:
                resetIndicator();
                break;
            case MSG_HIDE_INDICATOR:
                setIndicatorAnim(false);
                break;
            case MSG_NOTIFICATION_WIDGET_DISPLAY:
                Utils.setCurrentWidgetPosition(msg.arg1);
                mIntentWidget.putExtra(Constants.COM_INGENIC_WIDGET_DISPLAY_FLAG,
                        Utils.getWidgetClassName(msg.arg1, mContext));
                mContext.sendBroadcast(mIntentWidget);
                break;
            default:
                break;
            }
        };
    };

    public void resetIndicator() {
        mHandler.removeMessages(MSG_HIDE_INDICATOR);
        setIndicatorAnim(true);
        mHandler.sendEmptyMessageDelayed(MSG_HIDE_INDICATOR,
                DEFAULT_INDICATOR_ANIMATION_DURATION
                        + DEFAULT_INDICATOR_HIDE_DELAY_DURATION);
    }

    private void setIndicatorAnim(boolean show) {
        if (null == mIndicator || mIndicator.getChildCount() <= 1)
            return;

        ValueAnimator animator = ValueAnimator.ofFloat(show ? 0f : 1.0f,
                show ? 1.0f : 0f);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float alpha = (Float) animation.getAnimatedValue();
                mIndicator.setAlpha(alpha);
            }
        });

        animator.setDuration(DEFAULT_INDICATOR_ANIMATION_DURATION).start();
    }

    public void startStepAnim() {
        if (null == mStepView || null == mWidgetPager
                || mWidgetPager.getCurrentItem() != 0)
            return;
        mStepView.startAnim();
    }

    public void resetStep() {
        if (null == mStepView || null == mWidgetPager
                || mWidgetPager.getCurrentItem() != 0)
            return;
        mStepView.endAnim();
    }

}
