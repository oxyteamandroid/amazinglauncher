/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.health;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Bitmap.Config;

/**
 * CircleImage
 * 
 * @author Shi Guanghua
 * 
 */
public class CircleImage {
    private static final int scale = 10;
    private int rightTop = 0xffeca756;
    private int leftBottom = 0xffff6000;

    private Paint paintArrow;
    private Paint paintCircle;
    private Paint paintBakCircle;

    private int paintWidth = 40;
    private int smallPaintWidth = paintWidth / 6;// 7;
    private LinearGradient mShader;
    private RectF rect;

    private int width = 200;
    private int height = 200;

    private int sweepAngle = 1;
    private int halfPaintWidth = paintWidth / 2;

    public void setSweepAngle(int value) {
        value = value < 1 ? 1 : value;
        sweepAngle = value;
    }

    public int getSweepAngle() {
        return sweepAngle;
    }

    public void setPaintWidth(int value) {
        paintWidth = value;
        halfPaintWidth = paintWidth / 2;

        smallPaintWidth = paintWidth / 6;
        paintBakCircle.setStrokeWidth(paintWidth);
        paintCircle.setStrokeWidth(paintWidth);
        paintArrow.setStrokeWidth(smallPaintWidth);
    }

    private void initPaintBakCircle() {
        paintBakCircle = new Paint();
        paintBakCircle.setColor(Color.GRAY);
        paintBakCircle.setStrokeWidth(paintWidth);
        SetCommonPain(paintBakCircle);
    }

    private void initPaintArrow() {
        paintArrow = new Paint();
        paintArrow.setColor(Color.WHITE);
        paintArrow.setStrokeWidth(smallPaintWidth);
        SetCommonPain(paintArrow);
    }

    private void initPaintCircle() {
        paintCircle = new Paint();
        paintCircle.setStrokeWidth(paintWidth);
        SetCommonPain(paintCircle);
    }

    public CircleImage() {
        initPaintArrow();
        initPaintCircle();
        initPaintBakCircle();
    }

    public void setShader(int left, int right) {
        leftBottom = left;
        rightTop = right;

        mShader = new LinearGradient(0, height + paintWidth,
                width + paintWidth, 0, new int[] { leftBottom, rightTop },
                null, Shader.TileMode.REPEAT);
        paintCircle.setShader(mShader);
    }

    public Canvas getImage(Canvas canvas) {

        int width = getWidth() - paintWidth;
        int height = getHeight() - paintWidth;

        if (null == mShader) {
            mShader = new LinearGradient(0, height + halfPaintWidth, width
                    + halfPaintWidth, 0, new int[] { leftBottom, rightTop },
                    null, Shader.TileMode.REPEAT);
            paintCircle.setShader(mShader);
        }

        // background
        int radiusbak = width / 2;
        int offsetYbak = radiusbak + halfPaintWidth;
        int offsetXbak = radiusbak + halfPaintWidth;
        canvas.drawCircle(offsetYbak, offsetXbak, radiusbak, paintBakCircle);

        // percent circle
        if (null == rect)
            rect = new RectF(halfPaintWidth, halfPaintWidth, width
                    + halfPaintWidth, height + halfPaintWidth);
        canvas.drawArc(rect, 270, sweepAngle, false, paintCircle);

        int radius = paintWidth / 4;
        int offsetX = getWidth() / 2;
        int offsetY = paintWidth / 2;

        // arrow
        canvas.drawLine(offsetX, offsetY - radius, radius + offsetX, offsetY,
                paintArrow);
        canvas.drawLine(offsetX, radius + offsetY, radius + offsetX, offsetY,
                paintArrow);

        int dotX = offsetXbak;
        int dotY = offsetYbak;
        int dotRadius = radiusbak;

        double radians = Math.toRadians(sweepAngle - 90);
        float minDotX = (float) (dotX + dotRadius * Math.sin(radians));
        float minDotY = (float) (dotY + dotRadius * Math.cos(radians));

        // circle dynamic
        canvas.drawCircle(minDotY, minDotX, radius, paintArrow);

        // circle
        // canvas.drawCircle(offsetY, offsetX, radius, paintArrow);

        return canvas;
    }

    public Bitmap getImage() {

        Bitmap bmp = Bitmap.createBitmap(getWidth(), getHeight(),
                Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        getImage(canvas);
        return bmp;
    }

    private void SetCommonPain(Paint paint) {
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
    }

    public void setWidth(int value) {
        width = value;
        setPaintWidth(value / scale);
    }

    public void setHeight(int value) {
        height = value;
        setPaintWidth(value / scale);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
