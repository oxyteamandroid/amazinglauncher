/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *   
 *  Elf/AmazingHealth Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.health;

import java.text.DecimalFormat;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.launcher.R;

/**
 * HealthStepView
 * 
 * @author Shi Guanghua
 * 
 */
public class HealthStepView extends LinearLayout {

    private static final String TAG = HealthStepView.class.getSimpleName();

    protected static final String[] projection = new String[] { "id", "steps", "date" };

    private static Uri sUri = Uri.parse("content://com.ingenic.providers.stepsprovider");

    private static int target = 100;
    private static int steps;
    private static int angle;
    private static float calorie;

    private CircleImage mCircleImage;

    private TextView mTarget;
    private TextView mSteps;
    private TextView mCalorie;

    private Handler mHandler;

    private ContentResolver resolver;
    private ContentObserver observer;

    private ObjectAnimator mObjectAnimator;

    protected boolean isStart;

    public HealthStepView(Context context) {
        this(context, null);
    }

    public HealthStepView(final Context context, AttributeSet attrs) {
        super(context, attrs);
        mCircleImage = new CircleImage();
        setGravity(Gravity.CENTER);
        setOrientation(VERTICAL);

        mTarget = new TextView(context);
        mSteps = new TextView(context);
        mCalorie = new TextView(context);

        int margin = dp2px(3);
        float textSmallSize = sp2px(13);
        float textMediumSize = sp2px(18);

        mTarget.setTextSize(textSmallSize);
        mSteps.setTextSize(textMediumSize);
        mCalorie.setTextSize(textSmallSize);

        mTarget.setTextColor(getResources().getColor(android.R.color.white));
        mSteps.setTextColor(0xffff6000);
        mCalorie.setTextColor(getResources().getColor(android.R.color.white));

        LinearLayout.LayoutParams stepLp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        stepLp.setMargins(margin, margin, margin, margin);
        mTarget.setLayoutParams(stepLp);
        mSteps.setLayoutParams(stepLp);
        mCalorie.setLayoutParams(stepLp);

        addView(mTarget);
        addView(mSteps);
        addView(mCalorie);

        mHandler = new Handler();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        initModel();
        refresh();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        IwdsLog.d(TAG, "unregisterContentObserver");
        resolver.unregisterContentObserver(observer);
    }

    private void initModel() {
        resolver = getContext().getContentResolver();
        IwdsLog.d(TAG, "registerContentObserver1");
        observer = new ContentObserver(mHandler) {
            @Override
            public void onChange(boolean selfChange) {
                super.onChange(selfChange);
                IwdsLog.d(TAG, "onChange");
                refresh();
            }
        };
        resolver.registerContentObserver(sUri, true, observer);
    }

    private void refresh() {
        querySteps();
        fillView();
        if (isStart) {
            setSweepAngle(angle);
        }
    }

    private void setSweepAngle(int value) {
        mCircleImage.setSweepAngle(value);
        invalidate();
    }

    private void fillView() {
        mTarget.setText(getResources().getString(R.string.step_target, target));
        mSteps.setText(getResources().getString(R.string.step_steps, steps));
        // mCalorie.setText(new DecimalFormat("0.0").format(calorie) + "cal");
        mCalorie.setText(getResources().getString(R.string.step_calorie, new DecimalFormat("0.0").format(calorie)));
    }

    private void querySteps() {
        if (null == resolver)
            return;
        Cursor cursor = resolver.query(sUri, projection, null, null, null);
        steps = angle = 0;
        calorie = 0F;

        if (null != cursor && cursor.moveToFirst()) {
            steps = cursor.getInt(cursor.getColumnIndex("steps"));
            IwdsLog.d(TAG, "onChange " + steps);
            cursor.close();
            calorie = calculateCalorie(steps);

            float i = 0 == target ? 0 : (360 * steps / target);
            int f = (int) i;
            angle = f % 361;
        }
    }

    @SuppressLint("NewApi")
    public void startAnim() {

        IwdsLog.d(TAG, "startAnim");
        if (null != mObjectAnimator && mObjectAnimator.isRunning())
            return;

        isStart = false;
        final int sweepAngle = angle;

        mObjectAnimator = ObjectAnimator.ofInt(this, "anim", 0, sweepAngle).setDuration(500);
        mObjectAnimator.start();
        mObjectAnimator.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (Integer) animation.getAnimatedValue();
                setSweepAngle(value);
                IwdsLog.d(TAG, "value: " + value + "sweepAngle: " + sweepAngle + "value: " + value);
                if (value == sweepAngle) {
                    mObjectAnimator = null;
                    isStart = true;
                    refresh();
                }
            }
        });
    }

    public void endAnim() {
        IwdsLog.d(TAG, "endAnim");
        if (null != mObjectAnimator && mObjectAnimator.isRunning())
            mObjectAnimator.cancel();
        isStart = false;
        setSweepAngle(0);
    }

    public void setShader(int left, int right) {
        mCircleImage.setShader(left, right);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mCircleImage.setWidth(getWidth());
        mCircleImage.setHeight(getHeight());
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        drawCircle(canvas);
        super.dispatchDraw(canvas);
    }

    public void drawCircle(Canvas canvas) {
        mCircleImage.setWidth(getWidth());
        mCircleImage.setHeight(getHeight());
        mCircleImage.getImage(canvas);
    }

    // ========================================================================================================================================================================

    public static float calculateCalorie(float steps) {
        float stepsmin;

        int height = 180;
        int weight = 150;
        stepsmin = 70;

        double th = 0;
        double tw = 0;
        if ((height) >= 175)
            th = 175;
        else
            th = 0.8 * (height) + 40;
        if ((weight) >= 65)
            tw = 65;
        else
            tw = 0.4 * (weight) + 40;

        double temp = (0.43 * (height) + 0.57 * (weight) + 0.26 * stepsmin + 0.92 * 10/* min */- 108.44 * th / 175 * tw / 65) / 10 / 60 * steps;
        if (temp < 0)
            temp = 0;

        return (float) temp;
    }

    public int dp2px(float dipValue) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    public float sp2px(float spValue) {
        final float fontScale = getResources().getDisplayMetrics().scaledDensity;
        return (spValue * fontScale + 0.5f);
    }

}