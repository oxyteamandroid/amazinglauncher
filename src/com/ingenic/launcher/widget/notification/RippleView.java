/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/iwds-ui-jar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.notification;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Region;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.FrameLayout;

import com.ingenic.launcher.R;

@SuppressLint("ClickableViewAccessibility")
public class RippleView extends FrameLayout {
    static final String TAG = RippleView.class.getSimpleName();

    /**
     * 最大半径, 值小于0表示覆盖整个view
     */
    private static final float RIPPLE_RADIUS = -1f;
    /**
     * 半径快速扩展的持续时长
     */
    private static final long RIPPLE_FAST_DURATION = 250L;
    /**
     * 半径慢速扩展的持续时长
     */
    private static final long RIPPLE_SLOW_DURATION = 1000L;
    /**
     * 半径快速扩展的加速度, 值小于-0.5表示减速, 值大于0.5表示加速, -0.5到0.5之间表示匀速
     */
    private static final float RIPPLE_FAST_ACCELERATION = -0.8f;
    /**
     * 半径慢速扩展的加速度, 值小于-0.5表示减速, 值大于0.5表示加速, -0.5到0.5之间表示匀速
     */
    private static final float RIPPLE_SLOW_ACCELERATION = 0.6f;
    /**
     * 最大透明度
     */
    private static final float RIPPLE_ALPHA = 0.2f;
    /**
     * 透明度淡入的持续时长
     */
    private static final long RIPPLE_FADE_IN_DURATION = 50L;
    /**
     * 透明度淡出的持续时长
     */
    private static final long RIPPLE_FADE_OUT_DURATION = 100L;
    /**
     * 透明度淡入的加速度, 值小于-0.5表示减速, 值大于0.5表示加速, -0.5到0.5之间表示匀速
     */
    private static final float RIPPLE_FADE_IN_ACCELERATION = 0f;
    /**
     * 透明度淡出的加速度, 值小于-0.5表示减速, 值大于0.5表示加速, -0.5到0.5之间表示匀速
     */
    private static final float RIPPLE_FADE_OUT_ACCELERATION = 0f;
    /**
     * 半径快速扩展的颜色
     */
    private static final int RIPPLE_FAST_COLOR = Color.GRAY;
    /**
     * 半径慢速扩展的颜色
     */
    private static final int RIPPLE_SLOW_COLOR = Color.GRAY;
    /**
     * 慢速扩展需要的长按延迟
     */
    private static final long RIPPLE_SLOW_DELAY = 100;
    /**
     * 是否在一次动画结束后才能触发下一次动画
     */
    private static final boolean RIPPLE_ONESHOT = true;
    /**
     * 是否在动画结束后才触发监听事件
     */
    private static final boolean RIPPLE_LAZY_LISTEN = true;
    /**
     * 左边距
     */
    private static final int RIPPLE_PADDING_LEFT = 0;
    /**
     * 上边距
     */
    private static final int RIPPLE_PADDING_TOP = 0;
    /**
     * 右边距
     */
    private static final int RIPPLE_PADDING_RIGHT = 0;
    /**
     * 下边距
     */
    private static final int RIPPLE_PADDING_BOTTOM = 0;

    private float mRippleRadius;
    private long mRippleFastDuration;
    private long mRippleSlowDuration;
    private float mRippleFastAcceleration;
    private float mRippleSlowAcceleration;
    private float mRippleAlpha;
    private long mRippleFadeInDuration;
    private long mRippleFadeOutDuration;
    private float mRippleFadeInAcceleration;
    private float mRippleFadeOutAcceleration;
    private int mRippleFastColor;
    private int mRippleSlowColor;
    private long mRippleSlowDelay;
    private boolean mRippleOneshot;
    private boolean mRippleLazyListen;
    private int mRipplePaddingLeft;
    private int mRipplePaddingTop;
    private int mRipplePaddingRight;
    private int mRipplePaddingBottom;

    private Set<RippleEvent> mRippleEvents;
    private int mLayoutLeft;
    private int mLayoutTop;
    private int mLayoutRight;
    private int mLayoutBottom;
    private RippleSlowRunnable mRippleSlowRunnable;
    private LongClickRunnable mLongClickRunnable;
    private boolean mTouchDown;
    private OnRippleCompleteListener mInternalOnRippleCompleteListener;

    public RippleView(Context context) {
        super(context);
        initJava();
        init();
    }

    public RippleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initXml(attrs);
        init();
    }

    public RippleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initXml(attrs);
        init();
    }

    private void initJava() {
        mRippleRadius = RIPPLE_RADIUS;
        mRippleFastDuration = RIPPLE_FAST_DURATION;
        mRippleSlowDuration = RIPPLE_SLOW_DURATION;
        mRippleFastAcceleration = RIPPLE_FAST_ACCELERATION;
        mRippleSlowAcceleration = RIPPLE_SLOW_ACCELERATION;
        mRippleAlpha = RIPPLE_ALPHA;
        mRippleFadeInDuration = RIPPLE_FADE_IN_DURATION;
        mRippleFadeOutDuration = RIPPLE_FADE_OUT_DURATION;
        mRippleFadeInAcceleration = RIPPLE_FADE_IN_ACCELERATION;
        mRippleFadeOutAcceleration = RIPPLE_FADE_OUT_ACCELERATION;
        mRippleFastColor = RIPPLE_FAST_COLOR;
        mRippleSlowColor = RIPPLE_SLOW_COLOR;
        mRippleSlowDelay = RIPPLE_SLOW_DELAY;
        mRippleOneshot = RIPPLE_ONESHOT;
        mRippleLazyListen = RIPPLE_LAZY_LISTEN;
        mRipplePaddingLeft = RIPPLE_PADDING_LEFT;
        mRipplePaddingTop = RIPPLE_PADDING_TOP;
        mRipplePaddingLeft = RIPPLE_PADDING_LEFT;
        mRipplePaddingBottom = RIPPLE_PADDING_BOTTOM;
    }

    private void initXml(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs,
                R.styleable.RippleView);
        mRippleRadius = ta.getFloat(R.styleable.RippleView_ripple_radius,
                RIPPLE_RADIUS);
        mRippleFastDuration = ta.getInteger(
                R.styleable.RippleView_ripple_fast_duration,
                (int) RIPPLE_FAST_DURATION);
        mRippleSlowDuration = ta.getInteger(
                R.styleable.RippleView_ripple_slow_duration,
                (int) RIPPLE_SLOW_DURATION);
        mRippleFastAcceleration = ta.getFloat(
                R.styleable.RippleView_ripple_fast_acceleration,
                RIPPLE_FAST_ACCELERATION);
        mRippleSlowAcceleration = ta.getFloat(
                R.styleable.RippleView_ripple_slow_acceleration,
                RIPPLE_SLOW_ACCELERATION);
        mRippleAlpha = ta.getFloat(R.styleable.RippleView_ripple_alpha,
                RIPPLE_ALPHA);
        mRippleFadeInDuration = ta.getInteger(
                R.styleable.RippleView_ripple_fade_in_duration,
                (int) RIPPLE_FADE_IN_DURATION);
        mRippleFadeOutDuration = ta.getInteger(
                R.styleable.RippleView_ripple_fade_out_duration,
                (int) RIPPLE_FADE_OUT_DURATION);
        mRippleFadeInAcceleration = ta.getFloat(
                R.styleable.RippleView_ripple_fade_in_acceleration,
                RIPPLE_FADE_IN_ACCELERATION);
        mRippleFadeOutAcceleration = ta.getFloat(
                R.styleable.RippleView_ripple_fade_out_acceleration,
                RIPPLE_FADE_OUT_ACCELERATION);
        mRippleFastColor = ta.getColor(
                R.styleable.RippleView_ripple_fast_color, RIPPLE_FAST_COLOR);
        mRippleSlowColor = ta.getColor(
                R.styleable.RippleView_ripple_slow_color, RIPPLE_SLOW_COLOR);
        mRippleSlowDelay = ta.getInteger(
                R.styleable.RippleView_ripple_slow_delay,
                (int) RIPPLE_SLOW_DELAY);
        mRippleOneshot = ta.getBoolean(R.styleable.RippleView_ripple_oneshot,
                RIPPLE_ONESHOT);
        mRippleLazyListen = ta.getBoolean(
                R.styleable.RippleView_ripple_lazy_listen, RIPPLE_LAZY_LISTEN);
        mRipplePaddingLeft = ta
                .getDimensionPixelSize(
                        R.styleable.RippleView_ripple_padding_left,
                        RIPPLE_PADDING_LEFT);
        mRipplePaddingTop = ta.getDimensionPixelSize(
                R.styleable.RippleView_ripple_padding_top, RIPPLE_PADDING_TOP);
        mRipplePaddingRight = ta.getDimensionPixelSize(
                R.styleable.RippleView_ripple_padding_right,
                RIPPLE_PADDING_RIGHT);
        mRipplePaddingBottom = ta.getDimensionPixelSize(
                R.styleable.RippleView_ripple_padding_bottom,
                RIPPLE_PADDING_BOTTOM);
        ta.recycle();
    }

    private void init() {
        mRippleEvents = new HashSet<RippleEvent>();

        setWillNotDraw(false);
        setClickable(true);
    }

    public float getRippleRadius() {
        return mRippleRadius;
    }

    public long getRippleFastDuration() {
        return mRippleFastDuration;
    }

    public long getRippleSlowDuration() {
        return mRippleSlowDuration;
    }

    public float getRippleFastAcceleration() {
        return mRippleFastAcceleration;
    }

    public float getRippleSlowAcceleration() {
        return mRippleSlowAcceleration;
    }

    public float getRippleAlpha() {
        return mRippleAlpha;
    }

    public long getRippleFadeInDuration() {
        return mRippleFadeInDuration;
    }

    public long getRippleFadeOutDuration() {
        return mRippleFadeOutDuration;
    }

    public float getRippleFadeInAcceleration() {
        return mRippleFadeInAcceleration;
    }

    public float getRippleFadeOutAcceleration() {
        return mRippleFadeOutAcceleration;
    }

    public int getRippleFastColor() {
        return mRippleFastColor;
    }

    public int getRippleSlowColor() {
        return mRippleSlowColor;
    }

    public long getRippleSlowDelay() {
        return mRippleSlowDelay;
    }

    public boolean ismRippleOneshot() {
        return mRippleOneshot;
    }

    public boolean ismRippleLazyListen() {
        return mRippleLazyListen;
    }

    public int getRipplePaddingLeft() {
        return mRipplePaddingLeft;
    }

    public int getRipplePaddingTop() {
        return mRipplePaddingTop;
    }

    public int getRipplePaddingRight() {
        return mRipplePaddingRight;
    }

    public int getRipplePaddingBottom() {
        return mRipplePaddingBottom;
    }

    public void setRippleRadius(float rippleRadius) {
        mRippleRadius = rippleRadius;
    }

    public void setRippleFastDuration(long rippleFastDuration) {
        mRippleFastDuration = rippleFastDuration;
    }

    public void setRippleSlowDuration(long rippleSlowDuration) {
        mRippleSlowDuration = rippleSlowDuration;
    }

    public void setRippleFastAcceleration(float rippleFastAcceleration) {
        mRippleFastAcceleration = rippleFastAcceleration;
    }

    public void setRippleSlowAcceleration(float rippleSlowAcceleration) {
        mRippleSlowAcceleration = rippleSlowAcceleration;
    }

    public void setRippleAlpha(float rippleAlpha) {
        mRippleAlpha = rippleAlpha;
    }

    public void setRippleFadeInDuration(long rippleFadeInDuration) {
        mRippleFadeInDuration = rippleFadeInDuration;
    }

    public void setRippleFadeOutDuration(long rippleFadeOutDuration) {
        mRippleFadeOutDuration = rippleFadeOutDuration;
    }

    public void setRippleFadeInAcceleration(float rippleFadeInAcceleration) {
        mRippleFadeInAcceleration = rippleFadeInAcceleration;
    }

    public void setRippleFadeOutAcceleration(float rippleFadeOutAcceleration) {
        mRippleFadeOutAcceleration = rippleFadeOutAcceleration;
    }

    public void setRippleFastColor(int rippleFastColor) {
        mRippleFastColor = rippleFastColor;
    }

    public void setRippleSlowColor(int rippleSlowColor) {
        mRippleSlowColor = rippleSlowColor;
    }

    public void setRippleSlowDelay(long rippleSlowDelay) {
        mRippleSlowDelay = rippleSlowDelay;
    }

    public void setRippleOneshot(boolean rippleOneshot) {
        mRippleOneshot = rippleOneshot;
    }

    public void setRippleLazyListen(boolean rippleLazyListen) {
        mRippleLazyListen = rippleLazyListen;
    }

    public void setRipplePaddingLeft(int ripplePaddingLeft) {
        mRipplePaddingLeft = ripplePaddingLeft;
    }

    public void setRipplePaddingTop(int ripplePaddingTop) {
        mRipplePaddingTop = ripplePaddingTop;
    }

    public void setRipplePaddingRight(int ripplePaddingRight) {
        mRipplePaddingRight = ripplePaddingRight;
    }

    public void setRipplePaddingBottom(int ripplePaddingBottom) {
        mRipplePaddingBottom = ripplePaddingBottom;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
            int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (!changed) {
            return;
        }

        mLayoutLeft = left;
        mLayoutTop = top;
        mLayoutRight = right;
        mLayoutBottom = bottom;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        if (mRippleEvents.isEmpty()) {
            return;
        }

        canvas.save();

        int width = canvas.getWidth();
        int height = canvas.getHeight();
        canvas.clipRect(0, 0, width, height);
        canvas.clipRect(mRipplePaddingLeft, mRipplePaddingTop, width
                - mRipplePaddingRight, height - mRipplePaddingBottom,
                Region.Op.INTERSECT);

        Iterator<RippleEvent> iterator = mRippleEvents.iterator();
        while (iterator.hasNext()) {
            RippleEvent event = iterator.next();

            if (event.completed) {
                iterator.remove();

                if (mInternalOnRippleCompleteListener != null) {
                    mInternalOnRippleCompleteListener.onRippleComplete(this);
                    mInternalOnRippleCompleteListener = null;
                }
            } else {
                event.frame();
                canvas.drawCircle(event.x, event.y, event.radius, event.paint);
            }
        }

        canvas.restore();

        invalidate();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return !(getParent() instanceof AdapterView) && onTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN: {
            mTouchDown = true;

            if (mRippleSlowRunnable == null) {
                mRippleSlowRunnable = new RippleSlowRunnable(event.getX(),
                        event.getY());
                postDelayed(mRippleSlowRunnable, mRippleSlowDelay);
            }
            if (mLongClickRunnable == null) {
                mLongClickRunnable = new LongClickRunnable();
                postDelayed(mLongClickRunnable,
                        ViewConfiguration.getLongPressTimeout());
            }

            break;
        }
        case MotionEvent.ACTION_UP: {
            mTouchDown = false;

            if (mRippleSlowRunnable != null) {
                removeCallbacks(mRippleSlowRunnable);
                if (!mRippleSlowRunnable.executed) {
                    createRippleEvent(event.getX(), event.getY(), false);
                }
                mRippleSlowRunnable = null;
            }
            if (mLongClickRunnable != null) {
                removeCallbacks(mLongClickRunnable);
                if (!mLongClickRunnable.executed) {
                    if (mRippleLazyListen) {
                        if (mInternalOnRippleCompleteListener == null) {
                            mInternalOnRippleCompleteListener = new OnRippleCompleteListener() {
                                @Override
                                public void onRippleComplete(
                                        RippleView rippleView) {
                                    handleClickEvent(false);
                                }
                            };
                        }
                    } else {
                        handleClickEvent(false);
                    }
                }
                mLongClickRunnable = null;
            }

            break;
        }
        case MotionEvent.ACTION_CANCEL: {
            mTouchDown = false;

            if (mRippleSlowRunnable != null) {
                removeCallbacks(mRippleSlowRunnable);
                mRippleSlowRunnable = null;
            }
            if (mLongClickRunnable != null) {
                removeCallbacks(mLongClickRunnable);
                mLongClickRunnable = null;
            }

            break;
        }
        }
        return super.onTouchEvent(event);
    }

    public void createRipple(float x, float y) {
        createRippleEvent(x, y, false);
    }

    private void createRippleEvent(float x, float y, boolean slow) {
        if (!isEnabled()) {
            return;
        }

        if (mRippleOneshot) {
            if (mRippleEvents.size() > 0) {
                return;
            }
        }

        RippleEvent rippleEvent = new RippleEvent(x, y, slow);
        mRippleEvents.add(rippleEvent);

        invalidate();
    }

    @SuppressWarnings("rawtypes")
    private void handleClickEvent(boolean longClick) {
        if (getParent() instanceof AdapterView) {
            AdapterView adapterView = (AdapterView) getParent();
            int position = adapterView.getPositionForView(this);
            long id = adapterView.getItemIdAtPosition(position);

            if (longClick) {
                AdapterView.OnItemLongClickListener listener = adapterView
                        .getOnItemLongClickListener();
                if (listener != null) {
                    listener.onItemLongClick(adapterView, this, position, id);
                }
            } else {
                AdapterView.OnItemClickListener listener = adapterView
                        .getOnItemClickListener();
                if (listener != null) {
                    listener.onItemClick(adapterView, this, position, id);
                }
            }
        } else {
            if (getChildCount() > 0) {
                View childView = getChildAt(getChildCount() - 1);
                if (longClick) {
                    childView.performLongClick();
                } else {
                    childView.performClick();
                }
            }
        }
    }

    private class RippleEvent {
        float x;
        float y;
        boolean slow;

        final float xx;
        final float yy;
        final float maxX;
        final float maxY;
        final float maxRadius;
        final CustomAccelerateInterpolator fastInterpolator;
        final CustomAccelerateInterpolator slowInterpolator;
        final CustomAccelerateInterpolator fadeInInterpolator;
        final CustomAccelerateInterpolator fadeOutInterpolator;
        final Paint paint;

        float fastMaxRadius;
        float radius;
        long fastRemain;
        long slowRemain;
        float fadeOutMaxAlpha;
        float alpha;
        long fadeInRemain;
        long fadeOutRemain;

        boolean fadedOut;
        boolean completed;

        long period;
        long millis;

        RippleEvent(float x, float y, boolean slow) {
            this.x = x;
            this.y = y;
            this.slow = slow;

            if (mRippleRadius < 0) {
                xx = (mLayoutRight - mLayoutLeft) / 2f;
                yy = (mLayoutBottom - mLayoutTop) / 2f;
                maxX = xx - this.x;
                maxY = yy - this.y;
                maxRadius = (float) Math.hypot(xx, yy);
            } else {
                xx = this.x;
                yy = this.y;
                maxX = 0;
                maxY = 0;
                maxRadius = mRippleRadius;
            }
            fastInterpolator = new CustomAccelerateInterpolator(
                    mRippleFastAcceleration);
            slowInterpolator = new CustomAccelerateInterpolator(
                    mRippleSlowAcceleration);
            fadeInInterpolator = new CustomAccelerateInterpolator(
                    mRippleFadeInAcceleration);
            fadeOutInterpolator = new CustomAccelerateInterpolator(
                    mRippleFadeOutAcceleration);
            paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(mRippleSlowColor);

            radius = 0;
            fastRemain = mRippleFastDuration;
            slowRemain = mRippleSlowDuration;
            fadeOutMaxAlpha = 0;
            alpha = 0;
            fadeInRemain = mRippleFadeInDuration;
            fadeOutRemain = mRippleFadeOutDuration;

            if (!slow) {
                fastMaxRadius = maxRadius;
                slowRemain = 0;
                paint.setColor(mRippleFastColor);
            }

            millis = System.currentTimeMillis();
        }

        void frame() {
            if (slow && !mTouchDown) {
                slow = false;
                fastMaxRadius = maxRadius - radius;
                slowRemain = 0;
                paint.setColor(mRippleFastColor);
            }
            if (!fadedOut && !slow && fastRemain <= mRippleFadeOutDuration) {
                fadedOut = true;
                fadeOutMaxAlpha = alpha;
                fadeInRemain = 0;
            }
            if (!completed && fadeOutRemain == 0) {
                completed = true;
                return;
            }

            timing();
            if (!fadedOut) {
                fadeIn();
            } else {
                fadeOut();
            }
            if (slow) {
                slow();
            } else {
                fast();
            }
        }

        void timing() {
            long currentMillis = System.currentTimeMillis();
            period = currentMillis - millis;
            millis = currentMillis;
        }

        boolean fast() {
            fastRemain -= period;
            fastRemain = Math.max(fastRemain, 0);
            radius = maxRadius
                    - fastMaxRadius
                    + interpolate(fastInterpolator, mRippleFastDuration,
                            fastRemain, fastMaxRadius);
            x = (radius / maxRadius - 1f) * maxX + xx;
            y = (radius / maxRadius - 1f) * maxY + yy;
            return fastRemain == 0;
        }

        boolean slow() {
            slowRemain -= period;
            slowRemain = Math.max(slowRemain, 0);
            radius = interpolate(slowInterpolator, mRippleSlowDuration,
                    slowRemain, maxRadius);
            x = (radius / maxRadius - 1f) * maxX + xx;
            y = (radius / maxRadius - 1f) * maxY + yy;
            return slowRemain == 0;
        }

        boolean fadeIn() {
            fadeInRemain -= period;
            fadeInRemain = Math.max(fadeInRemain, 0);
            alpha = interpolate(fadeInInterpolator, mRippleFadeInDuration,
                    fadeInRemain, mRippleAlpha);
            paint.setAlpha(Math.round(alpha * 255));
            return fadeInRemain == 0;
        }

        boolean fadeOut() {
            fadeOutRemain -= period;
            fadeOutRemain = Math.max(fadeOutRemain, 0);
            alpha = fadeOutMaxAlpha
                    - interpolate(fadeOutInterpolator, mRippleFadeOutDuration,
                            fadeOutRemain, fadeOutMaxAlpha);
            paint.setAlpha(Math.round(alpha * 255));
            return fadeOutRemain == 0;
        }

        float interpolate(TimeInterpolator interpolator, long duration,
                long remain, float max) {
            float percent = ((float) duration - (float) remain)
                    / (float) duration;
            return max * interpolator.getInterpolation(percent);
        }

        class CustomAccelerateInterpolator implements TimeInterpolator {
            final float accelerateFactor;
            final TimeInterpolator interpolator;

            CustomAccelerateInterpolator(float accelerateFactor) {
                this.accelerateFactor = accelerateFactor;
                if (this.accelerateFactor < -0.5f) {
                    interpolator = new DecelerateInterpolator(
                            -this.accelerateFactor);
                } else if (this.accelerateFactor > 0.5f) {
                    interpolator = new AccelerateInterpolator(
                            this.accelerateFactor);
                } else {
                    interpolator = new LinearInterpolator();
                }
            }

            @Override
            public float getInterpolation(float input) {
                return interpolator.getInterpolation(input);
            }
        }
    }

    private class RippleSlowRunnable implements Runnable {
        final float x;
        final float y;

        boolean executed;

        RippleSlowRunnable(float x, float y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void run() {
            createRippleEvent(x, y, true);
            executed = true;
        }
    }

    private class LongClickRunnable implements Runnable {
        boolean executed;

        @Override
        public void run() {
            handleClickEvent(true);
            executed = true;
        }
    }

    private interface OnRippleCompleteListener {
        void onRippleComplete(RippleView rippleView);
    }
}
