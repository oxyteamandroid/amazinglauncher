/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingLauncher Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.notification;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.ingenic.iwds.app.Note;
import com.ingenic.iwds.app.NotificationProxyServiceManager;
import com.ingenic.iwds.app.NotificationServiceBackend;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.utils.IwdsLog;

/**
 * 消息model
 */
public class ServiceNotificationProxyModel implements
        ServiceClient.ConnectionCallbacks {

    public static ServiceNotificationProxyModel sInstance = null;

    public static final int NOTIFICATION_CONNECTED_FLAG = 0x00;
    public static final int SHOW_NOTIFICATION_CONTENT = 0x02;
    public static final int CLEAR_NOTIFICATION_FLAG = 0x03;
    public static final int SHOW_NOTIFICATION_FLAG = 0x04;

    static final String TAG = ServiceNotificationProxyModel.class
            .getSimpleName();

    static String LAUNCHER_PROXY_UUID = "9207c288-dd9f-8fdd-0b88-3ea582bbbeb2";
    static String LAUNCHER_UI_PROXY_UUID = "dcc480e3-5e33-468a-bc78-7ff7fe41fcf4";
    static String NOTIFICATION_PROXY_UUID = "14a73c88-06eb-4d75-a6ef-58bd91742a46";
    private static ServiceClient mClient;
    private static NotificationProxyServiceManager mService;

    private Handler mHandler = null;

    public ServiceNotificationProxyModel(Context context) {
        if (mClient == null) {
            mClient = new ServiceClient(context,
                    ServiceManagerContext.SERVICE_NOTIFICATION_PROXY, this);
        }
    }

    public synchronized static ServiceNotificationProxyModel getInstance(
            Context context) {
        if (sInstance == null) {
            sInstance = new ServiceNotificationProxyModel(context);
        }
        return sInstance;
    }

    public ServiceClient getNotificationClient() {
        return mClient;
    }

    private class NoteHandler extends NotificationServiceBackend {

        private static final String NOTIFICATION_PACKAGENAME = "com.ingenic.notification";
        private static final String NOTIFICATION_PROVIDER_PACKAGENAME = "com.ingenic.provider.notification";

        @Override
        public boolean onHandle(String packageName, int id, Note note) {
            IwdsLog.d(TAG, "widget receive notification packageName = " + packageName + ", id = " + id + ", note = " + note);
            if (id == -1) {
                // 清除通知
                Message msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putParcelable("note", note);
                bundle.putString("packageName", packageName);
                msg.setData(bundle);
                msg.what = CLEAR_NOTIFICATION_FLAG;
                mHandler.sendMessage(msg);
            } else {
                // 显示通知
                Message msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putParcelable("note", note);
                bundle.putString("packageName", packageName.equals(NOTIFICATION_PROVIDER_PACKAGENAME) ? NOTIFICATION_PACKAGENAME : packageName);
                bundle.putInt("noteId", id);
                msg.setData(bundle);
                msg.what = SHOW_NOTIFICATION_FLAG;
                mHandler.sendMessage(msg);
            }
            return true;
        }

        @Override
        public void onCancel(String packageName, int id) {
        }

        @Override
        public void onCancelAll(String packageName) {
        }
    }

    public void setServiceManagerNotification() {
        if (mService == null) {
            mService = (NotificationProxyServiceManager) mClient
                    .getServiceManagerContext();
        }
        registerNotificationBackend();
    }

    private void registerNotificationBackend() {
        try {
            if (mService != null)
                mService.registerBackend(new NoteHandler(),
                        LAUNCHER_PROXY_UUID);
        } catch (Exception e) {
        }
    }

    public void startConnect(Handler handler) {
        mHandler = handler;
        if (mClient != null)
            mClient.connect();
    }

    public void stopConnect() {
        if (mClient != null)
            mClient.disconnect();
    }

    private void sendConnectMessage() {
        // Message msg = new Message();
        // msg.arg1 = ServiceConnectClientModel.DISCONNECT_STATE;
        // msg.what = ServiceConnectClientModel.CONNECT_STATE_FLAG;
        // mHandler.removeMessages(ServiceConnectClientModel.CONNECT_STATE_FLAG);
        // mHandler.sendMessage(msg);
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        mHandler.sendEmptyMessage(NOTIFICATION_CONNECTED_FLAG);
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        sendConnectMessage();
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {
        sendConnectMessage();
    }

}
