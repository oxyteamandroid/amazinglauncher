/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/iwds-ui-jar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.notification;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.launcher.R;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
@SuppressLint("HandlerLeak")
public class NotificationCenterWidget extends LinearLayout {

    static final String TAG = NotificationCenterWidget.class.getSimpleName();

    private Context mContext;
    private View mRootView;
    private SwipeListView mListView;
    private View mHeaderView;
    private Button mHeaderClearButton;
    private NotificationCenterListAdapter mAdapter;
    private ServiceNotificationProxyModel mProxyModel;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    public static final String CLEAR_BY_ID = "Clear By Id";
    public static final String CLEAR_BY_PACKAGENAME = "Clear By PackageName";
    public static final String CLEAR_IPHONE_NOTIFICATIONS = "Clear Iphone Notifications";

    public NotificationCenterWidget(Context context) {
        this(context, null);
    }

    public NotificationCenterWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NotificationCenterWidget(Context context, AttributeSet attrs,
            int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        mRootView = LayoutInflater.from(context).inflate(
                R.layout.widget_notifications_center_layout, this, false);
        mListView = (SwipeListView) mRootView
                .findViewById(R.id.widget_notification_center);

        mHeaderView = LayoutInflater.from(mContext).inflate(
                R.layout.widget_notifications_header_layout, null);
        mListView.addHeaderView(mHeaderView);
        mHeaderClearButton = (Button) mHeaderView
                .findViewById(R.id.notifications_header);
        mHeaderClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFastDoubleClick() && null != mAdapter) {
                    mAdapter.clearAll();
                }
            }
        });

        View emptyView = mRootView.findViewById(R.id.empty);
        mListView.setEmptyView(emptyView);

        mAdapter = new NotificationCenterListAdapter(context);
        mListView.setAdapter(mAdapter);

        mListView.setOnDismissCallback(new SwipeListView.OnDismissCallback() {
            @Override
            public void onDismiss(SwipeListView listView, int position,
                    boolean toRightSide) {
                IwdsLog.d(TAG, "toRightSide = " + toRightSide);
                mAdapter.remove(position);
                if (mAdapter.getCount() < 2 && null != mHeaderClearButton) {
                    mHeaderClearButton.setText(R.string.delete);
                }
            }
        });

        mListView.enableSwipeToDismiss();
        mListView.setSwipeDirection(SwipeListView.SwipeDirection.BOTH);
        mListView.setSwipingLayout(R.id.swiping_layout);
        mListView.setsetSwipingLeftLayout(R.id.swipe_left_layout);
        mListView.setsetSwipingRightLayout(R.id.swipe_right_layout);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                Object item = mAdapter.getItem(position
                        - mListView.getHeaderViewsCount());
                if (!isFastDoubleClick() && null != item
                        && item instanceof Note) {
                    Note note = (Note) item;

                    if (null == note.pendingIntent)
                        return;

                    try {
                        mContext.startIntentSender(
                                note.pendingIntent.getIntentSender(), null,
                                Intent.FLAG_ACTIVITY_NEW_TASK,
                                Intent.FLAG_ACTIVITY_NEW_TASK, 0, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        });

        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);
        addView(mRootView, params);

        mProxyModel = ServiceNotificationProxyModel.getInstance(context);
        mProxyModel.startConnect(mHandler);
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case ServiceNotificationProxyModel.NOTIFICATION_CONNECTED_FLAG: {
                mProxyModel.setServiceManagerNotification();
                break;
            }
            case ServiceNotificationProxyModel.SHOW_NOTIFICATION_FLAG: {
                Bundle insertBundle = msg.getData();
                com.ingenic.iwds.app.Note receiveNote = (com.ingenic.iwds.app.Note) insertBundle
                        .getParcelable("note");
                Note insertNote = new Note();
                insertNote.id = insertBundle.getInt("noteId");
                insertNote.packageName = insertBundle.getString("packageName");
                insertNote.title = receiveNote.title;
                insertNote.content = receiveNote.content;
                insertNote.time = dateFormat.format(new Date(System
                        .currentTimeMillis()));
                insertNote.pendingIntent = receiveNote.pendingIntent;
                mAdapter.insertNotification(insertNote);

                resetHeaderButtonText();
                break;
            }
            case ServiceNotificationProxyModel.CLEAR_NOTIFICATION_FLAG: {
                Bundle clearBundle = msg.getData();
                com.ingenic.iwds.app.Note clearNote = (com.ingenic.iwds.app.Note) clearBundle
                        .get("note");
                String packageName = clearBundle.getString("packageName");

                if (CLEAR_BY_ID.equals(clearNote.title)) {
                    // 清除某一条通知
                    mAdapter.clearById(packageName,
                            Integer.parseInt(clearNote.content));

                } else if (CLEAR_BY_PACKAGENAME.equals(clearNote.title)) {
                    // 清除某一类通知
                    mAdapter.clearByPkg(packageName);

                } else if (CLEAR_IPHONE_NOTIFICATIONS.equals(clearNote.title)) {
                    // 清除来自IPHONE的消息
                    mAdapter.clearByPkg(packageName);

                } else {
                    // 清除所有通知
                    mAdapter.clearAll();

                }

                resetHeaderButtonText();
                break;
            }
            default:
                break;
            }
        }

    };

    private void resetHeaderButtonText() {
        if (null == mHeaderClearButton)
            return;

        if (mAdapter.getCount() < 2) {
            mHeaderClearButton.setText(R.string.delete);
        } else if (mAdapter.getCount() == 2) {
            mHeaderClearButton.setText(R.string.delete_all);
        }
    }

    private static long lastClickTime;

    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < 1000) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    /**
     * 为通知列表设置滚动监听
     * 
     * @param listener
     */
    public void setListOnScrollListener(AbsListView.OnScrollListener listener) {
        if (null == listener || null == mListView)
            return;
        mListView.setOnScrollListener(listener);
    }

}
