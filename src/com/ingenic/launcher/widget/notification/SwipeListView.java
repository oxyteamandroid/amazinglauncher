/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingLauncherWidget Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.launcher.widget.notification;

import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.ListView;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;

/**
 * 一个实现刷卡（横向滑动）功能ListView
 * 
 * @author tZhang
 */
public class SwipeListView extends ListView {

    static final String TAG = SwipeListView.class.getSimpleName();

    /**
     * 刷卡方向
     */
    public enum SwipeDirection {

        /**
         * 左边和右边都可以
         */
        BOTH,

        /**
         * 从右往左
         */
        START,

        /**
         * 从左往右
         */
        END

    }

    /**
     * 可以通过该回调实现禁用某些Item项的刷卡功能
     */
    public interface OnShouldSwipeCallback {

        /**
         * position位置的Item是否应该具备刷卡功能<br/>
         * 返回真表示可以刷卡position位置的Item，否则，不能。
         */
        boolean onShouldSwipe(SwipeListView listView, int position);

    }

    /**
     * 刷卡结束后的回调
     */
    public interface OnDismissCallback {

        /**
         * 当用户刷卡完成后调用
         */
        void onDismiss(SwipeListView listView, int position, boolean toRightSide);

    }

    /**
     * 待执行刷卡的项
     * 
     * @author tZhang
     */
    private class PendingDismissData implements Comparable<PendingDismissData> {

        public int position;
        /**
         * 需要刷卡的View（只是Item项中需要滑动的部分）
         */
        public View view;
        /**
         * Item项整体
         */
        public View childView;

        PendingDismissData(int position, View view, View childView) {
            this.position = position;
            this.view = view;
            this.childView = childView;
        }

        @Override
        public int compareTo(PendingDismissData other) {
            // 降序
            return other.position - position;
        }

    }

    private float mTouchSlop;
    private int mMinFlingVelocity;
    private int mMaxFlingVelocity;
    private long mAnimationTime;

    private final Object[] mAnimationLock = new Object[0];

    // START Swipe-To-Dismiss
    private boolean mSwipeEnabled;
    private OnDismissCallback mDismissCallback;
    private OnShouldSwipeCallback mShouldSwipeCallback;
    private SwipeDirection mSwipeDirection = SwipeDirection.BOTH;
    private int mSwipingLayout;
    private int mSwipingLeftLayout;
    private int mSwipingRightLayout;

    private SortedSet<PendingDismissData> mPendingDismisses = new TreeSet<PendingDismissData>();
    private List<View> mAnimatedViews = new LinkedList<View>();
    private int mDismissAnimationRefCount;

    private boolean mChangeAlphaEnable = true;
    private boolean mSwipePaused;
    private boolean mSwiping;
    private int mViewWidth = 1; // 1 and not 0 to prevent dividing by zero
    private View mSwipeDownView;
    private View mSwipeDownChild;
    private View mSwipeLeftView;
    private View mSwipeRightView;
    private VelocityTracker mVelocityTracker;
    private float mDownX;
    private int mDownPosition;

    // END Swipe-To-Dismiss

    /**
     * {@inheritDoc}
     */
    public SwipeListView(Context context) {
        super(context);
        init(context);
    }

    /**
     * {@inheritDoc}
     */
    public SwipeListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    /**
     * {@inheritDoc}
     */
    public SwipeListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    /**
     * 初始化
     * 
     * @param context
     *            应用上下文
     */
    private void init(Context context) {

        if (isInEditMode()) {
            // 跳过在编辑模式的初始化（IDE预览）。
            return;
        }
        ViewConfiguration vc = ViewConfiguration.get(context);
        mTouchSlop = vc.getScaledTouchSlop();
        mMinFlingVelocity = vc.getScaledMinimumFlingVelocity();
        mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
        mAnimationTime = context.getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        setOnScrollListener(makeScrollListener(null));
    }

    /**
     * 使能刷卡功能
     * 
     * @return {@link SwipeListView}
     * @throws java.lang.IllegalStateException
     *             调用该方法前必须先设置刷卡后的回调
     *             {@link #setDismissCallback(SwipeListView.OnDismissCallback)}
     *             否则将抛出异常。
     */
    public SwipeListView enableSwipeToDismiss() {

        if (mDismissCallback == null) {
            throw new IllegalStateException(
                    "You must pass an OnDismissCallback to the list before enabling Swipe to Dismiss.");
        }

        mSwipeEnabled = true;

        return this;
    }

    /**
     * 禁用刷卡功能
     * 
     * @return {@link SwipeListView}
     */
    public SwipeListView disableSwipeToDismiss() {
        mSwipeEnabled = false;
        return this;
    }

    /**
     * 设置刷卡后的回调
     * 
     * @param dismissCallback
     *            刷卡后的回调.
     * @return {@link SwipeListView}
     */
    public SwipeListView setOnDismissCallback(OnDismissCallback dismissCallback) {
        mDismissCallback = dismissCallback;
        return this;
    }

    /**
     * 设置Item项是否需要刷卡功能的回调
     * 
     * @param shouldSwipeCallback
     *            Item项是否需要刷卡功能的回调
     * @return {@link SwipeListView}
     */
    public SwipeListView setShouldSwipeCallback(
            OnShouldSwipeCallback shouldSwipeCallback) {
        mShouldSwipeCallback = shouldSwipeCallback;
        return this;
    }

    /**
     * 设置刷卡方向
     * 
     * @param direction
     *            刷卡方向，direction只能是{@link SwipeDirection#BOTH},
     *            {@link SwipeDirection#START} 或 {@link SwipeDirection#END}
     * @return {@link SwipeListView}
     */
    public SwipeListView setSwipeDirection(SwipeDirection direction) {
        mSwipeDirection = direction;
        return this;
    }

    /**
     * 设置执行刷卡的布局的资源ID {@link #enableSwipeToDismiss()} to enable the feature.
     * 
     * @param swipingLayoutId
     *            需要执行刷卡的布局的资源ID
     * @return {@link SwipeListView}
     */
    public SwipeListView setSwipingLayout(int swipingLayoutId) {
        mSwipingLayout = swipingLayoutId;
        return this;
    }

    /**
     * 设置刷卡时左边的布局的资源ID
     * 
     * @param swipingLeftLayoutId
     *            左边布局的资源ID
     * @return {@link SwipeListView}
     */
    public SwipeListView setsetSwipingLeftLayout(int swipingLeftLayoutId) {
        mSwipingLeftLayout = swipingLeftLayoutId;
        if (mSwipingLeftLayout > 0) {
            mChangeAlphaEnable = false;
        }
        return this;
    }

    /**
     * 设置刷卡时右边的布局的资源ID
     * 
     * @param swipingRightLayoutId
     *            右边布局的资源ID
     * @return {@link SwipeListView}
     */
    public SwipeListView setsetSwipingRightLayout(int swipingRightLayoutId) {
        mSwipingRightLayout = swipingRightLayoutId;
        if (mSwipingRightLayout > 0) {
            mChangeAlphaEnable = false;
        }
        return this;
    }

    /**
     * 删除指定位置的Item项
     * 
     * @param position
     *            Item项在列表中的位置
     * @throws java.lang.IndexOutOfBoundsException
     *             当试图删除列表范围以外的Item项时抛出异常。
     * @throws java.lang.IllegalStateException
     *             调用此方法前必须先调用
     *             {@link #setDismissCallback(SwipeListView.OnDismissCallback)}
     *             ，否则将抛出异常.
     */
    public void delete(int position) {
        if (mDismissCallback == null) {
            throw new IllegalStateException(
                    "You must set an OnDismissCallback, before deleting items.");
        }
        if (position < 0 || position >= getCount()) {
            throw new IndexOutOfBoundsException(String.format(
                    "Tried to delete item %d. #items in list: %d", position,
                    getCount()));
        }
        View childView = getChildAt(position - getFirstVisiblePosition());
        View view = null;
        if (mSwipingLayout > 0) {
            view = childView.findViewById(mSwipingLayout);
        }
        if (view == null) {
            view = childView;
        }
        slideOutView(view, childView, position, true);
    }

    /**
     * 刷卡滑出动画，在动画结束后会执行方法
     * {@link #performDismiss(android.view.View, android.view.View, int)}.
     * 
     * @param view
     *            需要滑出的View
     * @param childView
     *            被滑动View所在的Item项整体
     * @param position
     *            Item项的位置
     * @param toRightSide
     *            是否从右边滑出
     */
    private void slideOutView(final View view, final View childView,
            final int position, final boolean toRightSide) {

        synchronized (mAnimationLock) {
            if (mAnimatedViews.contains(view)) {
                return;
            }
            ++mDismissAnimationRefCount;
            mAnimatedViews.add(view);
        }

        // 是否需要透明度变化
        if (mChangeAlphaEnable) {
            ViewPropertyAnimator.animate(view)
                    .translationX(toRightSide ? mViewWidth : -mViewWidth)
                    .alpha(0).setDuration(mAnimationTime)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            performDismiss(view, childView, position,
                                    toRightSide);
                        }
                    });
        } else {
            ViewPropertyAnimator.animate(view)
                    .translationX(toRightSide ? mViewWidth : -mViewWidth)
                    .setDuration(mAnimationTime)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            performDismiss(view, childView, position,
                                    toRightSide);
                        }
                    });
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!mSwipeEnabled) {
            return super.onTouchEvent(ev);
        }

        if (mViewWidth < 2) {
            mViewWidth = getWidth();
        }

        switch (ev.getActionMasked()) {
        case MotionEvent.ACTION_DOWN: {
            if (mSwipePaused) {
                return super.onTouchEvent(ev);
            }
            // TODO: ensure this is a finger, and set a flag

            // 确保触摸范围实在Item项范围内
            Rect rect = new Rect();
            int childCount = getChildCount();
            int[] listViewCoords = new int[2];
            getLocationOnScreen(listViewCoords);
            int x = (int) ev.getRawX() - listViewCoords[0];
            int y = (int) ev.getRawY() - listViewCoords[1];
            View child;
            for (int i = getHeaderViewsCount(); i < childCount; i++) {
                child = getChildAt(i);
                if (child != null) {
                    child.getHitRect(rect);
                    if (rect.contains(x, y)) {
                        if (mSwipingLayout > 0) {
                            View swipingView = child
                                    .findViewById(mSwipingLayout);
                            if (swipingView != null) {
                                mSwipeDownView = swipingView;
                                mSwipeDownChild = child;
                                // 启动按压效果
                                downAnim(mSwipeDownView, true);
                                if (mSwipingLeftLayout > 0) {
                                    View swipingLeftView = child
                                            .findViewById(mSwipingLeftLayout);
                                    if (swipingLeftView != null) {
                                        mSwipeLeftView = swipingLeftView;
                                    }
                                }
                                if (mSwipingRightLayout > 0) {
                                    View swipingRightView = child
                                            .findViewById(mSwipingRightLayout);
                                    if (swipingRightView != null) {
                                        mSwipeRightView = swipingRightView;
                                    }
                                }
                                break;
                            }
                        }
                        // 如果没有设置需要滑动的布局，就滑动整个Item项
                        mSwipeDownView = mSwipeDownChild = child;

                        break;
                    }
                }
            }

            if (mSwipeDownView != null) {
                int position = getPositionForView(mSwipeDownView)
                        - getHeaderViewsCount();
                if ((mShouldSwipeCallback == null)
                        || mShouldSwipeCallback.onShouldSwipe(this, position)) {
                    mDownX = ev.getRawX();
                    mDownPosition = position;

                    mVelocityTracker = VelocityTracker.obtain();
                    mVelocityTracker.addMovement(ev);
                } else {
                    mSwipeDownView = mSwipeDownChild = null;
                }
            }
            super.onTouchEvent(ev);
            return true;
        }

        case MotionEvent.ACTION_CANCEL:
        case MotionEvent.ACTION_UP: {
            // 还原按压状态
            downAnim(mSwipeDownView, false);

            if (mVelocityTracker == null) {
                break;
            }

            float deltaX = ev.getRawX() - mDownX;
            mVelocityTracker.addMovement(ev);
            mVelocityTracker.computeCurrentVelocity(1000);
            float velocityX = Math.abs(mVelocityTracker.getXVelocity());
            float velocityY = Math.abs(mVelocityTracker.getYVelocity());
            boolean dismiss = false;
            boolean dismissRight = false;
            if (Math.abs(deltaX) > mViewWidth / 2 && mSwiping) {
                dismiss = true;
                dismissRight = deltaX > 0;
            } else if (mMinFlingVelocity <= velocityX
                    && velocityX <= mMaxFlingVelocity && velocityY < velocityX
                    && mSwiping
                    && isSwipeDirectionValid(mVelocityTracker.getXVelocity())
                    && Math.abs(deltaX) >= mViewWidth * 0.2f) {
                dismiss = true;
                dismissRight = mVelocityTracker.getXVelocity() > 0;
            }
            if (dismiss) {
                // 滑出
                slideOutView(mSwipeDownView, mSwipeDownChild, mDownPosition,
                        dismissRight);
            } else if (mSwiping) {
                // 回复
                if (mChangeAlphaEnable) {
                    ViewPropertyAnimator.animate(mSwipeDownView)
                            .translationX(0).alpha(1)
                            .setDuration(mAnimationTime).setListener(null);
                } else {
                    ViewPropertyAnimator.animate(mSwipeDownView)
                            .translationX(0).setDuration(mAnimationTime)
                            .setListener(null);
                }
            }

            if (deltaX > 0) {
                // 左侧回滚
                scrollBack(mSwipeLeftView, true);
            } else {
                // 右侧回滚
                scrollBack(mSwipeRightView, false);
            }

            mVelocityTracker.recycle();
            mVelocityTracker = null;
            mDownX = 0;
            mSwipeDownView = null;
            mSwipeDownChild = null;
            mSwipeLeftView = null;
            mSwipeRightView = null;
            mDownPosition = AbsListView.INVALID_POSITION;
            mSwiping = false;
            break;
        }

        case MotionEvent.ACTION_MOVE: {
            // 还原按压状态
            if (null != mSwipeDownView && mSwipePaused) {
                ViewHelper.setAlpha(mSwipeDownView, 1.0f);
                ViewHelper.setPivotX(mSwipeDownView,
                        mSwipeDownView.getMeasuredWidth() * 1.0f);
                ViewHelper.setPivotY(mSwipeDownView,
                        mSwipeDownView.getMeasuredHeight() * 1.0f);
                ViewHelper.setScaleX(mSwipeDownView, 1.0f);
                ViewHelper.setScaleY(mSwipeDownView, 1.0f);
            }

            if (mVelocityTracker == null || mSwipePaused) {
                break;
            }

            mVelocityTracker.addMovement(ev);
            float deltaX = ev.getRawX() - mDownX;
            // Only start swipe in correct direction
            if (isSwipeDirectionValid(deltaX)) {
                ViewParent parent = getParent();
                if (parent != null) {
                    // 不让父容器拦截触摸事件，不然将不能刷卡
                    parent.requestDisallowInterceptTouchEvent(true);
                }
                if (Math.abs(deltaX) > mTouchSlop) {
                    mSwiping = true;
                    requestDisallowInterceptTouchEvent(true);

                    MotionEvent cancelEvent = MotionEvent.obtain(ev);
                    cancelEvent
                            .setAction(MotionEvent.ACTION_CANCEL
                                    | (ev.getActionIndex() << MotionEvent.ACTION_POINTER_INDEX_SHIFT));
                    super.onTouchEvent(cancelEvent);
                }
            } else {
                // 如果不是刷卡方向，需要重新记录触摸位置和滑动距离
                mDownX = ev.getRawX();
                deltaX = 0;
            }

            if (mSwiping) {
                ViewHelper.setTranslationX(mSwipeDownView, deltaX);
                if (mChangeAlphaEnable) {
                    ViewHelper.setAlpha(
                            mSwipeDownView,
                            Math.max(
                                    0f,
                                    Math.min(1f, 1f - 2f * Math.abs(deltaX)
                                            / mViewWidth)));
                }

                float scale = 1.0f * deltaX / mViewWidth;
                slidLeftOrRightLayout(mSwipeLeftView, true, scale);

                slidLeftOrRightLayout(mSwipeRightView, false, scale);
                return true;
            }
            break;
        }
        }
        return super.onTouchEvent(ev);
    }

    /**
     * 滑动左边或右边的布局
     */
    private void slidLeftOrRightLayout(View view, boolean left, float scale) {
        if (null == view)
            return;

        float rot;
        float tran;
        if (left) {
            rot = Math.min(0.0f, 180.0f * scale - 90.0f);
            tran = Math.min(view.getMeasuredWidth(),
                    4.0f * view.getMeasuredWidth() * scale);
        } else {
            rot = Math.max(0.0f, 180.0f * scale + 90.0f);
            tran = Math.max(-1.0f * view.getMeasuredWidth(),
                    4.0f * view.getMeasuredWidth() * scale);
        }

        if (rot > 90.0f) {
            view.setVisibility(View.INVISIBLE);
        } else {
            if (view.getVisibility() != View.VISIBLE)
                view.setVisibility(View.VISIBLE);
            ViewHelper.setPivotX(view, left ? 0f
                    : view.getMeasuredWidth() * 1.0f);
            ViewHelper.setPivotY(view, view.getMeasuredHeight() * 0.5f);
            ViewHelper.setTranslationX(view, tran);
            ViewHelper.setRotationY(view, rot);
        }
    }

    /**
     * 按压效果
     * 
     * @param view
     *            按压的View
     * @param down
     *            是否是按下
     */
    private void downAnim(View view, boolean down) {
        if (null == view)
            return;

        ViewPropertyAnimator.animate(mSwipeDownView)
                .scaleXBy(mSwipeDownView.getMeasuredWidth() * 0.5f)
                .scaleX(down ? 0.95f : 1.0f)
                .scaleYBy(mSwipeDownView.getMeasuredHeight() * 0.5f)
                .scaleY(down ? 0.95f : 1.0f).setDuration(mAnimationTime)
                .alpha(down ? 0.75f : 1.0f).setListener(null);
    }

    /**
     * 左/右View的还原动画
     * 
     * @param view
     *            左/右View
     * @param isLeft
     *            是否为左边的View
     */
    private void scrollBack(View view, boolean isLeft) {
        if (null == view)
            return;

        ViewPropertyAnimator.animate(view)
                .translationX((isLeft ? -1 : 1) * view.getMeasuredWidth())
                .rotationXBy(0.0f).rotationYBy(view.getMeasuredHeight() * 0.5f)
                .rotationY((isLeft ? -1 : 1) * 90.0f)
                .setDuration(mAnimationTime).setListener(null);
    }

    /**
     * 滑出动画+Item项压缩动画（高度变为0）+列表位置调整<br/>
     * 在所有动画结束后，回调刷卡结束后的回调方法
     * {@link SwipeListView.OnDismissCallback #onDismiss(SwipeListView, int)}.
     * 
     * @param dismissView
     *            将要滑出的View
     * @param listItemView
     *            滑出View所在的Item的整体
     * @param dismissPosition
     *            滑出View在列表中的位置
     */
    private void performDismiss(final View dismissView,
            final View listItemView, final int dismissPosition,
            final boolean toRightSide) {

        final ViewGroup.LayoutParams lp = listItemView.getLayoutParams();
        final int originalLayoutHeight = lp.height;

        int originalHeight = listItemView.getHeight();
        ValueAnimator animator = ValueAnimator.ofInt(originalHeight, 1)
                .setDuration(mAnimationTime);

        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {

                // 从刷卡动画运行列表中删除这个刷卡动画，确保没有别的刷卡动画正在执行
                boolean noAnimationLeft;
                synchronized (mAnimationLock) {
                    --mDismissAnimationRefCount;
                    mAnimatedViews.remove(dismissView);
                    noAnimationLeft = mDismissAnimationRefCount == 0;
                }

                if (noAnimationLeft) {
                    // No active animations, process all pending dismisses.
                    ViewGroup.LayoutParams lp;
                    for (PendingDismissData pendingDismiss : mPendingDismisses) {
                        mDismissCallback.onDismiss(SwipeListView.this,
                                pendingDismiss.position, toRightSide);
                        // 还原Item项状态（透明度和位置），
                        if (mChangeAlphaEnable) {
                            ViewHelper.setAlpha(pendingDismiss.view, 1f);
                        }
                        ViewHelper.setTranslationX(pendingDismiss.view, 0);
                        lp = pendingDismiss.childView.getLayoutParams();
                        lp.height = originalLayoutHeight;
                        pendingDismiss.childView.setLayoutParams(lp);
                    }

                    mPendingDismisses.clear();
                }
            }
        });

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                lp.height = (Integer) valueAnimator.getAnimatedValue();
                listItemView.setLayoutParams(lp);
            }
        });

        mPendingDismisses.add(new PendingDismissData(dismissPosition,
                dismissView, listItemView));
        animator.start();
    }

    @Override
    public void setOnScrollListener(OnScrollListener l) {
        super.setOnScrollListener(makeScrollListener(l));
    }

    /**
     * 处理列表上下滑动跟Item左右滑动的冲突
     * 
     * @param listener
     * @return
     */
    private OnScrollListener makeScrollListener(final OnScrollListener listener) {
        return new OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                mSwipePaused = scrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL;

                if (null != listener) {
                    listener.onScrollStateChanged(view, scrollState);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                    int visibleItemCount, int totalItemCount) {
                if (null != listener) {
                    listener.onScroll(view, firstVisibleItem, visibleItemCount,
                            totalItemCount);
                }

                final int count = getChildCount();
                for (int i = 0; i < count; i++) {
                    getChildAt(i).invalidate();
                }
            }
        };
    }

    /**
     * 检查滑动方向是否符合 {@link setSwipeDirection(SwipeListView.SwipeDirection)}
     * 设定的刷卡方向
     * 
     * @param deltaX
     *            滑动偏移量.
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private boolean isSwipeDirectionValid(float deltaX) {

        int rtlSign = 1;
        // 在API level 17 之后, 需要检查是否是从右到左的布局，用来判断滑动偏移的正负
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if (getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
                rtlSign = -1;
            }
        }

        switch (mSwipeDirection) {
        default:
        case BOTH:
            return true;
        case START:
            return rtlSign * deltaX < 0;
        case END:
            return rtlSign * deltaX > 0;
        }

    }

}