/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/iwds-ui-jar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.widget.notification;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingenic.launcher.R;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class NotificationCenterListAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Note> mItems = new ArrayList<Note>();

    public NotificationCenterListAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    private static final String ALARM_CLOCK_PACKAGE_NAME = "com.ingenic.alarmclock";
    private static final int ALARM_CLOCK_INDEX = 0;

    public void insertNotification(Note note) {
        if (ALARM_CLOCK_PACKAGE_NAME.equals(note.packageName)) {
            // 闹钟只有一个通知，并且占据第一条
            if (mItems.size() > 0) {
                if (note.id != mItems.get(ALARM_CLOCK_INDEX).id)
                    mItems.add(ALARM_CLOCK_INDEX, note);
                else
                    mItems.set(ALARM_CLOCK_INDEX, note);
            } else
                mItems.add(ALARM_CLOCK_INDEX, note);
        } else {
            mItems.add(note);
        }
        notifyDataSetChanged();
    }

    public void remove(int position) {
        if (position >= mItems.size()) {
            return;
        }
        mItems.remove(position);
        notifyDataSetChanged();
    }

    public void clearById(String packageName, int id) {
        for (Note note : mItems) {
            if (packageName.equals(note.packageName) && id == note.id) {
                mItems.remove(note);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void clearByPkg(String packageName) {
        for (int i = 0; i < mItems.size(); i++) {
            Note note = mItems.get(i);
            if (packageName.equals(note.packageName)) {
                mItems.remove(note);
                i--;
            }
        }
        notifyDataSetChanged();
    }

    public void clearAll() {
        // 闹钟为本地应用，通知需要保留
        for (int i = 0; i < mItems.size(); i++) {
            Note note = mItems.get(i);
            if (!ALARM_CLOCK_PACKAGE_NAME.equals(note.packageName)) {
                mItems.remove(note);
                i--;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return position >= mItems.size() ? null : mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (mItems.size() <= position)
            return null;

        ViewHolder holder = null;
        if (null == convertView) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(
                    R.layout.widget_notifications_item_layout, parent, false);
            holder.icon = (ImageView) convertView.findViewById(R.id.item_icon);
            holder.title = (TextView) convertView.findViewById(R.id.item_title);
            holder.content = (TextView) convertView
                    .findViewById(R.id.item_content);
            holder.time = (TextView) convertView.findViewById(R.id.item_time);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(mItems.get(position).title);
        holder.content.setText(mItems.get(position).content);
        holder.time.setText(mItems.get(position).time);

        return convertView;
    }

    class ViewHolder {
        ImageView icon;
        TextView title;
        TextView content;
        TextView time;
    }

}
