/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  Elf/AmazingLauncher Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.view;

import java.util.ArrayList;
import java.util.List;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;

import com.ingenic.launcher.utils.Constants;
import com.ingenic.library.analog.AnalogClockView;
import com.ingenic.library.digital.DigitalClockView;

/**
 * 左右滑屏控件，装载表盘使用
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class AmazingClockView extends FrameLayout {
    private static final int INVALID_POINTER = -1;
    private int mActivePointerId = INVALID_POINTER;
    /** 横向 */
    public static final int HORIZONTAL = 0;
    /** 纵向 */
    public static final int VERTICAL = 1;
    private static final int MODE_NULL = 0;
    private static final int MODE_PREVIOUS = -1;
    private static final int MODE_NEXT = 1;
    private int mTouchType = HORIZONTAL;
    private int mLastMotionX;
    private int mLastMotionY;
    private boolean mInDragging = false;
    private int mTouchSlop;
    private int mCurrentPage = -1;
    private List<View> mViews = new ArrayList<View>();
    private VelocityTracker mVelocityTracker;
    private int mMinimumVelocity;
    private int mMaximumVelocity;
    private ValueAnimator mAnimator;
    private boolean mAnimating = false;
    private OnPageScrollListener mScrollListener;
    private OnPageChangeListener mChangeListener;
    private boolean mCircularable = false;
    private boolean mIsLongClick = true;
    private View mTranslateView;
    private View mScaleView;

    public AmazingClockView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ViewConfiguration configuration = ViewConfiguration.get(context);
        mTouchSlop = configuration.getScaledTouchSlop();
        mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();
        mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
    }

    public AmazingClockView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AmazingClockView(Context context) {
        this(context, null);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
            int bottom) {

        if (mCurrentPage == -1) {
            setCurrentPage(0);
        }
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    public void addView(View child, int index,
            android.view.ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        // 记录添加的子View，方便获取和配置
        if (index == -1) {
            mViews.add(child);
        } else {
            mViews.add(index, child);
        }
    }

    public void setScale(float scale) {
        for (int i = 0; i < mViews.size(); i++) {
            View clockView = mViews.get(i);
            Log.e("test", "===test==mcurrentpage===" + mCurrentPage
                    + "====i===" + i);
            if (mCurrentPage != i && clockView != null) {

                if (clockView instanceof DigitalClockView) {
                    View view = ((DigitalClockView) clockView).getRootView()
                            .getChildAt(0);
                    view.setScaleX(scale);
                    view.setScaleY(scale);

                    View viewClockName = ((DigitalClockView) clockView)
                            .getRootView().getChildAt(1);
                    if (scale != Constants.ZOOM_NORMAL) {
                        if (viewClockName != null)
                            viewClockName.setVisibility(View.VISIBLE);
                    } else {
                        if (viewClockName != null)
                            viewClockName.setVisibility(View.INVISIBLE);
                    }

                } else if (clockView instanceof AnalogClockView) {
                    View view = ((AnalogClockView) clockView).getRootView()
                            .getChildAt(0);
                    view.setScaleX(scale);
                    view.setScaleY(scale);

                    View viewClockName = ((AnalogClockView) clockView)
                            .getRootView().getChildAt(1);

                    if (scale != Constants.ZOOM_NORMAL) {
                        if (viewClockName != null)
                            viewClockName.setVisibility(View.VISIBLE);
                    } else {
                        if (viewClockName != null)
                            viewClockName.setVisibility(View.INVISIBLE);
                    }
                }

            }
        }
    }

    /**
     * 设置滑动方式
     * 
     * @param type
     *            滑动方式：横向：{@link #HORIZONTAL}，纵向：{@link #VERTICAL}
     */
    public void setTouchType(int type) {
        if (type != HORIZONTAL && type != VERTICAL) {
            throw new IllegalArgumentException(
                    "type must be horizonal or vertical");
        }
        mTouchType = type;
    }

    /**
     * 设置是否可循环，若设置为可循环，滑动将没有边界，默认值为不可循环：false
     */
    public void setCircularable(boolean enable) {
        mCircularable = enable;
    }

    /**
     * 是否可循环
     * 
     * @return 可循环返回true，否则返回false
     */
    public boolean isCircularable() {
        return mCircularable;
    }

    private void initVelocityTrackerifNotExists() {
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        }
    }

    private void recycleVelocityTracker() {
        if (mVelocityTracker != null) {
            mVelocityTracker.recycle();
            mVelocityTracker = null;
        }
    }

    private void initOrResetVelocityTracker() {
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        } else {
            mVelocityTracker.clear();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mIsLongClick) {
            return super.onInterceptTouchEvent(ev);
        }

        if (getChildCount() == 1) {
            return super.onInterceptTouchEvent(ev);
        }
        final int action = ev.getAction();
        // 如果已经是滑动模式，直接拦截事件
        if (action == MotionEvent.ACTION_MOVE && mInDragging) {
            return true;
        }
        switch (action & MotionEvent.ACTION_MASK) {
        case MotionEvent.ACTION_DOWN: {
            mLastMotionX = (int) ev.getX();
            mLastMotionY = (int) ev.getY();
            mActivePointerId = ev.getPointerId(0);
            initOrResetVelocityTracker();
            mVelocityTracker.addMovement(ev);
            break;
        }
        case MotionEvent.ACTION_MOVE:
            final int activePointerId = mActivePointerId;
            if (activePointerId == INVALID_POINTER) {
                // If we don't have a valid id, the touch down wasn't on
                // content.
                break;
            }
            final int pointerIndex = ev.findPointerIndex(activePointerId);
            if (pointerIndex == -1) {
                break;
            }
            int x = (int) ev.getX(pointerIndex);
            int y = (int) ev.getY(pointerIndex);
            int deltaX = Math.abs(x - mLastMotionX);
            int deltaY = Math.abs(y - mLastMotionY);

            if (mTouchType == HORIZONTAL) {
                if (deltaX > mTouchSlop && deltaX > deltaY) {
                    initVelocityTrackerifNotExists();
                    mVelocityTracker.addMovement(ev);
                    mInDragging = true;
                    Log.e("test", "=======1111======");
                } else {
                    Log.e("test", "=======22222222======");
                }
            } else {
                if (deltaY > mTouchSlop && deltaY > deltaX) {
                    initVelocityTrackerifNotExists();
                    mVelocityTracker.addMovement(ev);
                    mInDragging = true;
                    Log.e("test", "=======333333333======");
                } else {
                    Log.e("test", "=======444444444======");
                }
            }
            break;
        default:
            break;
        }
        return mInDragging || super.onInterceptTouchEvent(ev);
    }

    private int mDraggingMode = MODE_NULL;
    private boolean isStarted = false;

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        Log.e("test", "===test===on  event event==mIsLongClick=="
                + mIsLongClick);

        if (mIsLongClick) {
            return super.onTouchEvent(event);
        }

        // 仅有1个子View时，不处理任何事件
        if (getChildCount() == 1 || mIsLongClick) {
            return super.onTouchEvent(event);
        }
        initVelocityTrackerifNotExists();
        mVelocityTracker.addMovement(event);
        final int action = event.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
        case MotionEvent.ACTION_DOWN: {
            mLastMotionX = (int) event.getX();
            mLastMotionY = (int) event.getY();
            mActivePointerId = event.getPointerId(0);
            break;
        }
        case MotionEvent.ACTION_MOVE:
            Log.e("test", "===test===on  move==begin=====");
            final int activePointerIndex = event
                    .findPointerIndex(mActivePointerId);
            if (activePointerIndex == -1) {
                break;
            }
            int x = (int) event.getX(activePointerIndex);
            int y = (int) event.getY(activePointerIndex);
            int deltaX = x - mLastMotionX;
            int deltaY = y - mLastMotionY;
            Log.e("test", "===test===on  move==mInDragging==" + mInDragging);
            // 做一次判断，防止事件未被拦截
            if (!mInDragging) {
                if (mTouchType == HORIZONTAL) {
                    if (Math.abs(deltaX) > mTouchSlop
                            && Math.abs(deltaX) > Math.abs(deltaY)) {
                        mInDragging = true;
                    }
                } else {
                    if (Math.abs(deltaY) > mTouchSlop
                            && Math.abs(deltaY) > Math.abs(deltaX)) {
                        mInDragging = true;
                    }
                }
            }

            // 进入滑动模式
            if (mInDragging) {
                int mode = getModeByDelta(mTouchType == HORIZONTAL ? deltaX
                        : deltaY);

                // 滑动模式变更
                if (mode != mDraggingMode) {
                    mVelocityTracker.clear();
                    if (isStarted && mScrollListener != null) {
                        mScrollListener.onScrollModeChange(mDraggingMode, mode);
                    }
                }

                // 正在滑动时，取消之前的动画
                if (mode != MODE_NULL && mAnimating) {
                    mAnimator.end();
                    mAnimating = false;
                }

                // 没有监听到滑动开始，即为刚开始滑动，发送消息给使用者
                if (!isStarted) {
                    if (mScrollListener != null) {
                        mScrollListener.onScrollStart(mCurrentPage, mode);
                    }
                    isStarted = true;
                }

                // 根据滑动模式获取变更的子View
                switch (mode) {
                case MODE_PREVIOUS:
                    mTranslateView = getPrevious();
                    mScaleView = getCurrent();
                    // getNext().setAlpha(0);
                    break;
                case MODE_NEXT:
                    mTranslateView = getCurrent();
                    mScaleView = getNext();
                    // getPrevious().setAlpha(0);
                    break;
                default:
                    // 无滑动，恢复
                    resetViews(mDraggingMode, mode);
                    if (getCurrent() != null)
                        getCurrent().bringToFront();
                    break;
                }
                mDraggingMode = mode;

                // 子View变化
                if (mTranslateView != null) {
                    translateView(mTranslateView,
                            mTouchType == HORIZONTAL ? deltaX : deltaY);
                }
                if (mScaleView != null) {
                    scaleView(mScaleView, mTouchType == HORIZONTAL ? deltaX
                            : deltaY);
                }

            }
            break;
        case MotionEvent.ACTION_CANCEL:
        case MotionEvent.ACTION_UP:
            Log.e("test", "=====mInDragging===action up ====" + mInDragging);
            if (mInDragging) {
                // 滑动
                final VelocityTracker velocityTracker = mVelocityTracker;
                velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
                int initialVelocity = (int) (mTouchType == HORIZONTAL ? velocityTracker
                        .getXVelocity(mActivePointerId) : velocityTracker
                        .getYVelocity(mActivePointerId));
                int page = mCurrentPage;

                if (mDraggingMode == MODE_PREVIOUS) {
                    // 往前滑动
                    if (getCurrent() != null) {
                        float value = getCurrent().getAlpha();
                        if (initialVelocity > mMinimumVelocity || value < 0.5f) {
                            // 满足切换条件，切换上一张
                            showPrevious();
                        } else {
                            scrollBack(getPrevious(), getCurrent(), 1);
                        }
                    }
                } else if (mDraggingMode == MODE_NEXT) {
                    // 往后滑动
                    if (mTouchType == HORIZONTAL) {
                        // 横向
                        int value = 0;
                        if (getCurrent() != null) {
                            value = (int) getCurrent().getTranslationX();
                        } else {
                            break;
                        }
                        if (initialVelocity < -mMinimumVelocity
                                || value < -getWidth() / 2) {
                            // 满足切换条件，切换下一张
                            showNext();
                        } else {
                            scrollBack(getCurrent(), getNext(), 0);
                        }
                    } else {
                        // 纵向
                        if (getCurrent() != null) {
                            int value = (int) getCurrent().getTranslationY();
                            if (initialVelocity < -mMinimumVelocity
                                    || value < -getHeight() / 2) {
                                // 满足切换条件，切换下一张
                                showNext();
                            } else {
                                scrollBack(getCurrent(), getNext(), 0);
                            }
                        }
                    }
                } else {
                    // 没有滑动，恢复
                    scrollBack(getCurrent(), getNext(), 0);
                }

                // 发送消息给使用者，滑动结束
                if (mScrollListener != null) {
                    mScrollListener.onScrollEnd(page, page == mCurrentPage ? 0
                            : mDraggingMode);
                }
                isStarted = false;
                recycleVelocityTracker();
                mDraggingMode = MODE_NULL;
                mInDragging = false;
            }
            break;
        default:
            break;
        }
        return true;
    }

    public boolean getLongClickEnable() {
        return mIsLongClick;
    }

    /**
     * 设置是否触发了长点击事件
     * 
     * @param isLongClick
     */
    public void setLongClickEnable(boolean isLongClick) {
        mIsLongClick = isLongClick;
    }

    private void resetViews(int oldMode, int mode) {
        if (oldMode != mode) {
            int finalMode = oldMode == MODE_PREVIOUS ? oldMode : mode;
            if (mTranslateView != null) {
                if (mTouchType == HORIZONTAL) {
                    mTranslateView.setTranslationX(finalMode
                            * mTranslateView.getWidth());
                } else {
                    mTranslateView.setTranslationY(finalMode
                            * mTranslateView.getHeight());
                }
            }
            if (mScaleView != null) {
                // mScaleView.setScaleX(-finalMode);
                // mScaleView.setScaleY(-finalMode);
                mScaleView.setAlpha(-finalMode);
            }
        }
        mTranslateView = null;
        mScaleView = null;
    }

    private int getModeByDelta(int delta) {
        int mode = mDraggingMode;
        if (delta > 0) {
            if (mCurrentPage == 0 && !mCircularable) {
                mode = MODE_NULL;
            } else {
                mode = MODE_PREVIOUS;
            }
        } else if (delta < 0) {
            if (mCurrentPage == getChildCount() - 1 && !mCircularable) {
                mode = MODE_NULL;
            } else {
                mode = MODE_NEXT;
            }
        } else {
            mode = MODE_NULL;
        }
        return mode;
    }

    private void translateView(View translate, float delta) {
        if (translate != null) {
            translate.setVisibility(VISIBLE);
            translate.bringToFront();
            requestLayout();
            translate.setAlpha(1.0f);
            // translate.setScaleX(1.0f);
            // translate.setScaleY(1.0f);
            if (mTouchType == HORIZONTAL) {
                translate.setTranslationX(delta > 0 ? delta - getWidth()
                        : delta);
            } else {
                translate.setTranslationY(delta > 0 ? delta - getHeight()
                        : delta);
            }
        }
    }

    private void scaleView(View scale, float delta) {
        if (scale != null) {
            scale.setVisibility(VISIBLE);

            scale.setTranslationX(0);
            scale.setTranslationY(0);
            float value = Math.abs(delta)
                    / (float) (mTouchType == HORIZONTAL ? getWidth()
                            : getHeight());
            if (delta > 0) {
                value = 1 - value;
            }
            // scale.setScaleX(cubicScale(value));
            // scale.setScaleY(cubicScale(value));
            scale.setAlpha(value);
        }
    }

    /**
     * 获取当前视图
     * 
     * @return 当前View
     */
    public View getCurrent() {
        if (mViews.size() == 0 || mCurrentPage < 0
                || mCurrentPage > (mViews.size() - 1)) {
            return null;
        }
        return mViews.get(mCurrentPage);
    }

    /**
     * 获取上一张
     * 
     * @return 上一张View
     */
    public View getPrevious() {
        if (mViews.size() <= 1) {
            return null;
        }
        int prev = mCurrentPage - 1;
        if (prev < 0) {
            if (mCircularable) {
                prev += mViews.size();
            } else {
                return null;
            }
        }
        return mViews.get(prev);
    }

    /**
     * 获取下一张
     * 
     * @return 下一张View
     */
    public View getNext() {
        if (mViews.size() <= 1) {
            return null;
        }
        int next = mCurrentPage + 1;
        if (next >= mViews.size()) {
            if (mCircularable) {
                next -= mViews.size();
            } else {
                return null;
            }
        }
        return mViews.get(next);
    }

    /**
     * 设置当前页，暂不支持动画，故此方法仅供初始化使用，若不调用，默认初始化第0页
     * 
     * @param page
     *            需要设置的页数
     */
    public void setCurrentPage(int page) {
        if (mCurrentPage == page || page < 0 || page >= getChildCount()) {
            return;
        }
        for (int i = 0; i < mViews.size(); i++) {
            View child = mViews.get(i);
            if (i == page) {
                child.bringToFront();
            } else {
                child.setAlpha(0);
            }
        }
        if (mChangeListener != null) {
            mChangeListener.onPageChange(mCurrentPage, page);
        }
        mCurrentPage = page;
    }

    /**
     * 获取当前页
     * 
     * @return
     */
    public int getCurrentPage() {
        return mCurrentPage;
    }

    private void scrollBack(View translate, View scale, float to) {
        float start;
        if (scale != null) {
            start = scale.getAlpha();
        } else if (translate != null) {
            start = mTouchType == HORIZONTAL ? translate.getTranslationX()
                    / (float) translate.getWidth() : translate
                    .getTranslationY() / (float) translate.getHeight();
        } else {
            start = 0.5f;
        }
        animationViews(translate, scale, start, to);
    }

    /**
     * 显示下一页
     */
    public void showNext() {
        if (!mCircularable && mCurrentPage >= getChildCount() - 1) {
            return;
        }
        View translate = getCurrent();
        View scale = getNext();
        float start = scale.getAlpha();
        float to = 1.0f;
        animationViews(translate, scale, start, to);
        int old = mCurrentPage;
        mCurrentPage++;
        if (mCurrentPage >= mViews.size()) {
            mCurrentPage -= mViews.size();
        }

        if (old != mCurrentPage && mChangeListener != null) {
            mChangeListener.onPageChange(old, mCurrentPage);
        }
    }

    /**
     * 显示上一页
     */
    public void showPrevious() {
        if (!mCircularable && mCurrentPage <= 0) {
            return;
        }
        View translate = getPrevious();
        View scale = getCurrent();
        float start = scale.getAlpha();
        float to = 0.0f;
        animationViews(translate, scale, start, to);
        int old = mCurrentPage;
        mCurrentPage--;
        if (mCurrentPage < 0) {
            mCurrentPage += mViews.size();
        }

        if (old != mCurrentPage && mChangeListener != null) {
            mChangeListener.onPageChange(old, mCurrentPage);
        }
    }

    private void animationViews(final View translationView,
            final View scaleView, float fromValue, float toValue) {
        mAnimator = ValueAnimator.ofFloat(fromValue, toValue);
        int duration = (int) (300 * Math.abs(fromValue - toValue));
        if (translationView == null)
            return;
        translationView.setVisibility(VISIBLE);
        scaleView.setVisibility(VISIBLE);
        mAnimator.setDuration(duration);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = ((Float) animation.getAnimatedValue())
                        .floatValue();
                if (translationView != null) {
                    if (mTouchType == HORIZONTAL) {
                        translationView.setTranslationX(-getWidth() * value);
                    } else {
                        translationView.setTranslationY(-getHeight() * value);
                    }
                }
                if (scaleView != null) {
                    // scaleView.setScaleX(cubicScale(value));
                    // scaleView.setScaleY(cubicScale(value));
                    scaleView.setAlpha(value);
                }
            }
        });
        mAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {

                getCurrent().bringToFront();
                setDisplayedChild();
                animation.removeAllListeners();
                mAnimating = false;
            }
        });
        mAnimating = true;
        mAnimator.start();
    }

    void setDisplayedChild() {

        for (View vd : mViews) {

            if (vd.equals(getCurrent())) {
                resetView(vd);
            } else {
                vd.setVisibility(GONE);
            }
        }
    }

    private void resetView(View view) {
        view.setVisibility(VISIBLE);
        view.setTranslationX(0);
        view.setTranslationY(0);
        // view.setScaleX(1);
        // view.setScaleY(1);
        view.setAlpha(1);
    }

    public void setOnPageScrollListener(OnPageScrollListener l) {
        mScrollListener = l;
    }

    public void setOnPageChangeListener(OnPageChangeListener l) {
        mChangeListener = l;
    }

    public interface OnPageScrollListener {
        /**
         * 开始滑屏
         * 
         * @param current
         *            当前页
         * @param mode
         *            模式, 1为下一页，-1为上一页
         */
        void onScrollStart(int current, int mode);

        /**
         * 滑动模式切换
         * 
         * @param oldMode
         *            原来的模式
         * @param mode
         *            现在的模式
         */
        void onScrollModeChange(int oldMode, int mode);

        /**
         * 滑屏结束
         * 
         * @param current
         *            当前也
         * @param mode
         *            模式， 1为下一页，-1为上一页, 0为未切换
         */
        void onScrollEnd(int current, int mode);
    }

    public interface OnPageChangeListener {
        /**
         * 页面切换
         * 
         * @param old
         *            原来的页面
         * @param current
         *            现在的页面
         */
        void onPageChange(int old, int current);
    }
}
