/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *   
 *  Elf/iwds-ui-jar
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsCompatibilityChecker;
import com.ingenic.launcher.R;
import com.ingenic.launcher.utils.Constants;
import com.ingenic.launcher.utils.Utils;

/**
 * 
 * @author LiJinWen(Kevin) & ShiGuangHua(Kenny)
 * 
 */
public class DecorView extends ViewGroup {
    public static final int NO_BAR = 0;
    public static final int BAR_TOP = 1;
    public static final int BAR_BOTTOM = -1;
    private static final int MODE_NULL = -1;
    private static final int MODE_HORIZONTAL = 0;
    private static final int MODE_VERTICAL = 1;
    private static final int DEFAULT_BAR_ANIMATION_DURATION = 600;
    private ViewGroup mStatusBar;
    private ViewGroup mContentLayout;
    private ViewGroup mBottomBar;
    private ImageView mStatusArrowView;
    private ImageView mBottomArrowView;
    private boolean mStatusBarEnable = false;
    private boolean mBottomBarEnable = false;
    private int mShowingBar = NO_BAR;
    private int mTouchSlop;
    private int mMovingBar = NO_BAR;
    private int mTouchMode = MODE_NULL;
    private float mFinalAlpha = 1f; // -1f;
    private LongClickRunnable mLongClickRunnable = new LongClickRunnable();
    private Handler mHandler = new Handler();
    private boolean mIsLongClicked = false;
    private boolean mIsNotMoveStatus = false;

    private boolean mIsDisplayOtherView = false;

    private boolean mBarTouch = false;
    private boolean isCanScroll = true;

    private Context mContext;
    private Intent mWidgetIntent;

    public DecorView(Context context) {
        this(context, null);
    }

    public DecorView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DecorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        IwdsAssert.dieIf(this, !IwdsCompatibilityChecker.getInstance().check(),
                "Compatibility check failed.");
        mContentLayout = new FrameLayout(context);
        mContext = context;
        mWidgetIntent = new Intent(
                Constants.COM_INGENIC_LAUNCHER_DISPLAY_WIDGET_ACTION);
        addView(mContentLayout);
        mContentLayout.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mShowingBar == NO_BAR) {
                    return mLongClick != null
                            && mLongClick.onContentLongClick(v);
                } else {
                    return false;
                }
            }
        });
        mStatusBar = new FrameLayout(getContext());
        addView(mStatusBar);
        mBottomBar = new FrameLayout(getContext());
        addView(mBottomBar);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public void setBarsFinalApha(float alpha) {
        if (alpha > 0) {
            // mFinalAlpha = Math.min(1f, alpha);
        }
    }

    /**
     * 设置内容视图
     * 
     * @param resId
     *            视图资源ID
     * @see #setContentView(View)
     * @see #setContentView(View, LayoutParams)
     */
    public void setContentView(int resId) {
        View view = inflate(getContext(), resId, null);
        setContentView(view);
    }

    /**
     * 设置内容视图
     * 
     * @param view
     *            内容
     * @see #setContentView(int)
     * @see #setContentView(View, LayoutParams)
     */
    public void setContentView(View view) {
        setContentView(view, null);
    }

    /**
     * 设置内容视图
     * 
     * @param view
     *            内容
     * @param params
     *            布局参数
     * @see #setContentView(int)
     * @see #setContentView(View)
     */
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (params == null) {
            params = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
        }
        if (mContentLayout.getChildCount() > 0) {
            mContentLayout.removeAllViews();
        }
        mContentLayout.addView(view, params);
    }

    /**
     * 添加上方状态栏视图
     * 
     * @param view
     *            上方状态栏
     * @param statusArrowId
     *            箭头的资源ID
     * @see #setBottomBar(View)
     */
    public void setStatusBar(View view, int statusArrowId) {
        if (mStatusBar.getChildCount() > 0) {
            mStatusBar.removeAllViews();
        }

        if (statusArrowId > 0) {
            mStatusArrowView = (ImageView) view.findViewById(statusArrowId);
        }

        mStatusBar.addView(view);
        mStatusBarEnable = true;
    }

    /**
     * 添加下方状态栏社图
     * 
     * @param view
     *            下方状态栏
     * @param bottomArrowId
     *            箭头的资源ID
     * @see #setStatusBar(View)
     */
    public void setBottomBar(View view, int bottomArrowId) {
        if (mBottomBar.getChildCount() > 0) {
            mBottomBar.removeAllViews();
        }

        if (bottomArrowId > 0) {
            mBottomArrowView = (ImageView) view.findViewById(bottomArrowId);
        }

        mBottomBar.addView(view);
        mBottomBarEnable = true;
    }

    public void setStatusBarEnable(boolean enable) {
        mStatusBarEnable = enable;
    }

    public void setBottomBarEnable(boolean enable) {
        mBottomBarEnable = enable;
    }

    public void setStatusBarBackColor(int color) {
        mStatusBar.setBackgroundColor(color);
    }

    public void setBottomBarColor(int color) {
        mBottomBar.setBackgroundColor(color);
    }

    public void setStatusBarBackgroundResource(int resid) {
        mStatusBar.setBackgroundResource(resid);
    }

    public void setBottomBarBackgroundResource(int resid) {
        mBottomBar.setBackgroundResource(resid);
    }

    public void setStatusBarBackground(Drawable drawable) {
        mStatusBar.setBackground(drawable);
    }

    public void setBottomBarBackgroud(Drawable drawable) {
        mBottomBar.setBackground(drawable);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child == mContentLayout) {
                child.measure(widthMeasureSpec, heightMeasureSpec);
            } else {
                child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(-1,
                        MeasureSpec.UNSPECIFIED));
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
            int bottom) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            int height = child.getMeasuredHeight();
            if (child == mStatusBar) {
                child.layout(0, -height, child.getMeasuredWidth(), 0);
            } else if (child == mBottomBar) {
                child.layout(0, bottom - top, child.getMeasuredWidth(), bottom
                        - top + height);
            } else {
                child.layout(0, 0, right - left, bottom - top);
            }
        }
    }

    /**
     * 是否现实 other 应用
     * 
     * @param isDisplayOtherView
     */
    public void setDisplayOtherView(boolean isDisplayOtherView) {
        mIsDisplayOtherView = isDisplayOtherView;
    }

    /**
     * 设置是否可以滑动状态栏
     * 
     * @param moveStatus
     */
    public void setNotMoveStatus(boolean moveStatus) {
        mIsNotMoveStatus = moveStatus;
    }

    /**
     * 是否显示 status bar
     * 
     * @return
     */
    public boolean isShowStatusBar() {
        if (mShowingBar == NO_BAR) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 显示上方状态栏
     * 
     * @see #hideStatusBar()
     */
    public void showStatusBar() {
        if (mIsNotMoveStatus) {
            return;
        }
        hideBottomBar();
        if (!mStatusBarEnable)
            return;
        animateBar(BAR_TOP, true);
        if (mShowingBar == BAR_TOP)
            return;
        mShowingBar = BAR_TOP;
        if (mBarListener != null) {
            mBarListener.onBarActionChange(mShowingBar);
        }
        if (mBlurListener != null)
            mBlurListener.refreshBlurView(mContentLayout.getHeight(), true);
    }

    /**
     * 隐藏上方状态栏
     * 
     * @see #showStatusBar()
     */
    public void hideStatusBar() {
        if (mIsNotMoveStatus) {
            return;
        }
        if (!mStatusBarEnable)
            return;
        animateBar(BAR_TOP, false);
        if (mShowingBar == BAR_BOTTOM)
            return;
        mShowingBar = NO_BAR;
        if (mBarListener != null) {
            mBarListener.onBarActionChange(mShowingBar);
        }
        if (mBlurListener != null)
            mBlurListener.refreshBlurView(-1, true);
    }

    /**
     * 显示下方状态栏
     * 
     * @see #hideBottomBar()
     */
    public void showBottomBar() {
        if (mIsNotMoveStatus) {
            return;
        }
        hideStatusBar();
        if (!mBottomBarEnable)
            return;
        animateBar(BAR_BOTTOM, true);
        mShowingBar = BAR_BOTTOM;
        if (mBarListener != null) {
            mBarListener.onBarActionChange(mShowingBar);
        }

        if (mBlurListener != null)
            mBlurListener.refreshBlurView(mContentLayout.getHeight(), false);

        mWidgetIntent.putExtra(Constants.COM_INGENIC_WIDGET_DISPLAY_FLAG,
                Utils.getWidgetClassName(Utils.getCurrentWidgetPostion(),
                        mContext));
        mContext.sendBroadcast(mWidgetIntent);

    }

    /**
     * 隐藏下方状态栏
     * 
     * @see #hideBottomBar()
     */
    public void hideBottomBar() {
        if (!mBottomBarEnable)
            return;
        if (mIsNotMoveStatus) {
            return;
        }
        animateBar(BAR_BOTTOM, false);
        mShowingBar = NO_BAR;
        if (mBarListener != null) {
            mBarListener.onBarActionChange(mShowingBar);
        }

        if (mBlurListener != null)
            mBlurListener.refreshBlurView(-1, false);

        mWidgetIntent.putExtra(Constants.COM_INGENIC_WIDGET_DISPLAY_FLAG, "");
        mContext.sendBroadcast(mWidgetIntent);

    }

    private void animateBar(final int which, final boolean isShow) {
        if (which == NO_BAR) {
            return;
        }
        final View bar = which == BAR_TOP ? mStatusBar : mBottomBar;
        final int height = bar.getHeight();
        final int from = (int) bar.getTranslationY();
        int duration = 0;
        if (height > 0) {
            duration = Math.max(0, height - which * from
                    * DEFAULT_BAR_ANIMATION_DURATION / height);
        }
        ValueAnimator animator = ValueAnimator.ofFloat(from / (float) height,
                isShow ? which : 0);
        if (isShow) {
            animator.setInterpolator(new BounceInterpolator());
        }
        animator.setDuration(duration).start();
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = ((Float) animation.getAnimatedValue())
                        .floatValue();
                int oldTranslationY = (int) bar.getTranslationY();
                bar.setTranslationY(height * value);

                float alpha = which * height * value / height;
                bar.setAlpha(mFinalAlpha > 0 ? mFinalAlpha * alpha
                        : cubicAlpha(alpha));
                setArrowImageResource(bar, which == BAR_TOP ? mStatusArrowView
                        : mBottomArrowView, oldTranslationY);
            }
        });
    }

    private float cubicAlpha(float alpha) {
        return (0.5f + alpha) / 2.0f;
    }

    /**
     * 使能滑动状态栏
     * 
     * @param canScroll
     *            是否可以滑动DecorView的StatusBar
     */
    public void setCanScroll(boolean canScroll) {
        isCanScroll = canScroll;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mIsDisplayOtherView)
            return super.onInterceptTouchEvent(ev);
        final int action = ev.getAction();
        int x = (int) ev.getX();
        int y = (int) ev.getY();
        switch (action & MotionEvent.ACTION_MASK) {
        case MotionEvent.ACTION_DOWN:
            mLastDownX = x;
            mLastDownY = mLastMotionY = y;
            mTouchMode = MODE_NULL;
            if (mLongClick != null && mShowingBar == NO_BAR) {
                mHandler.postDelayed(mLongClickRunnable, 1000);
            }
            mBarTouch = inBar(x, y);
            return mShowingBar != NO_BAR && !mBarTouch;
        case MotionEvent.ACTION_MOVE:
            int deltaX = Math.abs(x - mLastDownX);
            int deltaY = y - mLastMotionY;
            if (mShowingBar == BAR_TOP && Math.abs(deltaY) > mTouchSlop
                    && !isCanScroll) {
                return super.onInterceptTouchEvent(ev);
            }
            if (mTouchMode == MODE_NULL
                    && (deltaX > mTouchSlop || Math.abs(deltaY) > mTouchSlop)) {
                mTouchMode = deltaX > Math.abs(deltaY) ? MODE_HORIZONTAL
                        : MODE_VERTICAL;
            }
            if (mTouchMode != MODE_NULL || mBarTouch) {
                mHandler.removeCallbacks(mLongClickRunnable);
            }
            if (mShowingBar != NO_BAR) {
                return mTouchMode == MODE_VERTICAL;
            }
            if (mTouchMode == MODE_VERTICAL) {
                if (mMovingBar == NO_BAR) {
                    if (deltaY > 0 && mStatusBarEnable) {
                        if (null != mBlurListener)
                            mBlurListener.backgroundChange();
                        mMovingBar = BAR_TOP;
                    } else if (deltaY < 0 && mBottomBarEnable) {
                        if (null != mBlurListener)
                            mBlurListener.backgroundChange();
                        mMovingBar = BAR_BOTTOM;
                    }
                }
            }
            break;
        case MotionEvent.ACTION_UP:
            if (!mBarTouch && mTouchMode == MODE_NULL && !mIsLongClicked) {
                if (mListener != null) {
                    mListener.onContentClick();
                }
            }
            mIsLongClicked = false;
        case MotionEvent.ACTION_CANCEL:
            mHandler.removeCallbacks(mLongClickRunnable);
            mTouchMode = MODE_NULL;
            break;
        default:
            break;
        }
        return mMovingBar != NO_BAR || super.onInterceptTouchEvent(ev);
    }

    private int mLastDownX;
    private int mLastDownY;
    private int mLastMotionY;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mIsDisplayOtherView)
            return super.onTouchEvent(event);
        final int action = event.getAction();
        int x = (int) event.getX();
        int y = (int) event.getY();
        switch (action & MotionEvent.ACTION_MASK) {
        case MotionEvent.ACTION_DOWN:
            mLastDownX = x;
            mLastDownY = mLastMotionY = y;
            break;
        case MotionEvent.ACTION_MOVE:
            int deltaX = Math.abs(x - mLastDownX);
            int deltaY = y - mLastMotionY;
            if (mTouchMode == MODE_NULL
                    && (deltaX > mTouchSlop || Math.abs(deltaY) > mTouchSlop)) {
                mTouchMode = deltaX > Math.abs(deltaY) ? MODE_HORIZONTAL
                        : MODE_VERTICAL;
            }
            if (mShowingBar != NO_BAR && mTouchMode != MODE_NULL) {
                mHandler.removeCallbacks(mLongClickRunnable);
                mMovingBar = mShowingBar;
            }
            if (mMovingBar == NO_BAR) {
                if (Math.abs(deltaY) > mTouchSlop && Math.abs(deltaY) > deltaX) {
                    if (deltaY > 0 && mStatusBarEnable) {
                        mMovingBar = BAR_TOP;
                    } else if (deltaY < 0 && mBottomBarEnable) {
                        mMovingBar = BAR_BOTTOM;
                    }
                }
            }
            if (mMovingBar != NO_BAR) {
                moveBarBy(deltaY);
                mLastMotionY = y;
            }
            break;
        case MotionEvent.ACTION_CANCEL:
        case MotionEvent.ACTION_UP:
            if (mMovingBar != NO_BAR) {
                switch (mMovingBar) {
                case BAR_TOP: {
                    int tran = (int) mStatusBar.getTranslationY();
                    final int height = mStatusBar.getHeight();
                    if (mLastMotionY > mLastDownY) {
                        if (tran > height / 3) {
                            showStatusBar();
                        } else {
                            hideStatusBar();
                        }
                    } else {
                        if (tran < 2 * height / 3) {
                            hideStatusBar();
                        } else {
                            showStatusBar();
                        }
                    }
                    break;
                }
                case BAR_BOTTOM: {
                    int tran = (int) mBottomBar.getTranslationY();
                    final int height = mBottomBar.getHeight();
                    if (mLastMotionY < mLastDownY) {
                        if (tran < -height / 3) {
                            showBottomBar();
                        } else {
                            hideBottomBar();
                        }
                    } else {
                        if (tran > -2 * height / 3) {
                            hideBottomBar();
                        } else {
                            showBottomBar();
                        }
                    }
                    break;
                }
                default:
                    break;
                }
                mMovingBar = NO_BAR;
            } else if (mShowingBar != NO_BAR && mTouchMode == MODE_NULL
                    && !inBar(mLastDownX, mLastDownY)) {
                if (mShowingBar == BAR_TOP) {
                    hideStatusBar();
                } else {
                    hideBottomBar();
                }
            }
            mTouchMode = MODE_NULL;
            break;
        default:
            break;
        }
        return true;
    }

    private boolean inBar(int x, int y) {
        return inStatusBar(x, y) || inBottomBar(x, y);
    }

    private boolean inStatusBar(int x, int y) {
        if (mShowingBar != BAR_TOP) {
            return false;
        }
        y -= mStatusBar.getTranslationY();
        return x >= mStatusBar.getLeft() && x < mStatusBar.getRight()
                && y >= mStatusBar.getTop() && y < mStatusBar.getBottom();
    }

    private boolean inBottomBar(int x, int y) {
        if (mShowingBar != BAR_BOTTOM) {
            return false;
        }
        y -= mBottomBar.getTranslationY();
        return x >= mBottomBar.getLeft() && x < mBottomBar.getRight()
                && y >= mBottomBar.getTop() && y < mBottomBar.getBottom();
    }

    private void moveBarBy(int deltaY) {
        if (mIsNotMoveStatus) {
            return;
        }

        switch (mMovingBar) {
        case BAR_TOP: {
            int height = mStatusBar.getHeight();
            int old = (int) mStatusBar.getTranslationY();
            int dst = Math.min(height, old + deltaY);
            mStatusBar.setTranslationY(dst);
            float alpha = dst / (float) height;
            // mStatusBar.setAlpha(0f);
            mStatusBar.setAlpha(mFinalAlpha > 0 ? mFinalAlpha * alpha
                    : cubicAlpha(alpha));
            setArrowImageResource(mStatusBar, mStatusArrowView, old);

            if (mBlurListener != null)
                mBlurListener.refreshBlurView(dst, true);
            break;
        }
        case BAR_BOTTOM: {
            int height = mBottomBar.getHeight();
            int old = (int) mBottomBar.getTranslationY();
            int dst = Math.max(-height, old + deltaY);
            mBottomBar.setTranslationY(dst);
            float alpha = -dst / (float) height;
            // mStatusBar.setAlpha(0f);
            mBottomBar.setAlpha(mFinalAlpha > 0 ? mFinalAlpha * alpha
                    : cubicAlpha(alpha));
            setArrowImageResource(mBottomBar, mBottomArrowView, old);
            if (mBlurListener != null)
                mBlurListener.refreshBlurView(-dst, false);
            break;
        }
        }
    }

    private void setArrowImageResource(View bar, ImageView view,
            int oldTranslationY) {
        if (null == view)
            return;

        int curTranslationY = (int) bar.getTranslationY();
        if (oldTranslationY < curTranslationY) {
            view.setImageResource(R.drawable.down_arrow);
        } else if (oldTranslationY > curTranslationY) {
            view.setImageResource(R.drawable.up_arrow);
        }

        if (Math.abs(curTranslationY) == bar.getHeight()) {
            view.setImageResource(R.drawable.status_line);
        }
    }

    private OnContentClickListener mListener;

    public void setOnContentClickListener(OnContentClickListener l) {
        mListener = l;
    }

    public interface OnContentClickListener {
        void onContentClick();
    }

    private OnContentLongClickListener mLongClick;

    public void setOnContentLongClickListener(OnContentLongClickListener l) {
        mLongClick = l;
    }

    public interface OnContentLongClickListener {
        boolean onContentLongClick(View contentView);
    }

    private OnBarActionChangeListener mBarListener;

    public void setOnBarActionChangeListener(OnBarActionChangeListener l) {
        mBarListener = l;
    }

    public interface OnBarActionChangeListener {
        void onBarActionChange(int action);
    }

    private OnBlurListener mBlurListener;

    public void setOnBlurListener(OnBlurListener mBlurListener) {
        this.mBlurListener = mBlurListener;
    }

    // 需要进行高斯模糊的监听器
    public interface OnBlurListener {
        void refreshBlurView(int deltaY, boolean isStatus);

        /**
         * 重新创建一张高斯模糊的图片
         */
        void backgroundChange();
    }

    private class LongClickRunnable implements Runnable {

        @Override
        public void run() {
            mContentLayout.performLongClick();
            mIsLongClicked = true;
        }
    }
}