/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.ingenic.launcher.blur.Blur;
import com.ingenic.launcher.view.DecorView.OnBlurListener;

/**
 * 装载表盘和other应用的view
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class AmazingContentView extends FrameLayout {

    private List<View> mViews = new ArrayList<View>();

    private DecorView mDecorView;

    private Rect mRectBlurArea;
    private Rect mRectOriginalArea;
    private Bitmap mCanvasBitmap;
    private Bitmap mBlurBitmap;
    private Canvas mCanvas;
    private Paint paint;
    private int deltaY = 0;
    private Blur mBlur;

    private boolean isBackgroundChange = true;

    public AmazingContentView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public AmazingContentView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AmazingContentView(Context context) {
        this(context, null);

        setWillNotDraw(false);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
            int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    public void addView(View child, int index,
            android.view.ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        // 记录添加的子View，方便获取和配置
        if (index == -1) {
            mViews.add(child);
        } else {
            mViews.add(index, child);
        }
    }

    public void setDecorView(DecorView decorView) {
        mDecorView = decorView;
        if (mDecorView != null)
            mDecorView.setOnBlurListener(onBlurListener);
    }

    private OnBlurListener onBlurListener = new OnBlurListener() {
        public void refreshBlurView(int deltaY, boolean isStatus) {
            refreshView(deltaY, isStatus);
        }

        public void backgroundChange() {
            isBackgroundChange = true;
        }
    };

    public void refreshView(int deltaY, boolean isStatus) {
        if (Math.abs(this.deltaY - deltaY) <= 1) {
            return;
        }
        this.deltaY = deltaY;
        if (mBlur == null)
            mBlur = Blur.getInstance(getContext());
        initPaint();
        if (deltaY > 0) {
            initCanvas(isStatus);
        }
        postInvalidate();
    }

    private void initPaint() {
        if (paint == null) {
            paint = new Paint();
            paint.setAntiAlias(true);
        }
    }

    private void initCanvas(boolean isStatus) {
        if (mRectBlurArea == null)
            mRectBlurArea = new Rect();
        if (mRectOriginalArea == null)
            mRectOriginalArea = new Rect();
        calculateArea(isStatus);
    }

    private void calculateArea(boolean isStatus) {

        int viewWidth = getWidth();
        int viewHeight = getHeight();
        mRectBlurArea.left = 0;
        mRectBlurArea.right = viewWidth;
        mRectOriginalArea.left = 0;
        mRectOriginalArea.right = viewWidth;

        if (isStatus) {
            mRectBlurArea.bottom = viewHeight;
            mRectBlurArea.top = 0;
            mRectOriginalArea.bottom = viewHeight;
            mRectOriginalArea.top = deltaY;
        } else {
            mRectBlurArea.bottom = viewHeight;
            mRectBlurArea.top = 0;
            mRectOriginalArea.bottom = viewHeight - deltaY;
            mRectOriginalArea.top = 0;
        }
        if (mCanvasBitmap == null) {
            // 获取整个屏幕的位图，这样不用经常截取位图（经常截图位图会导致频繁的gc）
            mCanvasBitmap = Bitmap.createBitmap(viewWidth, viewHeight,
                    Config.ARGB_8888);
            mCanvas = new Canvas(mCanvasBitmap);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        if (deltaY > 0) {
            final Rect rect = mRectOriginalArea;

            final Paint mPaint = paint;
            final Rect rectBlur = mRectBlurArea;
            final Bitmap canvasBitmap = mCanvasBitmap;
            final Canvas blurCanvas = mCanvas;

            if (isBackgroundChange) {
                blurCanvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);
                blurCanvas.save();
                super.draw(blurCanvas);
                blurCanvas.restore();
                mBlurBitmap = mBlur.blur(canvasBitmap);// 对图片进行模糊处理
                isBackgroundChange = false;
            }
            if (mBlurBitmap != null) {
                canvas.drawBitmap(mBlurBitmap, null, rectBlur, mPaint);
                mPaint.setColor(Color.BLACK);
            }
            canvas.save();
            canvas.clipRect(rect);
            super.draw(canvas);
            canvas.restore();
        } else {
            super.draw(canvas);
        }
    }
}
