/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.launcher.utils;

/**
 * 常量
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public class Constants {

    /**
     * 日出日落的action
     */
    public static final String COM_TIME_ADDITIONAL_ACTION = "com.time.additional.action";

    /**
     * 重启日落日出服务的action
     */
    public static final String RESTART_TIME_ADDITIONAL_ACTION = "com.restart.time.additional.action";

    /**
     * 本地音乐widget
     */
    public static final String LOCAL_MUSIC_WIDGET_PACKAGE_NAME = "com.ingenic.music.local.widget.LocalMusicProvider";

    /**
     * 本地天气应用widget
     */
    public static final String WEATHER_WIDGET_PACKAGE_NAME = "com.ingenic.weather.widget.OriginWeatherWidgetProvider";

    /**
     * 设置应用widget 蓝牙，飞行模式，静音，屏幕亮度
     */
    public static final String SETTINGS_WIDGET_PACKAGE_NAME = "com.ingenic.settings.widget.OriginSettingsWidgetProvider";

    /**
     * 显示电量的 widget
     */
    public static final String BATTERY_WIDGET_PACKAGE_NAME = "com.ingenic.launcher.widget.battery";

    /**
     * 健康widget
     */
    public static final String HEALTH_WIDGET_PACKAGE_NAME = "com.ingenic.launcher.widget.health";

    /**
     * 心率widget
     */
    public static final String HEARTRATE_WIDGET_PACKAGE_NAME = "com.ingenic.launcher.widget.heartrate";

    /**
     * 远程音乐widget
     */
    public static final String REMOTE_MUSIC_WIDGET_PACKAGE_NAME = "com.ingenic.music.remote.widget.RemoteMusicProvider";

    /**
     * 当前显示哪一个widget的action， 显示的包名如果为 "",表示 隐藏所有的widget,flag:widgetPackageName
     */
    public static final String COM_INGENIC_LAUNCHER_DISPLAY_WIDGET_ACTION = "com.ingenic.launcher.display.widget.action";

    /**
     * 显示widget的flag
     */
    public static final String COM_INGENIC_WIDGET_DISPLAY_FLAG = "widgetPackageName";

    /**
     * 主题切换的action
     */
    public static final String THEME_CHANGE_ACTION = "com.ingenic.theme.change";

    /**
     * 闹钟是否设置action
     */
    public static final String ALARM_CLOCK_ACTION = "com.ingenic.getclock.data";

    /**
     * 表盘显示动画播放时间
     */
    public static int ANIMATION_DURATION_CLOCK_TIME = 700;

    /**
     * other 应用显示的时间
     */
    public static int ANIMATION_DURATION_OTHER_TIME = 700;

    /**
     * 日出日落服务的UUID
     */
    public static String TIMEADDITIONAL_UUID = "5ad8fcb6-46c7-3c03-62f6-3a1c414d9a93";

    /**
     * 屏幕的宽度和高度，这个是动态设置的，可以适配其它屏幕
     */
    public static int SCREEN_WIDTH = 320, SCREEN_HEIGHT = 320;

    /**
     * 表盘刷新时间 1s
     */
    public static final int FRESH_INTERVAL_TIME = 1000;

    public static final int MESSAGE_UPDATE_SECOND = 0x011;

    /**
     * 其它应用 延迟显示的毫秒数
     */
    public static final int OTHER_DISPLAY_DELAYED = 400;

    /**
     * 延迟显示的 标记 flag 1
     */
    public static final int DELAY_DISPLAY_FLAG = 1;

    /**
     * 表盘缩放比例
     */
    public static final float ZOOM_vALUES = 0.7f;

    /**
     * 表盘正常比例
     */
    public static final float ZOOM_NORMAL = 1.0f;

    /**
     * 表盘缩小的比例（从 表盘 缩小到 other应用 表盘的缩小比例）
     */
    public static final float ZOOM_CLOCK_VALUES = 0.2f;

    /**
     * other 应用缩放 正常
     */
    public static final float ZOOM_OTHER_NORMAL = 1.0f;

    /**
     * other 应用缩放 值
     */
    public static final float ZOOM_OTHER_VALUES = 2.5f;

    /**
     * widget 底部圆点的字体大小
     */
    public static final float WIDGET_INDICATOR_FONT_SIZE = 3.0f;

    /**
     * 通道是否畅通信息
     */
    public static final String CHANNEL_AVAILABLE_INFO = "avaiable";

    /**
     * 远程音乐的信息
     */
    public static final String REMOTE_MUSIC_INFO = "musicInfo";

    /**
     * 链接电源时弹出显示当前电量 Activity 的显示时间
     */
    public static final int DURATION_ACTIVITY_DISPLAY = 2 * 1000;

    /**
     * Bundle发送当前的电量
     */
    public static final String BATTERY_LEVEL_BUNDLE = "level";

    /**
     * Bundle发送满电的电量
     */
    public static final String BATTERY_FULL_BUNDLE = "full";

    /**
     * 默认电量
     */
    public static final int BATTERY_DEFAULT_CHARGE = 80;

    /**
     * 满电状态
     */
    public static final int BATTERY_FULL_POWER_STATE = 100;
}