/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.launcher.utils;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.ingenic.iwds.HardwareList;
import com.ingenic.launcher.R;
import com.ingenic.launcher.widget.battery.BatteryWidget;
import com.ingenic.launcher.widget.heartrate.HealthHeartRateView;
import com.ingenic.launcher.widget.wear.WearRoundedImageView;

/**
 * 工具类
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public class Utils {

    public static final int VOLUME_CURRENT = -5;
    public static final int PLAY_MUSIC_STATE = -1;

    private static boolean mIsFinishedAnimation = true;

    private static View viewHealth;

    private static String[] widgetArray = null;

    private static int mCurrentWidgetPostion = 0;

    /**
     * 通过位置索引获取widget包名
     * 
     * @param index
     * @param context
     * @return
     */
    public static String getWidgetClassName(int index, Context context) {
        if (widgetArray == null)
            widgetArray = ((Activity) context).getResources().getStringArray(
                    R.array.widget_class_name);
        if (index < widgetArray.length) {
            return widgetArray[index];
        }
        return null;
    }

    /**
     * 设置当前显示的widget
     *
     * @param currentPostion
     */
    public static void setCurrentWidgetPosition(int currentPostion) {
        mCurrentWidgetPostion = currentPostion;
    }

    /**
     * 获取当前显示的widget的位置
     *
     * @return
     */
    public static int getCurrentWidgetPostion() {
        return mCurrentWidgetPostion;
    }

    /**
     * 通过包名得到library的view
     *
     * @param packageName
     * @param context
     * @return
     */
    public static View getLocalLibraryWidget(String className, Context context) {
        if (className.equals(Constants.BATTERY_WIDGET_PACKAGE_NAME)) {
            return new BatteryWidget(context);
        } else if (className.equals(Constants.HEALTH_WIDGET_PACKAGE_NAME)) {
            return getHealthLibraryView(context);
        } else if (className.equals(Constants.HEARTRATE_WIDGET_PACKAGE_NAME)) {
            return new HealthHeartRateView(context);
        }
        return new BatteryWidget(context);
    }

    /**
     * 健康的view
     *
     * @param context
     * @return
     */
    public static View getHealthLibraryView(Context context) {
        if (viewHealth == null)
            viewHealth = LayoutInflater.from(context).inflate(
                    R.layout.widgethealth_steps_widget_layout, null);

        return viewHealth;
    }

    /**
     * 通过包名判断是否是本地的widget还是library true: widget false: library
     *
     * @param packageName
     * @param context
     * @return
     */
    public static boolean getWhetherWidget(String packageName, Context context) {
        if (packageName.equals(Constants.LOCAL_MUSIC_WIDGET_PACKAGE_NAME)
                || packageName.equals(Constants.WEATHER_WIDGET_PACKAGE_NAME)
                || packageName.equals(Constants.SETTINGS_WIDGET_PACKAGE_NAME)
                || packageName.equals(Constants.REMOTE_MUSIC_WIDGET_PACKAGE_NAME)) {
            return true;
        }
        return false;
    }

    /**
     * 得到当前需要显示的widget个数
     *
     * @param context
     * @return
     */
    public static int getWidgetSize(Context context) {
        String[] widgets = ((Activity) context).getResources().getStringArray(
                R.array.widget_class_name);
        return widgets.length;
    }

    /**
     * cp 转 px
     *
     * @param context
     * @param dp
     * @return
     */
    public static int dp2Px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /**
     * 是否完成动画的操作
     *
     * @return
     */
    public static boolean getIsFinishedAnimation() {
        return mIsFinishedAnimation;
    }

    /**
     * 对view进行缩放的方法
     *
     * @param fromScale
     *            起始缩放比例
     * @param toScale
     *            终止缩放比例
     * @param scaleView
     *            缩放的对象
     */
    public static void setScaleView(final float fromScale, final float toScale,
            final int animationDuration, final View scaleView,
            final boolean isClockView) {

        ValueAnimator animator = ValueAnimator.ofFloat(fromScale, toScale);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = ((Float) animation.getAnimatedValue())
                        .floatValue();
                if (isClockView) {
                    // scaleView.setScaleX(value);
                    // scaleView.setScaleY(value);
                    scaleView.setAlpha(value);
                } else {
                    WearRoundedImageView.isStopZoom = true;
                    scaleView.setScaleX(value);
                    scaleView.setScaleY(value);
                    if (toScale == Constants.ZOOM_NORMAL) {

                        if (fromScale == Constants.ZOOM_OTHER_VALUES) {
                            scaleView.setAlpha(Constants.ZOOM_OTHER_VALUES
                                    - value);
                        } else {
                            scaleView.setAlpha(value);
                        }

                    } else {
                        scaleView.setAlpha(Constants.ZOOM_OTHER_VALUES - value);
                    }
                }

            }
        });

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator arg0) {
                mIsFinishedAnimation = false;
            }

            @Override
            public void onAnimationRepeat(Animator arg0) {
            }

            @Override
            public void onAnimationEnd(Animator arg0) {

                mIsFinishedAnimation = true;
                if (toScale == Constants.ZOOM_NORMAL) {
                    WearRoundedImageView.isStopZoom = false;
                    scaleView.setVisibility(View.VISIBLE);
                } else {
                    scaleView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationCancel(Animator arg0) {
            }
        });

        animator.setDuration(animationDuration).start();

    }

    /**
     * 判断当前的屏是园还是方形
     *
     * @return
     */
    public static boolean IsCircularScreen() {
        return HardwareList.IsCircularScreen();
    }
}
